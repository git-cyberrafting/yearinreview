<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('myDropdown')) {
    function myDropdown($name,$options,$setDefault=null,$extra=null){
        $myOptions=[];
        foreach($options as $key=>$value){
            if($key!=''){
               $myOptions[$key.'-:|#|:-'.$value]=$value;   
            }else{
               $myOptions[$key]=$value; 
            }
            
        }
        unset($options);
        $ci=&get_instance();
        $inputName=$name;
        if(is_array($name)){
         $inputName=$name['name'];
        }
        $setDefault = ($ci->input->method()==='post')? $ci->input->post($inputName):$setDefault;
        return form_dropdown( $name, $myOptions,set_value($inputName,$setDefault),$extra);
    }
}

if (!function_exists('myDropdownSelectedValue')) {
    function myDropdownSelectedValue($name){
        $ci=&get_instance();
        $value=($ci->input->method()==='post')?$ci->input->post($name):(($ci->input->method()==='get')?$ci->input->get($name):[]);
        $selectedValue=explode('-:|#|:-',$value);
        return $selectedValue[0];
    }
}

if (!function_exists('myDropdownSelectedName')) {
    function myDropdownSelectedName($name){
        $ci=&get_instance();
        $value=($ci->input->method()==='post')?$ci->input->post($name):(($ci->input->method()==='get')?$ci->input->get($name):[]);
        $selectedValue=explode('-:|#|:-',$value);
        return end($selectedValue);
    }
}

if (!function_exists('jsDropdownSelectedName')) {
    function jsDropdownSelectedName($value){
        $selectedValue=explode('-:|#|:-',$value);
        return end($selectedValue);
    }
}
if (!function_exists('jsDropdownSelectedValue')) {
    function jsDropdownSelectedValue($value){
        $selectedValue=explode('-:|#|:-',$value);
        return $selectedValue[0];
    }
}