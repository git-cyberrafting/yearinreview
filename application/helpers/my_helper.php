<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('now')) {

    function now() {
        return date('Y-m-d H:i:s');
    }

}
if (!function_exists('numberToDecimal')) {

    function numberToDecimal($number) {
        $decimal='.';
        $brokenNumber = explode($decimal,$number);
        if(is_array($brokenNumber) && count($brokenNumber)==2){
            return(strlen($brokenNumber[1])==2)
                    ? $brokenNumber[0].'.'.$brokenNumber[1]
                    : ((strlen($brokenNumber[1])==1)
                        ? $brokenNumber[0].'.'.$brokenNumber[1].'0'
                        : $brokenNumber[0].'.'.substr($brokenNumber[1],2));
        }
        elseif(is_array($brokenNumber) && count($brokenNumber)==1){
            return $brokenNumber[0].'.00';    
        }
        else{
            return '00.00';    
        }
    }
}

if (!function_exists('d')) {

    function d($data = 'NONE') {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

}
if (!function_exists('dd')) {

    function dd($data = 'NONE') {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }

}
if (!function_exists('deleteDirectory')) {
    function deleteDirectory($dirPath) {
        if (is_dir($dirPath)) {
            $objects = scandir($dirPath);
            foreach ($objects as $object) {
                if ($object != "." && $object !="..") {
                    if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                        deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
        reset($objects);
        rmdir($dirPath);
        }
    }
}
if (!function_exists('makeDirectory')) {
    function makeDirectory($dirPath){
       $dirs= explode('/', $dirPath);
       $dirPath='./';
       foreach($dirs as $dir){
           if($dirPath!='./'){
           $dirPath.='/'.$dir;}else{
           $dirPath.=$dir;    
           }
            if (!is_dir ($dirPath)) {
                mkdir($dirPath,0777,true);
            }
            if(!is_writable($dirPath)){
                chmod($dirPath,0777);
            }
       }
    }
}
if (!function_exists('validationRules')) {

    /** getInputs creating insert input function and validation array
     * used to creating validation array and getting input field array.
     */
    function validationRules() {
        echo "if(\$this->input->method()==='post'){<br>";
        foreach ($_POST as $key => $value) {
            $label= $key;
            if (strtolower($label) == 'titlename'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'required');<br>";
            }
            elseif (strtolower($label) == 'firstname'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'trim|required|alpha|min_length[2]|max_length[40]');<br>";
            }
            elseif (strtolower($label) == 'middlename'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'trim|required|alpha|min_length[2]|max_length[40]');<br>";
            }
            elseif (strtolower($label) == 'lastname'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'trim|required|alpha|min_length[2]|max_length[40]');<br>";
            }
            elseif (strtolower($label) == 'email'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'trim|required|valid_email|max_length[100]');<br>";
            }
            elseif (strtolower($label) == 'password'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'required| min_length[6]|max_length[50]');<br>";
            }
            elseif (strtolower($label) == 'confirmpassword'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'required|matches[password]');<br>";
            }
            elseif (strtolower($label) == 'newpassword'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'required| min_length[6]|max_length[50]');<br>";
            }
            elseif (strtolower($label) == 'confirmnewpassword'){
            echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'required|matches[newpassword]');<br>";
            }
             else{
               echo "\$this->form_validation->set_rules('" . $key . "', '" . camelCaseToString($label) . "', 'trim|required');<br>";
             }
        }  
         echo "if (\$this->form_validation->run() == True){";
         
         
                 echo "<br>}<br>}";
                 die;
    }

}

function isValidationError($name) {
    return (validation_errors()) ? ((form_error($name)) ? true : false ): false;
}

if (!function_exists('isArray')) { 
    function isArray($array){
        return (is_array($array) && count($array)>0)?$obj:false;
    }
}

if (!function_exists('isObject')) { 
    function isObject($obj) {
        return (is_object($obj) && count($obj)>0)?$obj:false;
    }
}

