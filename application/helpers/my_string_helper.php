<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('uniqueString')) {

    function uniqueString() {
        return date('YmdHis') .'_'. random_string('alnum', 6);
    }

}
if (!function_exists('underscoreToDash')) {
    function underscoreToDash($str) {
        return str_replace('_', '-', $str);
    }
}
if (!function_exists('underscoreToSpace')) {
    function underscoreToSpace($str) {
        return str_replace('_', ' ', $str);
    }
}
if (!function_exists('dashToUnderscore')) {
    function dashToUnderscore($str) {
        return str_replace('-', '_', $str);
    }
}
if (!function_exists('dashToSpace')) {
    function dashToSpace($str) {
        return str_replace('-', ' ', $str);
    }
}
if (!function_exists('spaceToDash')) {
    function spaceToDash($str) {
        return str_replace(' ', '-', $str);
    }
}
if (!function_exists('spaceToUnderscore')) {
    function spaceToUnderscore($str) {
        return str_replace(' ', '_', $str);
    }
}

if (!function_exists('firstLetterOfEachWord')) {
    function firstLetterOfEachWord($str){
        $strWords = preg_split("/[\s,_-]+/", $str);
        $strAcronym = "";
        foreach ($strWords as $w) {
          $strAcronym .= $w[0];
        }
        return $strAcronym;
    }
}

if (!function_exists('camelCaseToString')) {
   function camelCaseToString($str){
    $matches=preg_split('/(?=[A-Z])/',$str);
    $matches=array_map('strtolower', $matches);
    return implode(' ',$matches);
}
}
if (!function_exists('camelCaseToSnakeCase')) {
   function camelCaseToSnakeCase($str){
    $matches=preg_split('/(?=[A-Z])/',$str);
    $matches=array_map('strtolower', $matches);
    return implode('_', $matches);
}
}
if (!function_exists('snakeCaseToCamelCase')) {
   function snakeCaseToCamelCase($str){
    $matches= explode('_',$str);
    $matches=array_map('ucfirst', $matches);
    return implode('', $matches);
    
}
}