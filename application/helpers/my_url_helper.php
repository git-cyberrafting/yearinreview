<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('redirectTo')) {

    function redirectTo($url,$method='auto',$code='') {
        return ($method=='location')? redirect(url($url),$method):redirect(url($url),$method,$code);
    }
}

if (!function_exists('url')) {
     function url($path = NULL) {
        $url = '';
        if ($path != NULL)
        {$url .= $path;}
        return base_url($url);
    }
}
if (!function_exists('urlApi')) {
    function urlApi($path = NULL) {
        $ci = & get_instance();
        return $ci->config->item('apiURL').$path;
    }
}
if (!function_exists('backendUrl')) {
    function backendUrl($path = NULL) {
        $ci = & get_instance();
        return $ci->config->item('backendUrl').$path;
    }
}
if (!function_exists('frontendUrl')) {
    function frontendUrl($path = NULL) {
        $ci = & get_instance();
        return $ci->config->item('frontendUrl').$path;
    }
}
if (!function_exists('makeSlug')) {

    function makeSlug($name,$id) {
        return str_replace(' ','-',trim($name)).'-'.trim($id);
    }
}
if (!function_exists('getSlugId')) { 
    
    function getSlugId($n=null) {
     $ci = & get_instance();
     $n=($n)?$n:$ci->uri->total_segments();
     $slugId=end((explode('-', $ci->uri->segment($n))));  
   
   return (is_numeric($slugId) && ctype_digit($slugId))?$slugId:0;  
   }
}

if (!function_exists('makeHashUrl')) {

    function makeHashUrl($url,$verficationHash,$id){
        return ($url.'/'.urlencode(base64_encode($verficationHash.'|:|#|:|'.$id)));
    }
}

if (!function_exists('verifyHashUrl')) {

    function verifyHashUrl($verficationHash){
        return explode('|:|#|:|', base64_decode(urldecode($verficationHash)));
    }
}


if (!function_exists('css')) {

    function css($url) {
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $ci = & get_instance();
        $extention=(end((explode('.',$url)))=='css')?'':'.css';
        if ($scheme == 'http' || $scheme == 'https')
        { return '<link href="' . $url . '" rel="stylesheet" />';}
        return '<link href="' . $ci->config->item('documentRootPath') . 'assets/css/'.$url.$extention. '" rel="stylesheet" />';
    }
}

if (!function_exists('js')) {

    function js($url) {
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $ci = & get_instance();
        $extention=(end((explode('.',$url)))=='js')?'':'.js';
        if ($scheme == 'http' || $scheme == 'https')
            {return '<script src="' . $url . '"></script>';}
        return '<script src="' . $ci->config->item('documentRootPath') . 'assets/js/'.$url.$extention. '"></script>';
    }
}

if (!function_exists('assets')) {

    function assets($url=NULL) {
        $ci = & get_instance();
        return (trim($url)==NULL)?$ci->config->item('documentRootPath') . 'assets/':$ci->config->item('documentRootPath') . 'assets/'.$url;
    }
}

if (!function_exists('favicon')) {
    function favicon($url) {
                $ci = & get_instance();
        return '<link rel="shortcut icon" type="image/x-icon" href="' . $ci->config->item('documentRootPath') . 'assets/'.$url . '"/>';
    }
}
