<?php
class Home extends Controller {

    function __construct() {
        parent::__construct();
        
    }

    public function view($page = 'index')
    {
      if ( ! file_exists(APPPATH.'views/'.$page.'.php'))
            {
                    show_404();
            }
            $this->load->view($page);
    }

    
}
