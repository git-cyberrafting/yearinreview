<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
         }

    /**
     * Load the user views
     * @param string $main_view
     * @param array $data
     */
       
    protected function loadView($mainView, $dataContainer) {
        $dataContainer=is_object($dataContainer) ? get_object_vars($dataContainer) : $dataContainer;
        $this->load->view('include/head', $dataContainer);
        $this->load->view('include/sideNavigation', $dataContainer);
        $this->load->view('include/header', $dataContainer);
        $this->load->view($mainView, $dataContainer);
        $this->load->view('include/footer', $dataContainer);
        $this->load->view('include/bottom', $dataContainer);
    }
}
