<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Defining CURD function
 *
 * C = create (insert)
 * U = update
 * R = read  (select)
 * D = delete
 */
class MY_Model extends CI_Model {

    protected $primaryTable = '';
    protected $primaryTablePK = '';
    
    public function __construct($primaryTable,$primaryTablePK) {
        parent::__construct();
        $this->primaryTable=$primaryTable;
        $this->primaryTablePK=$primaryTablePK;
    }
    
    function getAll($where=null) {
        if($where){
            return $this->getResult($this->db->where($where)->get($this->primaryTable));
        }
        else {
            return $this->getResult($this->db->get($this->primaryTable)); 
        }
    }
    
    function getOnly($where) {
         return $this->getResult($this->db->where($where)->get($this->primaryTable));
        
    }
     function getAllOrderBy($orderBy) {
        if(is_array($orderBy) && count($orderBy)>0){
             $this->db->order_by($orderBy[0],$orderBy[1]);
        }
        return $this->getResult($this->db->get($this->primaryTable));
    }
    function getOnlyOrderBy($where,$orderBy) {
        $this->db->where($where);
        if(is_array($orderBy) && count($orderBy)>0){
            $this->db->order_by($orderBy[0],$orderBy[1]);
        }
        return $this->getResult($this->db->get($this->primaryTable));
    }
    function getOne($where=null)
    {
        if($where){
            return $this->getRow($this->db->where($where)->get($this->primaryTable));}
        else {
            return $this->getRow($this->db->get($this->primaryTable)); 
        }
    }
    function selectOnly($select,$where) {
       return $this->getResult($this->db->select($select)->where($where)->get($this->primaryTable));
    }
    function selectAll($select,$where=null) {
       if($where){
            return $this->getResult($this->db->select($select)->where($where)->get($this->primaryTable));
        }
        else {
            return $this->getResult($this->db->select($select)->get($this->primaryTable)); 
        }
    }
    function selectOne($select,$where=null)
    {
        if($where){
            return $this->getRow($this->db->select($select)->where($where)->get($this->primaryTable));}
        else {
            return $this->getRow($this->db->select($select)->get($this->primaryTable)); 
        }
    }
    function put($updateData,$where){
       if(is_array($where) && count($where)>0 && is_array($updateData) && count($updateData)>0){
            $this->db->where($where)->update($this->primaryTable,$updateData);
            return $this->checkAction();
        }
        else {
            return false; 
        }
    }
    
    function flush($where){
       if(is_array($where) && count($where)>0){
            $this->db->delete($this->primaryTable);
            return $this->checkAction();
        }
        else {
            return false; 
        }
    }
    
    function push($insertData)
    {
        if(is_array($insertData) && count($insertData)>0){
            $this->db->insert($this->primaryTable,$insertData);
            return $this->pushDB();
        }
        else {
            return false; 
        } 
    }
    
    function has($where=NULL)
    {
        if(is_array($where) && count($where)>0){
           return ($this->db->where($where)->get($this->primaryTable)->num_rows() > 0) ? true : false;
        }
        else {
            return false; 
        }
    }
     
     protected function pushDB() {
        /**
     * Insert data into the database
     *
     * @param type $table_name inserting table name
     * @param type $data inserting data array
     * @return boolean or number
     */
       return ($this->db->affected_rows() > 0) ? $this->db->insert_id() : false;
    }

    

    protected function putDB() {
        /*
     *  check  record updated or not

     * @return boolean
     */
        return $this->checkAction();
    }

    

    protected function flushDB() {
     /*
     * check  record deleted or not
     * @return boolean
     */
        return $this->checkAction();
    }

    private function checkAction() {
        return ($this->db->affected_rows() > 0) ? true : false;
    }

    protected function getResult($query) {
        return ($query->num_rows() > 0) ? (object)$query->result() : false;
    }

    protected function getRow($query) {
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    
    function countAll()
    {
        return $this->db->where($where)->from($this->primaryTable)->count_all_results(); 
         
    }
    function countOnly($where){
        if($where){
            return $this->db->where($where)->from($this->primaryTable)->count_all_results(); 
        }
        else
        {
            return 0;
        }
    }
}
