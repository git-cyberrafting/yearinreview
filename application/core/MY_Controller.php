<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

    public $dataContainer;

    public function __construct() {
        parent::__construct();
        $cnf =& load_class('Config', 'core');
        $this->dataContainer = new stdClass();
	$this->dataContainer->pageTitle=$cnf->config['pageTitle'];
        $this->dataContainer->projectName=$cnf->config['projectName'];
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }

}
