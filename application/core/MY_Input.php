<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input {
    public $projectName;
    
    function __construct() {
        parent::__construct();
        $cnf =& load_class('Config', 'core');
        $this->projectName=$cnf->config['projectName'];
    }
    
    function only($inputs)
    {
       return $this->input_stream($inputs);
    }
     function onlyWith($inputArray)
    {
       $returnInputArray=[];
       if(is_array($inputArray) && count($inputArray)>0){
            if(array_key_exists('with', $inputArray) && is_array($inputArray['with']) && count($inputArray['with'])>0)
            { 
                foreach($inputArray['with'] as $key=>$value)
                {
                    $returnInputArray[$key]=$value;
                }
            }
           
            if(array_key_exists('only', $inputArray) && is_array($inputArray['only']) && count($inputArray['only'])>0){
                foreach($inputArray['only'] as $key)
                {
                    $returnInputArray[$key]=$this->post($key);
                }
            }
         
       }
        return $returnInputArray;
    }
    function all($inputArray=null)
    {
       $returnInputArray=$this->post();
       
        if(is_array($inputArray) && count($inputArray)>0){
            if(array_key_exists('except', $inputArray) && is_array($inputArray['except']) && count($inputArray['except'])>0){
                foreach($inputArray['except'] as $key)
                {
                    unset($returnInputArray[$key]);
                }
            }
            if(array_key_exists('with', $inputArray) && is_array($inputArray['with']) && count($inputArray['with'])>0)
            { 
                foreach($inputArray['with'] as $key=>$value)
                {
                    $returnInputArray[$key]=$value;
                }
            }
            
       }
        
        return $returnInputArray;
    }
    function allExcept($inputArray=null)
    {
        $returnInputArray=$this->post();
        if(is_array($inputArray) && count($inputArray)>0){
            foreach($inputArray as $key)
            {
                 unset($returnInputArray[$key]);
            }
        }
        return $returnInputArray;
    }
    function allWith($inputArray=null)
    {
        $returnInputArray=$this->post();
        if(is_array($inputArray) && count($inputArray)>0){
            foreach($inputArray as $key=>$value)
            {
                $returnInputArray[$key]=$value;
            }
        }
        return $returnInputArray;
    }
    
    
    function makePasswordHash($inputValue){
        $options = ['cost' => 8,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)];
        return password_hash($inputValue, PASSWORD_BCRYPT, $options);
    }
    function verifyPasswordHash($inputValue,$hash){
        return password_verify($inputValue, $hash)?true:false;
    }
    
    
    private function _cookieName($cookieName)
    {
        return ucfirst($this->projectName).ucfirst($cookieName);
    }
    private function _pushCookie($cookieDataArray)
    {
        $cookie=['name'=>$cookieDataArray['name'],
                     'value'=>$cookieDataArray['value']];
        $cookie['expire']= array_key_exists('expire',$cookieDataArray) ? $cookieDataArray['expire']:time()+31556926;
        $cookie['domain']='.'.parse_url($this->projectName, PHP_URL_HOST);
        $cookie['path']= array_key_exists('path',$cookieDataArray) ? $cookieDataArray['path']:'/';
        $cookie['prefix']='';
        $cookie['secure']=False;
        $cookie['httponly']=array_key_exists('path',$cookieDataArray) ? $cookieDataArray['httponly']:TRUE;
               
        $this->set_cookie($cookie['name'],$cookie['value'],$cookie['expire'],$cookie['domain'], $cookie['path'],$cookie['prefix'],$cookie['secure'],$cookie['httponly']);
   
    }
    function pushCookie($cookieDataArray)
    {
        if(is_array($cookieDataArray) &&  !(count(array_filter($cookieDataArray,'is_array'))>0) && count($cookieDataArray)>=2)
        {
           $this->_pushCookie($cookieDataArray); 
        }
        elseif(is_array($cookieDataArray) &&  count(array_filter($cookieDataArray,'is_array'))>0)
        {
            foreach($cookieDataArray as $cookieDataChildArray)
            {
                if(is_array($cookieDataChildArray) && count($cookieDataChildArray)>=2)
                {
                    $this->_pushCookie($cookieDataChildArray); 
                }
            }
        }
        return;
    }
    function getCookie($cookieName)
    {
        $cookieValue=$this->cookie($this->_cookieName($cookieName),true);
        return (($cookieValue!='') &&($cookieValue!=NULL)) ?$cookieValue:'';
    }
    function putCookie($cookieDataArray)
    {
       return $this->pushCookie($cookieDataArray);
    }
    function flushCookie($cookieDataArrayOrString)
    {   
        $cookie['prefix']=''; 
        $cookie['domain']='.'.parse_url($this->projectName, PHP_URL_HOST);
        if(is_array($cookieDataArrayOrString))
        {
            $cookie['name']=$cookieDataArrayOrString['name'];
            $cookie['path']= array_key_exists('path',$cookieDataArrayOrString) ? $cookieDataArrayOrString['path']:'/';
        }
        else {
            $cookie['name']=$cookieDataArrayOrString;
            $cookie['path']= '/'; 
        }
        delete_cookie( $cookie['name'], $cookie['domain'], $cookie['path'], $cookie['prefix']);
        
    }
 
}