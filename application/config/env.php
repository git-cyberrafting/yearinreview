<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Internationalization Details
|--------------------------------------------------------------------------
|
| This determines which country is used.
|
*/
//default country name
$config['defaultCountry'] = "us";
// list of countries in which website redirect
$config['acceptedCountries'] = ['ae','au','ca','gb','hk','in','kw','my','oh','qa','sa','sg','us'];

/*
|--------------------------------------------------------------------------
| Email smtp Details 
|--------------------------------------------------------------------------
|
| This determines which smtp is used for email.
|
*/
$config['smtp_host'] = "mail.herculeanrun.com";
$config['smtp_port'] = "587";
$config['smtp_user'] = "noreply@herculeanrun.com";
$config['smtp_pass'] = "5TVPbV1fg@.c";
//$config['smtp_crypto'] = "ssl"; // tls or ssl defalut null
$config['from_email'] = "noreply@herculeanrun.com";
$config['from_name'] = "Herculeanrun";
$config['reply_email'] = "";
$config['reply_name'] = "";
$config['to_email'] = "contact@herculeanrun.com";
$config['to_name'] = "Herculeanrun";

/*
|--------------------------------------------------------------------------
| Authentication 
|--------------------------------------------------------------------------
|
| This determines which table is used for user or admin login time.
|
*/
// frontend user login table name
$config['loginTable'] = 'tbAdmin';
// frontend user login table primary key
$config['loginTablePrimaryKey'] = 'id';



/*
|--------------------------------------------------------------------------
| Social Api
|--------------------------------------------------------------------------
|
| This determines which which social plugin is used.
|
*/

/*
|--------------------------------------------------------------------------
| Facebook  Api 
|--------------------------------------------------------------------------
|
*/

/*
|--------------------------------------------------------------------------
| Google  Api 
|--------------------------------------------------------------------------
|
*/


/*
|--------------------------------------------------------------------------
| Twitter  Api 
|--------------------------------------------------------------------------
|
*/

/*
|--------------------------------------------------------------------------
| Instagram  Api 
|--------------------------------------------------------------------------
|
*/


/*
|--------------------------------------------------------------------------
| Payment Gateway Api
|--------------------------------------------------------------------------
|
| This determines which payment gateway is used.
|
*/

/*
|--------------------------------------------------------------------------
| paytm  Api 
|--------------------------------------------------------------------------
|
*/

/*
|--------------------------------------------------------------------------
| paypal  Api 
|--------------------------------------------------------------------------
|
*/

/*
|--------------------------------------------------------------------------
| payu money  Api 
|--------------------------------------------------------------------------
|
*/