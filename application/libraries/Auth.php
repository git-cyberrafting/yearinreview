<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

    private $ci;
    private $sessionName;
    private $loginTable;
    private $loginTablePrimaryKey;
    private $isSession;
    function __construct() {
        $this->ci = & get_instance();
        $projectName=str_replace(' ','',$this->ci->config->item('projectName'));
        $this->ci->config->load('env', true);
        $this->loginTable=$this->ci->config->item('loginTable','env');
        $this->loginTablePrimaryKey=$this->ci->config->item('loginTablePrimaryKey','env');
        $this->sessionName=$projectName.ucfirst('user');
        $this->isSession=$this->sessionName.'IsSession';
    }
    
    
    
    function welcome($loginData,$extra=''){
        $password=$loginData['password'];
        unset($loginData['password']);
        $tablePrimaryKey=$this->loginTablePrimaryKey;
        $query=$this->ci->db->limit(1)->where($loginData)->get($this->loginTable);
        
        if ($query->num_rows()===0) {
           return false;
        } 
    
        $dbResult=$query->row();
        
        if(!$this->ci->input->verifyPasswordHash($password,$dbResult->password)){
          unset($dbResult);
          return false;
        }
        unset($dbResult->password);
        $primaryKeyValue=$dbResult->$tablePrimaryKey;
        unset($dbResult->$tablePrimaryKey);
        $dbResult->id=$primaryKeyValue;
        $urlName=array_key_exists('titleName', $dbResult) ? $dbResult->titleName.' ' : '';
        $urlName.=array_key_exists('firstName', $dbResult) ? $dbResult->firstName.' ': '';
        $urlName.=array_key_exists('middleName', $dbResult) ? $dbResult->middleName.' ': '';
        $urlName.=array_key_exists('lastName', $dbResult) ? $dbResult->lastName : '';
        $urlName =array_key_exists('name', $dbResult) ? str_replace(" ","-",$dbResult->name):str_replace(" ","-",$urlName);
        $dbResult->uriName=$urlName;
        $dbResult->name=str_replace("-"," ",$urlName);

        if(isArray($extra)){
            foreach($extra as $key=>$value) {
                $dbResult->$key=$value;
            }
        }

        $isSession=$this->isSession;
        $dbResult->$isSession=TRUE;
        
        $this->ci->session->push($dbResult,$this->sessionName);
        unset($dbResult);
        return true;
    }
    
    
    function has(){
        if($this->ci->session->has($this->sessionName)){
            $isSession=$this->isSession;
            if(array_key_exists($isSession,(array)$this->user()) && $this->user()->$isSession==TRUE){
                 return (trim($this->user()->id)!='' && $this->user()->id>0)?true:false;
            }
        }
        return false;      
    } 
    
    function user(){
        return $this->ci->session->get($this->sessionName);
    }
    
    function goodbye(){
        $this->ci->session->flush($this->sessionName);
    }
  
}

?>