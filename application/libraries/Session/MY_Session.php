<?php                                                                                                                                                                                           
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Session extends CI_Session {

    function __construct() {
        parent::__construct();
    } 

    function push($sessionDataArrayOrObject,$sessionName=''){
        $sessionDataArray=(is_object($sessionDataArrayOrObject))? (array)$sessionDataArrayOrObject:$sessionDataArrayOrObject;
        if($sessionName=='' || trim($sessionName)==''){
            foreach($sessionDataArray as $key=>$value){
               $this->set_userdata($key,$value); 
            }
        }
        $this->set_userdata($sessionName,$sessionDataArray);
    }
     
    function get($sessionName){
       return $this->has_userdata($sessionName)?(is_array($this->userdata($sessionName))?(object)$this->userdata($sessionName):$this->userdata($sessionName)):false;
    }
    
    function has($sessionName){
       return $this->has_userdata($sessionName)?true:false;
    } 
    
    function put($sessionDataArrayOrObject,$sessionName) {
         $this->push($sessionDataArrayOrObject,$sessionName);
    }
    function flush($sessionName) {
        $this->unset_userdata($sessionName);
    }
    function flushAll(){
        $this->sess_destroy();
    }
}  