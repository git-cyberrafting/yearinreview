<?php
class MY_Email extends CI_Email {
    private $ci;
    function __construct(){
        parent::__construct();
        $this->ci = & get_instance();
        $this->ci->config->load('env', true);
    }
    
    function dispatch($to,$subject,$body,$from=NULL,$reply=NULL,$cc=NULL,$bcc=NULL,$attach=NULL){
        $config['protocol'] = "smtp";
        $config['smtp_host'] = $this->ci->config->item('smtp_host','env');
        $config['smtp_port'] = $this->ci->config->item('smtp_port','env');
        $config['smtp_user'] = $this->ci->config->item('smtp_user','env');
        $config['smtp_pass'] = $this->ci->config->item('smtp_pass','env');
        $smtp_crypto=$this->ci->config->item('smtp_crypto','env');
        if(trim($smtp_crypto)!=''){
        $config['smtp_crypto'] = $this->ci->config->item('smtp_crypto','env');
        }
                        
        //$config['validate'] = TRUE;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->initialize($config);
       
        $fromEmail=strtolower((is_array($from) && count($from)>0 && count($from)<=2)?$from[0]:(!empty($from)?$from:$this->ci->config->item('from_email','env')));
        $fromName= ucwords((is_array($from) && count($from)==2)?$from[1]:$this->ci->config->item('from_name','env'));
       
        $this->from($fromEmail,$fromName);
        
        $toEmail=strtolower((is_array($to) && count($to)>0 && count($to)<=2)?
                ((count($to)==1)?$to[0]:
            ((count($to)==2 && (!filter_var($to[1], FILTER_VALIDATE_EMAIL) === false))? $to : $to[0])
            ):$to);
        $toName=ucwords((is_array($to) && count($to)==2)?((!filter_var($to[1], FILTER_VALIDATE_EMAIL) === false)?'':$to[1]):'');
        
        if (!empty($toName)){ $this->to($toEmail,$toName); }
        else{ $this->to($toEmail); }
       
        if(!empty($reply)){
        $replyEmail=strtolower((is_array($reply) && count($reply)>0 && count($reply)<=2)?$reply[0]:(!empty($reply)?$reply:$this->ci->config->item('reply_email','env')));
        $replyName= ucwords((is_array($reply) && count($reply)==2)?$reply[1]:$this->ci->config->item('reply_name','env'));
       
        $this->reply_to($replyEmail,$replyName);
        }
        
        if(!empty($cc)){
        $this->bcc($cc);
        }
        
        if(!empty($bcc)){
        $this->bcc($bcc);
        }
        
        if(file_exists($attach)){
        $this->attach($attach);
        }
        
        $this->subject($subject);
        $this->message($body);
        
        if($this->send()){ return true;}
        else {return false;}
    }

}