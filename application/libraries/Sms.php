<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sms {

    private $ci;
    function __construct() {
        $this->ci = & get_instance();
        $this->ci->config->load('env', true);
    }
   
    function dispatch($mobile,$message){
        $message=urlencode($message);
        $isValidMobile=strlen($mobile);
        if($isValidMobile<8 && $isValidMobile>14){
          return false;  
        }
        $user = $this->ci->config->item('SMS_user','env');
        $password = $this->ci->config->item('SMS_password','env');
        $sender = $this->ci->config->item('SMS_sender','env');
        $priority=$this->ci->config->item('SMS_priority','env');
        $stype=$this->ci->config->item('SMS_stype','env');
        $url='http://bhashsms.com/api/sendmsg.php?user='.$user.'&pass='.$password.'&sender='.$sender.'&phone='.$mobile.'&text='.$message.'&priority='.$priority.'&stype='.$stype;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        if(curl_errno($ch)){
           $error=curl_error($ch);
           return false;
        }
        curl_close($ch);
        return true;
    }
}

?>