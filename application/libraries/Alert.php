<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Alert {

    private $ci;
    private $smileyDanger='&#x1F62D;';
    private $smileySuccess='&#x1F60A;';
    private $smileyInfo='&#x1F60F;';
    private $smileyWarning='&#x1F914;';
    private $headerDanger='Oh snap!';
    private $headerSuccess='Well done!';
    private $headerInfo='Heads up!';
    private $headerWarning='Warning!';
    private $hr='hr.message-inner-separator
{
    clear: both;
    margin-top: 10px;
    margin-bottom: 13px;
    border: 0;
    height: 1px;
    background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
    background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}';
    function __construct() {
        $this->ci = & get_instance();
    }
    
    private function _alertIcon($alertType)
    {
       return ($alertType=='danger')?'<i class="fa fa-thumbs-down" aria-hidden="true"></i>':
            (($alertType=='success')?'<i class="fa fa-thumbs-up" aria-hidden="true"></i>':
            (($alertType=='info')?'<i class="fa fa-info-circle" aria-hidden="true"></i>':
            (($alertType=='warning')?'<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>':''))); 
    }
    private function _alertEmoji($alertType)
    {
       return ($alertType=='danger')?$this->smileyDanger:
            (($alertType=='success')?$this->smileySuccess:
            (($alertType=='info')?$this->smileyInfo:
            (($alertType=='warning')?$this->smileyWarning:''))); 
    }
    private function _alertHeader($alertType)
    {
       return ($alertType=='danger')?$this->headerDanger:
            (($alertType=='success')?$this->headerSuccess:
            (($alertType=='info')?$this->headerInfo:
            (($alertType=='warning')?$this->headerWarning:''))); 
    }
    
    private function _set($alertType,$message)
    {
    $alertMessage= '<style>'.
            $this->hr
    .'</style>
    <div class="alert alert-'.$alertType.' alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    '.$this->_alertIcon($alertType).' <strong>'.$this->_alertHeader($alertType).'</strong>
     <hr class="message-inner-separator">
     <p>'.$message.'</p> </div>';
     $this->ci->session->set_flashdata('alertMessage', $alertMessage);
    }
     private function _setWithoutAll($alertType,$message)
    {
     $alertMessage= '
    <div class="alert alert-'.$alertType.' alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    '.$message.'</div>';
     $this->ci->session->set_flashdata('alertMessage', $alertMessage);
    }
    private function _setWithAll($alertType,$message)
    {
    $alertMessage= '<style>'.
            $this->hr
    .'</style>
    <div class="alert alert-'.$alertType.' alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    '.$this->_alertIcon($alertType).' <strong>'.$this->_alertHeader($alertType).'</strong>
     <hr class="message-inner-separator">
     <p>'.$message.'&nbsp;'.$this->_alertEmoji($alertType).'&nbsp;'.$this->_alertEmoji($alertType).'</p> </div>';
     $this->ci->session->set_flashdata('alertMessage', $alertMessage);
    }
    function danger($message){return $this->_set('danger',$message);}
    function success($message){ return $this->_set('success',$message); }
    function info($message=''){return $this->_set('info',$message); }
    function warning($message=''){return $this->_set('warning',$message);}
    
    function dangerWithoutAll($message){return $this->_setWithoutAll('danger',$message);}
    function successWithoutAll($message){ return $this->_setWithoutAll('success',$message); }
    function infoWithoutAll($message=''){return $this->_setWithoutAll('info',$message); }
    function warningWithoutAll($message=''){return $this->_setWithoutAll('warning',$message);}
    
    function dangerWithAll($message){return $this->_setWithAll('danger',$message);}
    function successWithAll($message){ return $this->_setWithAll('success',$message); }
    function infoWithAll($message=''){return $this->_setWithAll('info',$message); }
    function warningWithAll($message=''){return $this->_setWithAll('warning',$message);}
    function display($openingTag='',$closingTag='')
    {
        if ($this->ci->session->flashdata('alertMessage')){
           echo  $openingTag.$this->ci->session->flashdata('alertMessage').$closingTag;
        }
    }
}

?>