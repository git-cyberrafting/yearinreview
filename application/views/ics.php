<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/otherhead1.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>ics2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 1, FRIDAY, 2<sup>nd</sup> SEPTEMBER 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue1">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue1-stip">
							<span>Session 1 : Cancer Registries</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. P. B. Desai, Dr. Gustad Daver </p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Registries - Mumbai</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mrs. Shravani Koyande</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Registries - Aurangabad</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mrs. Shravani Koyande</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Registries - Pune</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Kalpana Kulkarni</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Registries - Nagpur</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Krishna Kamble</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Rural Cancer Registries</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Rajesh Dixit</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Registries around Nuclear Plants</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. B. Ganesh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Role of counselling in preventive medicine</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Preeti Chhabria</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Population based survival studies</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Rajesh Dixit</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Improving cause of death	certification</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Padmaja Keskar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>NCRP - Role of National Cancer Registration Programme in India</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Speaker: Mrs. Roselind F S</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue1-stip">
							<span>Session 2 : Cancer Sites - 1</span>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Head and Neck Cancers</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Presenter : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Shraddha Shinde</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section2 end-->
					<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Head and Neck Cancers :Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Satish Rao</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Anil D'Cruz</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Dhairyasheel Sawant, Dr. Kirti Chadha, Dr. Supreeta Arya, Dr. Adwaita Gore, Dr. Amitav Shukla, Dr. Krishna Kamble, Dr. Kamlesh Desai, Dr. Suhail Sayed</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Bone and Soft Tissue Sarcomas</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Presenter : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Preeti Gamre</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Bone and Soft Tissue Sarcomas : Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. N. S. Laud, Dr. S. D. Banavali</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Manish Aggarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Ajay Puri, Dr. Vikas Agashe, Dr. Muzammil Shaikh, Dr. Bharat Rekhi, Dr. Siddhartha Laskar, Dr. Shashikant Juvekar, Dr. Tushar Vora, Dr. Nilendu Purandare</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>CNS Tumors</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Presenter : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Prachi Bandekar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>CNS Tumors : 
Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Milind Sankhe</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Rakesh Jalali</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Dattatraya Muzumdar, Dr. Atul Goel, Dr. Purna Kurkure, Dr. Shubhada Kane, Dr. Aadil chagla, Dr. Sona Pungavkar, Dr. Ranjeet Bajpai, Dr. Avinash Deo</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Hematological Malignancies</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Presenter : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Jayashree Jadhav</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Hematological Malignancies :
Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. V. P. Antia, Dr. Tapan Saikia</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Farah Jijina</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Nehal Khanna, Dr. Anil Kamat, Dr. S Sanyal, Dr. Sumeet Gujral,
							Dr. Kalpana Kulkarni, Dr. Suyash Kulkarni, Dr. Pratibha Kadam Amare,Dr. Mamta Manglani</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Hepatobiliary and Colorectal cancers :Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Avinash Supe, Dr. Ameet Mandot</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. P. Jagannath</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 ">
							<p>Dr. Mahesh Goel, Dr. Ganesh Nagarajan, Dr. Sudeep Shah, Dr. Nilesh Doctor, Dr. Sandeep De, Dr. Suyash Kulkarni, Dr. Chandralekha Tampi, Dr. Prachi Patil, Dr. Vikas Ostwal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
				
				<!-- section4-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue1-stip">
							<span>Session 3: Early Detection, Support & Survivorship</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>	Mrs. Sheila Nair, Dr. Purna Kurkure </p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Awareness & Detection</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mrs. Ulka Bangui</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS - Cancer Cure</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mrs. Gouri Raverkar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>ICS Survivorship - Ugam</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Vandana Dhamankar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>V-Care Support</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Speaker: Mrs. Vandana Gupta</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section4-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue1-stip">
							<span>Session 4 : Conference Inauguration - Hotel ITC Grand Central, Parel</span>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>50 years of the Mumbai Cancer Registry</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p> Dr. Vinay Deshmane</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Impact of registry data on public health</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. R. A. Badwe</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Release of data, Q & A</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. R. A. Badwe</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				

			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>