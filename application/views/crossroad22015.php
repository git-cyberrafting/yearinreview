<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/crossroadhead2015.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>crossroad2015" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a><a class="btn-nonactive" href="<?=url('');?>crossroad32015" ><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan-crossroad">DAY 2, SATURDAY, 25<sup>th</sup> JULY 2015,DELHI</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Introduction of Dr. David Warr</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Indian Perspective of Supportive Cancer Care</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Rakesh Roy</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Supportive Cancer Care: Key Note from MASCC</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. David Warr</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Update on Chemotherapy Induced Nausea and Vomiting: Bridging the gap between guidelines and clinical practice</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. David Warr</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion: CINV prophylaxis for all: Myth or Reality?</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Purvish Parikh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion: Chemo Induced Febrile Neutropenia: A Management approach</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Kunal Jain</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				</div>
		</div>
	</section>
	<?php include('include/footer.php');?>