<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/satellitehead.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SATURDAY, 10<sup>th</sup> OCTOBER 2015</span>

			</div>
		</div>
		<div class="container">
			<div class="back-violet">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="violet-stip">
							<span>Session I: Speaker Talk</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Boman Dhabhar and Dr. J.K. Singh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightviolet-stip">
							<span>Role of Endocrine Therapy in Gynaecological Cancers - Dr. T.P. Sahoo</span>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip">
							<span>Role of Endocrine Therapy in Advanced Breast Cancers -</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Chetan Deshmukh </p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="violet-stip">
							<span>Session II: Case based panel discussion on “Emerging use of Endocrine Therapy in Gynaecological Cancers”</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Sudeep Gupta</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sandeep Goyle,Dr. Amita Maheshwari,Dr. Nagraj Huilgol,Dr. Chetan Deshmukh,Dr. T.P. Sahoo</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--scetion2 end-->
				<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="violet-stip">
							<span>Session III: Case based panel discussion on “Optimising role of Endocrine Therapy in Advanced Breast Cancers”</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Ashish Bakshi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Nilesh Lokeshwar,Dr. Adwaita A. Gore,Dr. Muzammil Shaikh,Dr. Pritesh Lohar,Dr. Reetu Jain</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>