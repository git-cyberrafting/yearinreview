<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="healthcare">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/healthcare.png');?>">
		</div>
	</div>
	<div class="container">
		<div class="deepblue-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span style="float:right;margin-right:3em;">Day <a href="<?=url('');?>healthcare" ><img src="<?= assets('images/1.png');?>" style="width:20px"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>" style="width:20px"></a><a href="<?=url('');?>healthcareday3" ><img src="<?= assets('images/3.png');?>" style="width:20px"></a></span>
			<span style="float:right;margin-right:1em;">DAY 2, FRIDAY, 28th JANUARY 2017</span>
		
		</div>
	</div>
	<!-- section1-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>Session 5 : HEALTH COSTS</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Benjamin Anderson, Kailash Sharma</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Studies of Economic Impact of Cancer in LMICs - An Unfinished Agenda</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Ajay Mahal</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Creating Disease Through Screening</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Nikola Biller</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Real worth of high priced drugs</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Tito Fojo</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Debate - Is Healthcare a commodity?</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Yes: David W. Johnson</p>
			</div>
			<div>
				<p class="sentence2">No: Tito Fojo</p>
			</div>
				
		</div>
	</div>
	
	<!-- section 1 end-->

	<!--section2-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 6: CONFERENCE ORATION - 'HEALTHCARE FOR ALL - WHY AND HOW?'</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Shekhar Basu, Rajendra Badwe, Shubhada Chiplunkar, Kailash Sharma</p>
				<p class="sentence2">Introduction: Sudeep Gupta</p>
				<p class="sentence2">Speaker: Amartya Sen</p>
			</div>

		</div>
	</div>

	<!-- section 2 end-->
	<!--section3-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 7: HEALTHCARE SPENDING</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Ajay Mahal, HKV Narayant</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Determinants of National Healthcare Spending</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Nachiket Mor</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Catastrophic Health Expenditure</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Shankar Prinja</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Prioritization of Health Expenditure</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Alok Kumar</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Measuring value for money of healthcare interventions - NICE perspective</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Kalipso Chalkidou</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Does more spending result in better health outcomes?</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: R. A. Badwe</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Nachiket Mor, Shankar Prinja, Alok Kumar, Kalipso Chalkidou,Richard Sullivan, Nathan Cherny, </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Nikola Biller-Andorno and Sujata Saunik (Principal Secretary, Finance, Govt. of Maharashtra)</p>
			</div>
				
		</div>
	</div>
	<!-- section 3 end-->
	<!--section4-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 8: HEALTH FINANCING</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Nathan Cherny, Karine Chevreul</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Financial Risk Protection</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: T. Sundararaman</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Health insurance for vulnerable populations - Policy to implementation</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Taichi Ono</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Determinants and impact of out-of-pocket spending</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Kanchana T. K.</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Alternative Sources of Healthcare Financing</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Milind Barve</p>
			</div>
				
		</div>
	</div>
	
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Ajay Mahal</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : T. Sundararaman, Taichi Ono, Kanchana T.K, Milind Barve</p>
			</div>
				
		</div>
	</div>
	<!-- section 4 end-->
		<!-- section1-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 9: ECONOMICS OF INNOVATION - TECHNOLOGY AND RESEARCH</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: T. P. Lahane, David W. Johnson</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Role of Telemedicine in improving healthcare access</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Sabe Sabesan</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Market vs. Medicine - A Roadmap for Better Future</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: David W. Johnson</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Research (Cost, Focus & Policies) Industry Perspective</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Ranjit Shahani</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Cancer Grid</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: C. S. Pramesh</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Access to specialized knowledge - ECHO initiative</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Sanjeev Arora</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Opportunity Cost of Missed Innovation</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Ajay Bhatnagar</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Sabe Sabesan, David W. Johnson, Ranjit Shahani, Sudeep Gupta,C.S. Pramesh, </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sanjeev Arora, G.V.S. Manyam (Panacea),Terri Bresenham (GE) and Vineet Gupta (Sakra World Hospital)</p>
			</div>
				
		</div>
	</div>
	<!-- section 1 end-->

</section>
<?php include('include/footer.php');?>