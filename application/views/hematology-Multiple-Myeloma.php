<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumor-board">
	<div class="container">
		<div class="col-sm-12 col-md-12 tumor-head">
			<img src="<?= assets('images/tumorboard-slider.png');?>"  alt="tumor board" class="img-responsive">
			<div class="back-gray1">
			<h3> CONFERENCE PROCEDINGS</h3>
			<div class="text-border"></div>
			</div>
		</div>
	</div><hr>
	<div class="container">
		<div class="col-sm-12 col-md-12 abt-breadcrumb">
			<ol class="breadcrumb">
				<li><a href='javascript:void(0)'>HOME</a></li>
				<li><a href='javascript:void(0)'>CONFERENCE</a></li>
				<li class="active">HEMATOLOGY</li> 
				  <li class="active">MULTIPLE MYELOMA</li>     
			</ol>
		</div>
	</div><hr>

	<div class="container">
		<div class="row back-gray-colorectal">
			<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1">
							<a href="<?=url('');?>javascript:void(0)" style="cursor:pointer; color:#000;	" type="button" data-toggle="dropdown">Solid Tumor
							</a>

							<ul class="dropdown-menu">
								<li><span class="caret"></span></li>
								<li><a href="<?=url('');?>solid-tumor" >Colorectal</a></li>
							<li><a href="<?=url('');?>solid-tumor-breast" >Breast</a></li>
							<li><a href="<?=url('');?>solid-tumor-gentiourinary" >Gentiourinary</a></li>
							<li><a href="<?=url('');?>solid-tumor-gynecological" >Gynecological</a></li>
							<li><a href='javascript:void(0)'>Gastric</a></li>
							<li><a href='javascript:void(0)'>Hepato-biliary</a></li>
							<li><a href='javascript:void(0)'>Pancreatic</a></li>
							<li><a href='javascript:void(0)'>Head & Neck</a></li>
							<li><a href="<?=url('');?>solid-tumor-lung" >Lung</a></li>
							<li><a href='javascript:void(0)'>Soft Tissu Sarcoma</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1 dropdownside">
							<a href="<?=url('');?>javascript:void(0)" style="cursor:pointer;" type="button" data-toggle="dropdown">Hematology
							</a>

							<ul class="dropdown-menu">
								<li><span class="caret"></span></li>
								<li><a href='javascript:void(0)'>Leukemias</a></li>
								<li><a href='javascript:void(0)'>Lymphomas</a></li>
								<li><a href="<?=url('');?>hematology-Multiple-Myeloma" >Multiple Myeloma</a></li>
								<li><a href='javascript:void(0)'>MDS</a></li>
								<li><a href='javascript:void(0)'>BMT</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1 ">
							<a href="<?=url('');?>pediatric"  style="cursor:pointer;color:#000" >Pediatric
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1 ">
							<a href="<?=url('');?>javascript:void(0)" style="cursor:pointer;color:#000" type="button" data-toggle="dropdown">Hemato-Oncology
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1 ">
							<a href="<?=url('');?>solid-tumor-tmh"  style="cursor:pointer;color:#000" >TMH
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="col-sm-12 sidenav navbar-nav">
						<div class="dropdown dropborder1 ">
							<a href="<?=url('');?>other"  style="cursor:pointer;color:#000">Others
							</a>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-md-9 col-sm-9 verticalline-confer">
				<div class=" row confer-head">
					<h5>MULTIPLE MYELOMA</h5>
					<div class="confer1-border"></div>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>altitude" ><img src="<?= assets('images/hematology.jpg');?>"></a>
				</div>

			</div>

		</div>
	</div>
</section>

<?php include('include/footer.php');?>