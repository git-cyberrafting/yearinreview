<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/otherhead1.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>ics" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 2, FRIDAY, 3<sup>rd</sup> SEPTEMBER 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue1">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue1-stip">
							<span>Session 5 : Cancer Sites - 2</span>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Genitourinary Cancers</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Presenter : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Rashmi Vagal</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
					<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Genitourinary Cancers : Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. M. R. Kamat, Dr. Rajeev Joshi</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. J. N. Kulkarni</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Mangesh Patil, Dr. Ganesh Bakshi, Dr. Vedang Murthy, Dr. Archie Agarwal, Dr. Palak Popat, Dr. Amit Joshi, Dr. Bijal Kulkarni, Dr. Gagan Prakash, Dr. Santosh Menon, Dr. Mukta Ramadwar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Gynecological Cancers</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Shweta Jadhav</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Gynecological Cancers :Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Yogesh Nandanwar, Dr. Asha Dalal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Hemant Tongaonkar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Navin Bhambhani, Dr. Amish Dalal, Dr. Sudeep Sarkar, Dr. Sarita Bhalerao, Dr. Prasad Dandekar, Dr. Sachin Almel, Dr. Supriya Chopra, Dr. Kedar Deodhar, Dr. Meenakshi Thakur, Dr. Samar Gupte, Dr. Sharmila Pimple</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Breast Cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Mayuri Prabhale</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Breast Cancer :Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. P. B. Desai, Dr. S. H. Advani</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. R. A. Badwe</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Vani Parmar, Dr. Mandar Nadkarni, Dr. Rajiv Sarin, Dr. Shilpa Rao, Dr. Tabassum W., Dr. Sudeep Gupta, Dr. Sarbjeet Arneja Kaur, 
							Dr. Sachin Almel, Dr. Nita Nair, Dr. Raju Wadhwani</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Lung Cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Nagendra Shastri</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Lung Cancer : Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. N M Ramraje, Dr. Jaising Phadtare</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Raman Deshpande</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. R. C. Mistry, Dr. Kumar Prabhash, Dr. Anuradha Chougule, Dr. J P Agarwal, Dr. Sabita Jiwnani, Dr. Rajiv Kaushal, Dr. Shyam Sundar Tampi, Dr. Sanjay Sharma, Dr. Shubash Ramani,Dr. Sanjeev Mehta, Dr. Abhishek Mahajan</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Esophageal and Stomach Cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Pooja Manchekar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Esophageal and Stomach Cancer : Trends and panel discussion on prevention, early diagnosis and improving outcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. L. J. de Souza, Dr. Kamran Khan</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Sanjay Sharma</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Shailesh Shrikhande, Dr. Prasanna Shah, Dr. Deepak Chhabra,Dr. Mehul Bhansali, Dr. Chetan Kantharia, Dr. Pranav Chandra, Dr. Munita Bal, Dr. Santosh Gupta, Dr. Niradh Mehta, Dr. Vashishth Maniar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Lymphomas</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Kalpesh Malhari</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Lymphomas :</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Asha Kapadia, Dr. R. Gopal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. B. K. Smruti</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Boman Dhabar, Dr. Navin Khattry, Dr. Anjana Sainani, 
Dr. Maheboob Basade, Dr. Farah Jijina, Dr. Anita Borges, Dr. Nilesh Sable, 
Dr. Santosh Gupta, Dr. Niradh Mehta, Dr. Venkatesh Rangarajan</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>