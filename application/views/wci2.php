<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/wcihead.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>wci" ><img src="<?= assets('images/1.png');?>"></a><a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 2, SUNDAY, 11<sup>th</sup> OCTOBER 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-purple">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightpurple-stip">
							<span>Medical Ethics in Clinical Practice</span>
						</div>
						<div>
							<p class="sentence">Speaker : Sarbani Laskar</p>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="purple-stip">
							<span>Session V: Endometrium</span>
						</div>
						<div>
							<p class="pink mlr1">Chairpersons : Christine Meder, Rajendra Kerkar, Amrita Misri</p>
						</div>	
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Surgery in early endometrial cancer: Audit of quality across Indian Centres</span>
						</div>

						<div>
							<p class="sentence">Speaker : Latha Subramaniam</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Use of Vaginal BT alone for early endometrial cancer: Survey of practice in tertiary cancer centre</span>
						</div>

						<div>
							<p class="sentence">Speakers : Shalini Singh</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Adjuvant Chemotherapy for locally advanced endometrial cancer: Sequencing, Compliance and Outcomes</span>
						</div>

						<div>
							<p class="sentence">Speakers : Rejiv Rajendranath</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Session VI: Ovary</span>
							</div>
							<div>
							<p class="pink mlr1">Chairpersons : Tarini Sahoo, Vijay Kumar</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Epithelial ovarian cancer: Ensuring quality in adjuvant chemotherapy</span>
						</div>

						<div>
							<p class="sentence">Speakers : Senthil Rajappa</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Does Sequencing of chemotherapy surgery impact outcomes of ovarian cancers</span>
						</div>

						<div>
							<p class="sentence">Speakers : Hemant Tongaonkar</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				
				<!-- section2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Should all patients with high grade serous ovarian cancer be referred for genetic testing</span>
							</div>
							<div>
							<p class="pink mlr1">Chairpersons : Sudeep Gupta</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightpurple-stip">
						<span>Minimal Surgical Standards in Ovarian Cancer: Consensus Panel</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator : Rajendra Kerkar</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Rama Joshi, Abraham Peedicayil, Vijay Kumar,Yogesh Kulkarni, Vivek Nama</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
				<!-- section4 end-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Session VII: Survivorship and Fertility Preservation</span>
							</div>
							<div>
							<p class="pink mlr1">Chairpersons : Amita Maheshwari, Shyam Shrivastava</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Chemotherapy induced Peripheral Neuropathy: Institutional Audit</span>
						</div>

						<div>
							<p class="sentence">Speakers : Swarnabindu Banerjee</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Swarnabindu Banerjee</span>
						</div>

						<div>
							<p class="sentence">Speakers : Supriya Chopra</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightpurple-stip">
						<span>Panel Dicussion: Fertility Preservationin Gynecological Cancers</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator : Amita Maheshwari</p>
						<p class="mlr2" style="color:#330033">Panelists :Kaustav Talapatra, Pinky Shah, Christine Meder,Bhupesh Goyal, Seema Gulia, Monisha Gupta</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>