<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/metronomic_head.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn3">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>metronomic" ><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>metronomic2" ><img src="<?= assets('images/2.png');?>"  ></a><a href='javascript:void(0)'><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan-metronomic">DAY 3, SUNDAY, 8<sup>th</sup> MAY 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-grey">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip">
								<span>Interesting Case Capsules</span>
							</div>	
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightgrey-stip">
							<span>GI Cancer Case</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Vikas Ostwal, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Lymphoma Case</span>
						</div>

						<div>
							<p class="sentence">Speaker: Dr. Bhausaheb Bagal, Mumbai</p>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Session 1: Symposium On Pediatric Oncology</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metro-SFCE01 & Ongoing Trials in the SFCE</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Nicolas Andre, France</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Theranostics and Metronomic approach to relapse refractory paediatric solid tumors</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Jaroslav Sterba, Czech Republic</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>A Phase II trial of 5-drug oral antiangiogenic regimen in children with recurrent or progressive tumors</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Mark Kieran, USA</p>
						</div>		
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>DFMO therapy in high risk Neuroblastoma</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Giselle Saulnier Sholler, USA</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Results of metronomic principles based I-BFM LL09 protocol for lymphoblastic lymphomas</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Jaroslav Sterba, Czech Republic</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section2 end-->
		<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion : Metronomic in Paediatric Oncology</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Dr. Brijesh Arora, Mumbai</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Dr. Mark Kieran, USA ; Dr. Jaroslav Sterba, Czech Republic ;Dr. Giselle Saulnier Sholler, USA ;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dr. Sameer Bakshi, New Delhi; Dr. Nicolas Andre, France; Dr. Tushar Vora, Mumbai; </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr. Gaurav Narula, Mumbai; Dr. Girish Chinnaswamy, Mumbai</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
				<!-- section4 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>