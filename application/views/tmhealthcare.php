<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="healthcare">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/healthcare.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>tmhealthcare2" ><img src="<?= assets('images/2.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>tmhealthcare3" ><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan">DAY 1, FRIDAY, 27<sup>th</sup> JANUARY 2017</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 1: HEALTHCARE SYSTEMS IN SELECTED COUNTRIES</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Kim Lyerly, Suleman Merchant</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Brazil</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Bernard Couttolenc</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Thailand</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Somsak Chunharas</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Zambia</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Kennedy Lishimpi</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Japan</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ryozo Matsuda</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Iran</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Maziar Moradi-Lakeh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - France</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Karine Chevreul</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>National Health System - Cuba</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Osvaldo Garcia Gonzalez</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel discussion</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Mary Denise Leonard</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Bernard Couttolenc, Somsak Chunharas, Kennedy Lishimpi, Ryozo Matsuda,
Maziar Moradi-Lakeh, Karine Chevreul, Osvaldo Garcia Gonzalez,
Reza Salim (Amader Gram, Bangladesh)</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section 2 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 2: SUSTAINABLE MODELS OF HEALTH INSTITUTIONS</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Somsak Chunharas, Vijay Satbir Singh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Public Hospital Model </span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Sanjay Oak	</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>The HCG model of healthcare</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>AjaiKumar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>TMC Model</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>R. A. Badwe</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>CMC Vellore Model</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Sunil Chandy</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Aravind Eye Care Model</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>P. Namperumalsamy</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion - Scalability, Growth and Affordability of Institution Models</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Benjamin Anderson</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Sanjay Oak, AjaiKumar, R. A. Badwe, Sunil Chandy,J P Gupta (Commissioner Health, Gujarat), Avinash Supe (Dean, KEM),P. Namperumalsamy</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--section 2 end-->
				<!-- section 3 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 3: EFFICACY ENDPOINTS IN HEALTHCARE DELIVERY</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Anil Srivastava</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Yardsticks for Evaluating Healthcare Delivery</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p> Richard Sullivan</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Universal Health Coverage in India - Service Delivery</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p> Chandrakant Lahariya</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion - Healthcare auditing</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Karine Chevreul</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Richard Sullivan, Chandrakant Lahariya,Abha Mehndiratta (Consultant, Global Health and Development Group),Maturin Tchoumi (Roche, India),
							Vikram Gota (Clinical Pharmacologist, ACTREC)</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--section 3 end-->
				<!-- section 4 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 4: ACCESS TO AFFORDABLE CARE</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Sunil Chandy, Pallavi Govil</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>The Ethical Imperative of Accessible and Affordable Health Care</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Nathan Cherny</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Delivering High Quality Affordable Cancer Care Globally</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Benjamin Anderson</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Facilitating worldwide access to affordable medicines</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Yusuf Hamied</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Multilevel Interventions to Reduce Health Disparities</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>K. V. Viswanath</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Healthcare Access - State Government Initiatives</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Vini Mahajan</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Accessible Healthcare - Medical Regulators role</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Shiv Sarin</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion - Healthcare Access</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>K. V. Viswanath</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Nathan Cherny, Benjamin Anderson, Yusuf Hamied, Vini Mahajan,
							Shiv Sarin, Taichi Ono (Professor, GRIPS), Anil Srivastava</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--section 4 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>