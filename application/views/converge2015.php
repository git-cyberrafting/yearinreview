<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/convergehead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>converge20152" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 1, SATURDAY, 22<sup>nd</sup> AUGUST 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-violet">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightviolet-stip bottom10">
							<span>Welcome address</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Rajat Dasgupta</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Opening talk- Breast Cancer Management in India: Past and Future</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ajai Kumar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion: What is aggressive MBC?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Senthil R</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sachin Hingmire, Dr. Shekar Kesri, Dr. K N Srinivasan, Dr. T P Sahoo,Dr. Manohar Chari, Dr. Pawan Aggarwal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion: Case based Discussion on the Management of Aggressive MBC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. T.P. Sahoo</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Adwaita Gore, Dr. Manisha Singh, Dr. Shekar Patil, Dr. Joanne Blum,
							Dr. Bibek Acharya, Dr. Tapti Sen</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Optimizing the Management of Aggressive MBC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Joanne Blum</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Abraxane - Clinical Experience in Aggressive MBC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. Jyoti Bajpai, Dr. Hassan Jaffar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion: TNBC - Is Hope Around the Corner?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Chanchal Goswami</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. A K Malhotra, Dr. Anita Ramesh,
Dr. Shekar Salkar, Dr. S S Nirni, Dr. Rahul Jaiswal, Dr. Vikas Gowsami,
Dr. V Srinivasan</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stipbottom10">
							<span>Hereditary Breast Cancers - Diagnosis and Genetic Counselling</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>  Dr. Joanne Blum</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stipbottom10">
							<span>Panel Discussion: Medico-legal Implications in Day-to-Day Practice</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Purvish Parikh</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Indibor Singh, Dr. Suresh Babu, Dr. Kislay Dimri, Dr. Prasenjit Chatterjee, Dr. Jyoti Wadhwa, Dr. Anish Maru</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stipbottom10">
							<span>Oral Presentation of Three Best Posters: Novel Trials Proposed on Aggressive MBC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Chitresh Agrawal, Dr. Gita Bhat</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>