<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/gynhead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>gynecology2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 1, SATURDAY, 7<sup>th</sup> JANUARY 2017</span>
		</div>
	</div>
	<div class="container">
		<div class="back-grey">
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 1 : Surgical Videos</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Arun Behl, Prachi Gujrathi</p>
						</div>
						</div>
						<div>
							<p class="sentence1">Pelvic Exenteration</p>
						</div>	
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Chitra Thara</span>
					</div>
					<div>
					<p class="sentence">Upper Abdominal Surgery for Ovarian Cancer</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Amish Dalal</span>
					</div>
					<div>
					<p class="sentence">Lararoscopic Radical Hysterectomy + BPLND</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Subramanyeswara Rao</span>
					</div>
					<div>
					<p class="sentence">Radical Vulvectomy +GND</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Sushil Kumar Giri</span>
					</div>
					<div>
					<p class="sentence">Laprascopic Management of ADNEXAL Mass</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Neeta Warty</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
		
		<!--section 2-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 2 : OVARY-I</span>
						</div>
						<div>
							<p class="sentence">Global trends and predictions in ovarian cancer mortality annals of Oncology</p>
						</div>
						<div >
							<p class="author">Author: M. Malvezzi</p>
						</div>
						<div>
							<p class="sentence">Meta-analysis Of The Randomized Eortc And Chorus Neoadjuvant Versus Primary Debulking Trials In Advanced Tubo-ovarian</p>
						</div>
						<div >
							<p class="author">Author: Vergote</p>
						</div>
						<div>
							<p class="sentence">Matthew Stenger SGO and ASCO Clinical Practice Guideline on Neoadjuvant Chemotherapy for Newly Diagnosed Advanced Ovarian Cancer</p>
						</div>
						<div>
							<p class="author">Author: Matthew Stenger</p>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Samar Gupte</span>
					</div>
					<div>
					<p class="sentence">Tumor-infiltrating lymphocytes (TILs) and PDL1 expression in ovarian cancer (OC): Evolution with neoadjuvant chemotherapy (NCT) and prognostic value</p>
					</div>
					<div>
							<p class="author">Author: Soizick Mesnag</p>
					</div>	
					<div>
					<p class="sentence">"Molecular characterization and comparison of epithelial ovarian carcinoma (EOC) and primary peritoneal carcinoma (PCC)"</p>
					</div>
					<div>
							<p class="author">Author: A.M. Knipprat</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Santosh Menon</span>
					</div>
					<div>
					<p class="sentence">Serous ovarian and primary peritoneal cancers: A comparative analysis of clinico-pathological features, molecular subtypes and treatment outcome</p>
					</div>
					<div>
							<p class="author">Author: Bo Gao</p>
					</div>	
					<div>
					<p class="sentence">A Vanderstichele Chromosomal instability in cell-free DNA as a highly specific biomarker for detection of ovarian cancer in women with adnexal masses</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Rahul Roy Chowdhury</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
		<!-- section2 end-->

		<!-- section3-->
		<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion</span>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Yogesh Kulkarni</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Hemant Tongaonkar, Ashish Bakshi, Anil Heroor, Col. Tony Jose,</br>Kedar Deodhar, Pritha Ray, Sanket Mehta, Sabhyata Gupta</p>
						</div>	
						</div>
					<div>
					<p class="sentence">Targeted Therapy In Ovarian Cancer</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Krishnansu Tewari</span>
					</div>
					<div>
					<p class="sentence">Weekly vs. Every-3-Week Paclitaxel and Carboplatin for Ovarian Cancer</p>
					</div>
					<div>
							<p class="author">Author: Chan JK</p>
					</div>	
					<div>
					<p class="sentence">Prognostic and predictive blood-based biomarkers (BMs) in patients (pts) with advanced epithelial ovariancancer (EOC) treated with carboplatinpaclitaxel (CP) Â± bevacizumab (BEV)</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Rejiv Rejindranath</span>
					</div>
					<div>
					<p class="sentence">"Bevacizumab after bevacizumab in platinum sensitive recurrent ovarian cancer: A subgroup analysis of GOG0213"</p>
					</div>
					<div>
							<p class="author">Author: Robert L. Coleman</p>
					</div>	
					<div>
					<p class="sentence">chemotherapy in ovarian, fallopian tube, and peritoneal carcinoma NCI-supplied agent(s): A GOG/NRG trial (GOG 252) NCT01167712 a GOG/NRG trial (gog 252). Presented at: 2016 SGO Annual Meeting. March 19-22, 2016. San Diego, CA</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Bharath Rangarajan</span>
					</div>
					<div>
					<p class="sentence">Overall survival in patients with platinum- sensitive recurrent serous ovarian cancer receiving olaparib
					maintenance monotherapy: an updated analysis from a randomised, placebo- controlled, double-blind, phase 2 trial. Lancet Oncol. 2016 Nov;17(11):1579-1589.</p>
					</div>
					<div>
							<p class="author">Author: Ledermann JA</p>
					</div>	
					<div>
					<p class="sentence">Topotecan (T) Â± sorafenib (S) in platinumâ€resistant ovarian cancer (PROC): A double-blind placebo-controlled randomized NOGGO-AGO intergroup Trial-TRIAS</p>
					</div>
					<div>
							<p class="author">Author: Jalid Sehouli</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row--> 
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Tejinder singh</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
		<!-- section3 end-->

		<!-- section4-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 3 : OVARY-II Systemic Therapy</span>
						</div></br>	
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion on HER2+Ve Breast Cancer</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator: Senthil Rajjapa</p>
						<p class="mlr2" style="color:#330033">Panelists :Nilesh Lokeshwar, Jaya Ghosh, Bhavna Parikh, Sevanty Limaye, Manish Kumar,Shailesh Bondarde</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<!-- section4 end-->

		<!-- section5-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 4 : OVARY-III- Rare Subtypes</span>
						</div>
						<div>
							<p class="pink mlr1">Chairpersons : Maheboob Basade, Bhavesh Parekh</p>
						</div>
						<div>
							<p class="sentence1">Randomized Phase III Trial of Irinotecan Plus Cisplatin Compared With Paclitaxel Plus Carboplatin As First-Line Chemotherapy for Ovarian Clear Cell Carcinoma: JGOG3017/GCIG T</p>
						</div>	
						<div>
							<p class="author">Author: Sugiyama T</p>
					</div>
					<div>
						<p class="sentence">Response to chemotherapy in relapsed low-grade serous ovarian</p>
					</div>	
					<div>
						<p class="author">Author: J.M. McLachlan</p>
					</div>
					<div>
						<p class="sentence">Hormonal maintenance therapy for women with low grade serous carcinoma of the ovary or peritoneum</p>
					</div>	
					<div>
						<p class="author">Author: David Marc G</p>
					</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: T P Sahoo</span>
					</div>
					<div>
					<p class="sentence">"Hormonal therapy for epithelial ovarian cancer: a systematic review and meta-analysis of phase II studies</p>
					</div>
					<div>
							<p class="author">Author: L. Paleari</p>
					</div>	
					<div>
					<p class="sentence">Paclitaxel, Ifosfamide, and Cisplatin Efficacy for First-Line Treatment of</p>
					</div>
					<div>
							<p class="author">Author: D. R. Feldman</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Bharat Bhosle</span>
					</div>
					<div>
					<p class="sentence">Salvage high-dose chemotherapy in female patients with relapsed/refractory germ cell tumors: The EBMT experience</p>
					</div>
					<div>
							<p class="author">Author: U De Giorgi</p>
					</div>	
					<div>
					<p class="sentence">"Ovarian granulosa cell tumours: hormone receptor positivity and response to aromatase inhibitors"</p>
					</div>
					<div>
							<p class="author">Author: K. Herring</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Kaushal Patel</span>
					</div>
					<div>
					<p class="sentence">Ovarian cancer : Atleast 5 different diseases</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Jamie Prat</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Sudeep Gupta</p>
						<p class="mlr2" style="color:#330033">Panelists :Chetan Deshmukh, Vineet Talwar,Sandeep Batra, Jyoti Bajpai, Rupendra </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sekhon, Randeep Singh, Bharat Rekhi, Satinder Kaur</p>
					</div>
					<div>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<!-- section5 end-->
		<!-- section6-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 5: OVARY-IV Germline and Somatic Mutations and Their Clinical Implications</span>
						</div></br>
						<div>
							<p class="sentence1">"Incidence of germline mutations in risk genes including BRCA1/2 in consecutive ovarian cancer (OC) patients (AGO TR1)"</p>
						</div>	
						<div>
							<p class="author">Author: Philipp Harter</p>
					</div>
					<div>
						<p class="sentence">"Prevalence of somatic mutations in risk genes including BRCA1/2 in consecutive ovarian cancer patients (AGOTR1 study)"</p>
					</div>	
					<div>
						<p class="author">Author: Eric Hahnen</p>
					</div>
					<div>
						<p class="sentence">Randomized Noninferiority Trial of Telephone Delivery of BRCA1/2 Genetic Counseling Compared With In-Person Counseling: 1-Year Follow Up</p>
					</div>	
					<div>
						<p class="author">Author: Anita Y. Kinney</p>
					</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Anupama Rajanbabu</span>
					</div>
					<div>
					<p class="sentence">SOLO3: A randomized phase III trial of olaparib versus chemotherapy in platinum-sensitive relapsed ovarian cancer patients with a germline mutation (gm)</p>
					</div>
					<div>
							<p class="author">Author: Elizabeth S. Lowe</p>
					</div>	
					<div>
					<p class="sentence">Niraparib Maintenance Therapy in Platinum-Sensitive, Recurrent Ovarian Cancer. ENGOT-OV16/NOVA Investigators. Niraparib Maintenance Therapy in Platinum-Sensitive, Recurrent Ovarian Cancer</p>
					</div>
					<div>
							<p class="author">Author: Mirza MR</p>
					</div>
					<div>
					<p class="sentence">Overall survival in patients with platinum-sensitive recurrent serous ovarian cancer receiving olaparib maintenance monotherapy: an updated analysis from a randomised, placebo-controlled, double blind, phase 2 trial</p>
					</div>
					<div>
							<p class="author">Author: Ledermann JA</p>
					</div>
					<div>
					<p class="sentence">"Clinical activity of the poly(ADPâ€ribose) polymerase (PARP) inhibitor rucaparib in patients (pts) with high-grade ovarian carcinoma (HGOC) and a BRCA mutation (BRCAmut): Analysis of pooled data from Study 10 (parts 1, 2a, and 3) and ARIEL2 (parts 1 and 2)"</p>
					</div>
					<div>
							<p class="author">Author: R.S. Kristeleit</p>
					</div>
					<div>
					<p class="sentence">Homologous recombination deficiency score shows superior association with outcome compared with its individual score components in platinum-treated serous ovarian cancer</p>
					</div>
					<div>
							<p class="author">Author: G.B. Millsa</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Shona Nag</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
			<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Amit Agarwal</p>
						<p class="mlr2" style="color:#330033">Panelists :Chanchal Goswami, Sandeep Goyle, Jamie Prat, Krishnansu Tewari, </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manish Singhal, Bhagyalakshmi,Sailakshmi Daayana</p>
					</div>
					<div>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<!-- section6 end-->


		</div>
	</div>
</section>
<?php include('include/footer.php');?>