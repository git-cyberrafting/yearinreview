<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="about-us">
	<div class="container">
	<div class="back-gray2">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<h3> ABOUT YEAR IN REVIEW</h3>
			<div class="text-border"></div>
		</div>
		</div>
	</div><hr>
	<div class="container">
	<div class="col-sm-12 col-md-12 col-xs-12 abt-breadcrumb">
		<ol class="breadcrumb">
			<li><a href='javascript:void(0)'>HOME</a></li>
			<li class="active">YIR 2017</li>
			<li class="active">ABOUT YEAR IN REVIEW</li>        
		 </ol>
	 </div>
	</div><hr>
	<div class="container">
	<div class="back-gray">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<p class="abt">
				<b>Women's Cancer Initiative - Tata Memorial Hospital and Nag Foundation</b> Invites you for the <b>"2nd Year in Review conference on Breast Cancer"</b>. This conference will be held on Sat 21st and Sun 22nd Jan 2017, at Tata Memorial Hospital, Mumbai.
			</p>
			<p class="abt">
				As you are aware "Year in Review" is an idea that we have harbored in 2015, which subsequently led to a fruitful and successful organization of the 1st year in Review conference in Jan 2016. 
			</p>
			<p class="abt">
				Breast cancer is not so much a disease as a universe - endlessly complex, huge and continuously evolving. Every year a large number of clinical, translational and basic studies are presented at many conferences, at least some of which have an impact on practice. It is becoming increasingly difficult for even specialized oncologists to keep pace with this explosion. Number of meetings in Best of format now try to make this information digestible to practicing clinicians.
			</p>
			<p class="abt">
				Our idea is to recapitulate the best breast cancer science every year in a Year in Review format. The theme of this meeting is "Breast Cancer...the year that was 2016". We strive to recapitulate the best original science presented at the following conferences of the immediately preceding year: 
			</p><br/>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="aacr">AACR </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="asco">ASCO </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="ebcc">EBCC </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="esmo">ESMO </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="sabcs">SABCS </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="astro">ASTRO </p>
			<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="estro">ESTRO </p><br/>
		</div>
	</div>
	</div>
	<div class="black-strip">
		<div class="container">
			<div class="col-sm-12 col-sm-offset-2 col-md-12 col-xs-12">
				<p style="margin-bottom: 3px; color:#fff;margin-left: -5em;">The meeting will be broadly classified in the following sessions:</p>
			</div>
		</div>
	</div><br/>
	<div class="container">
	<div class="back-gray1">
	<div class="col-sm-12 col-md-12 col-xs-12">
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="Loco-regional therapy">Loco-regional therapy </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="Estrogen receptor positive disease">Estrogen receptor positive disease </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="HER2 positive disease">HER2 positive disease </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="Triple Negative Breast Cancer">Triple Negative Breast Cancer </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="Translational science">Translational science </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="State-of-the-Science talk(s)">State-of-the-Science talk(s) </p>
		<p class="abticon"><img src="<?= assets('images/arrow.png');?>"  alt="Rapid review session">Rapid review session </p><br/>
		<p class="abt" style="margin-top: -23px;">
			<p class="abt">
				In the above sessions, we intend to cover surgical, radiation, medical, pathology, biomarkers and imaging issues making this meeting a truly multi-disciplinary one. At the end of every session there will be rapid review presentation on additional abstracts. The selection of abstracts is based on originality, innovation and potential impact on clinical practice.
			</p>
			<p class="abt">
				We believe that presentation of the most impactful and important abstracts from all important conferences in the preceding year will give the participants a feel of the landscape of breast cancer research, especially that impacts practice.
			</p>
			<p class="abt">
				The meeting proceedings of the 1st Year in Review Conference are fully archived on www.yearinreview.in.
			</p>
			<p class="abt">
				<b>For online registration please visit</b> <a href="<?=url('');?>http://in.eregnow.com/ticketing/register/message/YIR2017">http://in.eregnow.com/ticketing/register/YIR2017.</a> 
			</p>
			<p class="abt">We hope to benefit from your partnership and wisdom in initiating, undertaking and nurturing this idea.</p>
			<p class="abt">With best wishes</br>
			<p class="abt" style="margin-top: -11px;"><b>Organizing Chairperson:</b></p>
			<div class="col-sm-12 col-md-12 col-xs-12">
			<div class="col-sm-6" style="margin-left: -29px;">
			<p class="abt">Dr.Sudeep Gupta </br>Professor of Medical Oncology </br>Tata Memorial Centre </br>Mumbai</br>sudeepgupta04@yahoo.com</p>
			</div>
			<div class="col-sm-6">
			<p class="abt">Dr.Shona Nag </br>Consultant Medical Oncologist</br>Jehangir Hospital 
 			</br>Pune</br>shonanag3@gmail.com</p>
	</div>
	</div>

	</div>
</section>

<?php include('include/footer.php');?>