<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/metronomic_head.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn3">&nbsp;DAY &nbsp; <a class="btn-nonactive"
			href="<?=url('');?>metronomic" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a><a class="btn-nonactive" href="<?=url('');?>metronomic3" ><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan-metronomic">DAY 2, SATURDAY, 7<sup>th</sup> MAY 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-grey">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightgrey-stip">
							<span>Welcome, Introduction & Inauguration:
							Dr. Anil Kakodkar; Dr. G N Singh; Dr. RA Badwe; Dr. Shripad Banavali; Dr. Kailash Sharma</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomics Scheduling - The Next Generation of Multi-Target Therapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Shripad Banavali, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomic Therapy and Immunity</span>
						</div>

						<div>
							<p class="sentence">Speaker: Dr. Francesco Bertolini, Italy</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Plenary Talk - 1</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomic Chemotherapy: (re)assessing its Anti-tumor Mechanisms</span>
						</div>
						<div>
							<p class="sentence">Speaker: Prof. Robert Kerbel, Canada</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section2 end-->
				<!-- section3-->
						<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Session 1: Symposium on Breast Cancer</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomic chemotherapy in Breast cancer: The Italian Experience</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Antonella Palazzo, Italy</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomic chemotherapy in Breast cancer: The Indian Experience</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Shripad Banavali, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section4-->
						<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Best paper - 1</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>A Phase II study of oral metronomic combination therapy in relapsed epithelial ovarian cancer:</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Gautam Goyal, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Best paper - 2</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Oral Metronomic therapy in metastatic triple negative breast cancer</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Hitesh Khatwani, Ahmedabad</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion : Metronomic In Breast Cancer</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator: Dr. Sudeep Gupta, Mumbai</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Dr. F Bertolini, Italy; Dr. Yuval Shaked, Israel; Dr. Lovenish Goyal,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Haryana; Dr. Amit Agarwal, New Delhi;Dr. Santam Chakraborty, Mumbai;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr. Mandar Nadakarni, Mumbai; Dr. Govind Babu, Bangalore; Dr. Shekhar Patil, Bangalore; Dr. Ashish Bakshi, Mumbai</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
				<!-- section4 end-->
				<!-- section5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Head & Neck Cancer - Mini Symposium</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Adjuvant & Neoadjuvant Metronomic Chemotherapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Amit Joshi, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Palliative Metronomic Therapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr Vijay Patil, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Best Paper - 1</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>A Prospective Randomized Phase II Study Comparing Metronomic Chemotherapy With Chemotherapy (single Agent Cisplatin), In Patients With Metastatic, Relapsed Or Inoperable Squamous Cell Carcinoma Of Head And Neck</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Vanita Noronha, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Best paper - 2</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Outcomes of Operable Oral Cavity Cancer in Rural India: A Retrospective Study</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. A Pandey</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion : Metronomic Chemotherapy in Head & Neck Cancer</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Dr. Kumar Prabhash, Mumbai</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Dr. Nilendu Purandare, Mumbai; Dr. Gaurav Gupta, Lucknow; Dr. Vamshi Muddu, Hyderabad;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dr. Nikhil Ghadyalpatil, Hyderabad; Dr. Supreeta Arya, Mumbai; Dr. Devender Chaukar, Mumbai; </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr. J K Singh, Patna; Dr. Sudhir Nair, Mumbai;Dr Vedang Murthy, Mumbai; Dr. Chetan Deshmukh, Pune;</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>2nd BARTON KAMEN/MGHI Prize</span>
						</div>
						<div>
							<p class="sentence">Speaker:Dr. Andre Nicolas, France; Dr. Eddy Pasquier, France;Dr. Giselle Saulnier Sholler, USA</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Metronomic Therapies As A Strategy To Handle Tumor Heterogeneity</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Nicolas Andre, France</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Regulation Of The Tumor Micro-environment With Resolvins</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Mark Kieran, France</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Commitment Of Corporates For Metronomic Concept</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Mubarak Naqvi, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Debate : Metronomic as Maintenance Therapy in Solid Cancers</span>
						</div>
						<div>
							<p class="sentence">In favour : Dr. T. P Sahoo, Bhopal</p>
						</div>
						<div>
							<p class="sentence">Against : Dr. Senthil Rajappa, Hyderabad</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section5 end-->

			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>