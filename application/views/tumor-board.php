<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumor-board">
	<div class="container">
		<div class="col-sm-12 col-md-12 tumor-head">
			<img src="<?= assets('images/tumorboard-slider.png');?>"  alt="tumor board" class="img-responsive">
			<div class="back-gray1">
			<h3>TUMOR BOARD</h3>
			<div class="tumor-border"></div>
			</div>
		</div>
	</div><hr>
	<div class="container">
		<div class="col-sm-12 col-md-12 abt-breadcrumb">
			<ol class="breadcrumb">
				<li><a href='javascript:void(0)'>HOME</a></li>
				<li class="active">TUMOR BOARD</li>        
			</ol>
		</div>
	</div><hr>
	<div class="tumor-image">
	
		<div class="container">
		<div class="back-gray">
			<div class="col-md-2 hidden-sm"></div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
					<a href="<?=url('');?>tbk1" ><img src="<?= assets('images/tumorboard1.png');?>"  alt="breast cancer" class="img-responsive"></a>
				</div>
			</div>
			
			<div class="col-md-2 hidden-sm"></div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
				<a href="<?=url('');?>tbk2" ><img src="<?= assets('images/tumorboard2.png');?>"  alt="indo British" class="img-responsive"></a>
			</div>
			</div>
			
			<div class="col-md-2 col-sm-12 hidden-sm">	</div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
				<a href="<?=url('');?>#" ><img src="<?= assets('images/tumorboard3.png');?>"  alt="lung cancer" class="img-responsive"></a>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>


<?php include('include/footer.php');?>