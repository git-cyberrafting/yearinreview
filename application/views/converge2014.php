<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/converge2014-head.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>converge20142" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 1, SATURDAY, 20<sup>th</sup> SEPTEMPER 2014</span>
		</div>
		</div>
		<div class="container">
			<div class="back-violet">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightviolet-stip bottom10">
							<span>Welcome </span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Mr. Praveen Bose / Mr. Shukrit Chimote</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Breast Cancer Status in India</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. AK Vaid</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Recent Advances Session 1: Diagnostic Imaging of Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Selvi Radhakrishna</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>International Speaker 1 (Individualizing Chemotherapy in MBC)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Adam Brufsky</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Debate 1 (Weekly Abraxane is New Standard of Care in Metastatic Breast Cancer)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Introduction :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Dr. Naresh Somani Agree: Dr. Anitha Ramesh Disagree: Dr. Rajeev Gupta</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Case Discussion 1 (Recurrent HR+ve, HER2-ve MBC case)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Raju T. Chacko</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. K V Gangadharan, Dr. Anish Maru, Dr. Prasad Gunari, Dr. Tushar Patil,Dr. Sachin Hingmire, Dr. Amit Kumar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>CMC Vellore Model</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Senthil Rajappa</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ankur Bahl, Dr. CN Patil, Dr. Ghanshyam Biswas, Dr. Krishna Prasad,
								Dr. Amit Bhargava</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>CANMAb/ Trastuzumab Case Presentation</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. T. P Sahoo</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Case Presentation (Nab Pacli/ Abraxane)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Anupama Hooda & Dr. Hassan Jaafar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel discussion 2 (Contralateral Prophylactic Mastectomies Saves Lives)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Raju Titus Chacko</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anitha Ramesh, Dr. Naresh Somani, Dr. Kumar Saurabh, Dr. CJ Tamane,
								Dr. Suresh Babu, Dr. Shaheenah Dawood</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>