<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="healthcare">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/healthcare.png');?>">
		</div>
	</div>
	<div class="container">
		<div class="deepblue-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span style="float:right;margin-right:3em;">Day <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>" style="width:20px"></a> <a href="<?=url('');?>healthcareday2" ><img src="<?= assets('images/2.png');?>" style="width:20px"></a><a href="<?=url('');?>healthcareday3" ><img src="<?= assets('images/3.png');?>" style="width:20px"></a></span>
			<span style="float:right;margin-right:1em;">DAY 1, FRIDAY, 27th JANUARY 2017</span>
		
		</div>
	</div>
	<!-- section1-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 1: HEALTHCARE SYSTEMS IN SELECTED COUNTRIES</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Kim Lyerly, Suleman Merchant</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Brazil</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: 	Bernard Couttolenc </p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Brazil</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: 	Bernard Couttolenc </p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Thailand</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Somsak Chunharas</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Zambia</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Kennedy Lishimpi</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Japan</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Ryozo Matsuda</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Iran</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Maziar Moradi-Lakeh</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - France</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Karine Chevreul</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>National Health System - Cuba</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Osvaldo Garcia Gonzalez</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel discussion</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Mary Denise Leonard</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Bernard Couttolenc, Somsak Chunharas, Kennedy Lishimpi, Ryozo Matsuda,Maziar Moradi-Lakeh,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Karine Chevreul, Osvaldo Garcia Gonzalez,
				Reza Salim (Amader Gram, Bangladesh)</p>
			</div>
				
		</div>
	</div>
	<!-- section 1 end-->

	<!--section2-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 2: SUSTAINABLE MODELS OF HEALTH INSTITUTIONS</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Somsak Chunharas, Vijay Satbir Singh</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Public Hospital Model </span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Sanjay Oak</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>The HCG model of healthcare</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: AjaiKumar</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>TMC Model</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: R. A. Badwe</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>CMC Vellore Model</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Sunil Chandy</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Aravind Eye Care Model</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: P. Namperumalsamy</p>
			</div>
				
		</div>
	</div>
	
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Scalability, Growth and Affordability of Institution Models</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Benjamin Anderson</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Sanjay Oak, AjaiKumar, R. A. Badwe, Sunil Chandy,J P Gupta </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  (Commissioner Health, Gujarat), Avinash Supe (Dean, KEM),P. Namperumalsamy</p>
			</div>
				
		</div>
	</div>
	<!-- section 2 end-->
	<!--section3-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 3: EFFICACY ENDPOINTS IN HEALTHCARE DELIVERY</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Anil Srivastava</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Yardsticks for Evaluating Healthcare Delivery</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Richard Sullivan</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Universal Health Coverage in India - Service Delivery</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Chandrakant Lahariya</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Healthcare auditing</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Karine Chevreul</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Richard Sullivan, Chandrakant Lahariya,Abha Mehndiratta (Consultant, Global Health and</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Development Group), Maturin Tchoumi (Roche, India),Vikram Gota (Clinical Pharmacologist, ACTREC)</p>
			</div>
				
		</div>
	</div>
	<!-- section 3 end-->
	<!--section4-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 4: ACCESS TO AFFORDABLE CARE</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Sunil Chandy, Pallavi Govil</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>The Ethical Imperative of Accessible and Affordable Health Care</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Nathan Cherny</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Delivering High Quality Affordable Cancer Care Globally</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Benjamin Anderson	</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Facilitating worldwide access to affordable medicines</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Yusuf Hamied</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Multilevel Interventions to Reduce Health Disparities</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: K. V. Viswanath</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Healthcare Access - State Government Initiatives</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Vini Mahajan</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Accessible Healthcare - Medical Regulators role</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Shiv Sarin</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Healthcare Access</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: K. V. Viswanath</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Nathan Cherny, Benjamin Anderson, Yusuf Hamied, Vini Mahajan,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shiv Sarin, Taichi Ono (Professor, GRIPS), Anil Srivastava</p>
			</div>
				
		</div>
	</div>
	<!-- section 4 end-->

</section>
<?php include('include/footer.php');?>