<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/naghead1.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>nag-foundation" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 2, SUNDAY, 17<sup>th</sup> JANUARY 2016</span>
		</div>
	</div>
	<div class="container">
		<div class="back-brown">
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 4: Genomics/ Translational Science</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">Naresh Somani, Madhuchanda Kar </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Interim analysis of multiplex gene panel testing for inherited susceptibility to breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Idos G</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Exome based germline mutation detection in a panel of 372 cancer associated genes in BRCA ½ negative familial breast cancer patients</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Shahi R</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Results of the OPTIMA (Optimal Personalized Treatment of early breast cancer using Multiparameter Analysis) prelim study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: R. Stein </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Shalaka Joshi</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>White adipose tissue inflammation and breast cancer progression</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: N.M. Iyengar</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>A Large Prospectively Designed Study of the DCIS Score: Recurrence Risk After Local Excision for Ductal Carcinoma in Situ Patients With or Without Irradiation</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Eileen Rakovitch </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Nita Nair</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Integrative Radiotranscriptomic Analysis of Breast Carcinoma Identifies Androgen Receptor as a Target for Therapeutic Sensitization</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: B. Yard</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>The PARP inhibitor olaparib is effective as radiosensitizer at 10 fold lower doses than as single agent</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: R. De Haan </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESTRO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Santam Chakraborty</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Subclonal diversification of primary breast cancer revealed by multiregion sequencing</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Yates L Nature Medicine July 2015</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Genomic sequencing in metastatic breast cancer patients to inform clinical practice at the University of North Caroline at Chapel Hill</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Grilley-Olsen J</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer : Amit Verma</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Panel Discussion on genomics and translational science</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>S.D. Banavali,, G.S. Chowdhury</h5>
						</div>
						</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Hope Rugo</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Tanuja Shet, Amit Verma, Raju Chacko, Atul Sharma, G S Bhattacharya, Linu Jacob, Chirag Desai, Nishita Shetty, M. Samson, Ramesh Nimmagadda</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Immunotherapy in breast cancer</span>
					</div>	
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speakers  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Hope Rugo</h5>
						</div>
						</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
				<!--//end//-->
				<!--section2-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 5: TNBC</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">S H Advani, S Hukku</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Early survival analysis of the randomized phase II trial investigating the addition of carboplatin to neoadjuvant therapy for triple-negative and HER2-positive early breast cancer (GeparSixto)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Von Minckwitz G </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Event-free and overall survival following neoadjuvant weekly paclitaxel and dose dense AC +/ carboplatin and/or beacizumab in triple-negative breast cancer: outcomes of CALGB 40603 (Alliance)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Sikov WM</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Vineet Gupta</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Profiles of genome complexity identify HORMAD1 as a driver of homologous recombination deficiency and platinum therapy response in triple-negative breast cancer”</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author:A. Grigoriadis</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>TITAN: Phase III study of doxorubicin/ cyclophosphamide (AC followed by ixabepilone(lxa) or paclitaxel (Pac) in early stage, triple-negative breast cancer (TNBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D. A. Yardley</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Low-dose oral cycliphosphamide-methotrexate maintenance (CMM) for receptor negative early breast cancer (BC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: M. Colleoni</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Rejiv Rajendranath</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Results from a phase 2 study of enzalutamide (ENZA), an androgen receptor (AR) inhibitor, in advanced AR triple negative breast cancer (TNBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: T. A. Traina</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference:ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Overall survival (OS) from the phase 2 study of enzalutamide (ENZA), anandrogen receptor (AR) signaling inhibitor, in AR+ advanced triple negative breast cancer (aTNBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: J. Cortes </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Phase III trial of etirinotecan pegol (EP) versus treatment of physician’s choice (TPC) in patients (pts) with advanced breast cancer (aBC) whose disease has progressed following anthracycline (A), taxane (T) and capecitabine : The BEACON Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: E.A. Perez </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Krishna Prasad</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>A Phase lb study of pembrolizumab (MK-3475) in patients (pts) with metastatic triple negative breast cancer (mTNBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: L. Buisseret</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Safety and efficacy of anti-Trop-2 antibody drug conjugate, sacituzumab govitecan (IMMU-132), in heavily pretreated patients with TNBC</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author:A. Bardia</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Inhibition of PD-L1 by MPDL3280A leads to clinical activity in patients with metastatic triple negative breast cancer (TNBC). Abstract 2859. Presented at : 2015 American Association for Cancer Research (AACR) Meeting: April 18-22, 2015; Philadelphia, PA</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Emens LA</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: AACR 2015</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer:Shyam Aggarwal</span>
					</div>
					</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Panel Discussion on TNBC</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Ashish Bakshi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Shyam Aggarwal, Shailesh Bondarde, Ganapathy Ramanan, Ashish Singh, Vani Parmar, Bhavesh Parikh, Boman Dhabhar, Sushil Mandhaniya</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				
				<!--section 2end-->
				<!--section3-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 6: Rapid Reviews – II</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">Meenakshi Thakur, Asha Kapadia, Shekhar Salkar</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Sudeep Gupta </span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>1.	Avelumab (MSB001071BC), an anti-PD-L1 antibody, in patients with locally advanced or metastatic breast cancer: a phase lb JAVELIN solid tumor trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Dirix LY</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>2.	Importance of margin width and re-excision in breast conserving treatment of early breast cancer; a Danish Breast Cancer Cooperative Group study at 11,900 women</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Bodilsen A</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>3.	HER2 status as predictive marker for AI vs Tam benefit: a TRANS-AIOG meta-analysis of 12129 patients from ATAC, BIG 1-98 and TEAM with centrally determined HER2</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Bartlett JMS</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>4.	Ten year follow-up of the BCIRG-006 trial comparing doxorubicin plus cycliphosphamide followed by docetaxel (ACT) with doxorubicin plus cyclophosphamide followed by docetaxel and trastuzumab (AC TH) with docetaxel, carboplatin and trastuzumab (TCH) in HER2+ early breast cancer patients.</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Slamon DJ</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>5.	Trastuzumab emtasine improves overall survival versus treatment of physician’s choice in patients with previously treated HER2-positive metastatic breast cancer: Final overall survival results from the phase 3 TH3RESA study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Wildiers H</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>6.	Randomized phase 3 trial of adjuvant letrozole versus anastrozole in postmenopausal patients with hormone receptor positive, node positive early breast cancer: Final efficacy and safety results of the femara versus anastrozole clinical evaluation (Face) triall</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: O’ Shaughanessy J</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>7.	NEO-EXCEL phase III neoadjuvant trial of pre-operative exemstane or letrozole +/ celecoxib in the treatment of ER positive postmenopausal early breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Francis A </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>8.	Final results of a first-in-human phase I study of the tamoxifen (TAM) metabolite, Z-Endoxifen hydrochloride (Z—Endx) in women with aromatase inhibitor (AI) refractory metastatic breast cancer (MBC) (NCT01327781)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Goetz MP </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>9.	HER A trial: 10 years follow up of trastuzumab after adjuvant chemotherapy in HER2 positive early breast cancer – Final analysis</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe;">Author: Jackisch C</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>10.	The survival benefit offered by the surgical management of low-grade ductal carcinoma in situ o f the breast</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Y. Sagara</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>11.	Recurrence rates for ductal carcinoma in situ: Analysis of 2,996 patients treated wth breast conserving surgery over 30 years</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Kimberly J. Van Zee </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO Breast Symposium</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>12.	Phase III trial of bisphosphonates as adjuvant therapy in primary breast cancer: SWOG/ Alliance/ ECOG ACRIN/ NCIC Clinical Trials Group/ NRG Oncology study S0307</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: J. Gralow</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Effect of regular exercise on survival in women with early stage luminal-a breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: O. Yazici</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>14.	Short and Long-term Toxicity and Cosmesis After Interstital Multicatheter Brachytherapy for Accelerated Partial Breast Irradiation: A Multi-Institutional Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Robert Kuske</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>15.	241: Impact of Ipsilateral Blood Pressure Measurements, Blood Draws, Infusions, and Air Travel on the Risk of Lymphedema forPatients Treated for Breast Cancer: A Prospective Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
							<div class="col-sm-3">
							<p style="color:#0082fe">Author:Alphonse Taghian  </p>
							</div>
							<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>16.	Impact of Sequencing Radiation Therapy and Chemotherapy on Long-term Local Toxicity for Early Breast Cancer: Results of a Randomized Study at 15 Year Follow up</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Paolo Pinnaro</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">
				Conference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>17.	Physician-driven variation in non-recommended imaging for women with early stage breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. N. Lipitz Snyderman</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>18.	 Outcome analysis of patients with breast cancer and positive sentinel node</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author:  A. Syed</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div><div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>19. Prediction of pathological complete response (pCR) by Homologous Recombination Deficiency (HRD) after carboplatin containing neoadjuvant chemotherapy in patients witt TNBC: Results from GeparSixto</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: G. Von Minekwitz</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference:ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>20.	Impact of neoadjuvant therapy on breast conservation rates in triple-negative and HER2 positive breast cancer: Combined results of CALGB 40603 and 40601 (Alliance)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: M. Golshan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>21.	 NRG Oncology/ RTOG 1014: 1 Year Toxicity Report From a Phase II Study of Repeat Breast Preserving Surgery and 3-D Conformal Partial-Breast Reirradiation (PBrl) for In-Breast Recurrence</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Douglas Arthur</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>22.	Concordance of local and central HER2 status in 1597 patients participating in German neoadjuvant breast cancer studiesd 3-D Conformal Partial-Breast Reirradiation (PBrl) for In-Breast Recurrence</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: B. Pftizner</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>23. PALOMA3: A double-blind, phase III trial of fulvestrant with or without palbociclib in pre- and post menopausal women with hormone receptorpositive, HER2-negative metastatic breast cancer that progressed on prior endocrine therapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: N.C. Turner</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>24.	Phase III trial evaluating the addition of bevacizumab to letrozole as first-line endocrine therapy for treatment of hormone-receptor positive advanced breast cancer: CALGB 40503 (Alliance)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: M. N. Dickler </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>25.	Final results for overall survival (OS), the primary endpoint of the CECOG TURANDOT prospective randomized trial evaluating bevacizumab-paclitaxel (BEV-PAC) vs BEV capecitabine (CAP) for HER2 negative locally recurrent/ metastatic breast cancer (LR/mBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: T. Brodowicz</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>26.	Radiotherapy plus trastuzumab in the treatment of Her2 positive locally advanced breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Belokhvostova</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>27.	“High risk permenopausal luminal A breast cancer patients derive no benefit from adjuvant chemotherapy: results from DBCG77B randomized trial”</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author:Nielson TO </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
			<!--end-->
			
		</div>
	</div>
</section>
<?php include('include/footer.php');?>