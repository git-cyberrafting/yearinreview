<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="contact">
	<div class="container">
	<div class="back-gray2">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<h3> CONTACT US</h3>
			<div class="text-border-contact"></div>
		</div>
		</div>
	</div><hr>
	<div class="container">
	<div class="col-sm-12 col-md-12 col-xs-12 abt-breadcrumb">
		<ol class="breadcrumb">
			<li><a href='javascript:void(0)'>HOME</a></li>
			<li class="active">CONTACT US</li>        
		 </ol>
		 </div>
	</div><hr>
	<div class="container">
	<div class="back-gray">
		<div class="col-sm-12 col-md-10 col-md-offset-1">
			<h2>Aditi Mistry</h2>
			<p>Year in Review Web Co-ordinator </p>
			<h4>River Route Creative Group</h4>
			<p>Unit No.2, Cama Industrial Premises, Co-Op Society Ltd.  </p>
			<p>Sunmill Compound, Lower Parel (W), Mumbai 400 013</p>
			<p>Tel: 022 24931357/58 | Email: virtualyir@gmail.com</p>
			<div class="address-bottom-line"></div>
		</div>
		<div class="col-sm-12 col-md-10 col-md-offset-1">
			<h2>Rohan Mane</h2>
			<p>Admin Assistant </p>
			<h4>WCI-TMH </h4>
			<p>CRS/IRB Dept. 3rd Floor, Main Building. Dr. Ernest Borges</p>
			<p>Marg, Parel (E), Mumbai 400 012 </p>
			<p>Cell : +91-922-072-8503 +91-809-759-7486 </p>
			<p>Fax : +91-22-2417 7201</p>
		</div>
		<legend></legend>
		<div class="col-sm-12 col-md-10 col-md-offset-1">
		<p>For Conference Enquiries write to us at virtualyir@gmail.com</p>
		<div class="col-md-5" style="margin-left: 29%;">
		 <form role="form">
        <br style="clear:both">
    				<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Phone No." required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="email" name="email" placeholder="E-mail" required>
					</div>
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" maxlength="140" rows="7"></textarea>                   
                    </div>
             <button type="button" id="submit" name="submit" class="btn btn-primary" style="    margin-left: 106px;">SUBMIT</button>
        </form>
        </div>
		</div>
	</div>
	</div>
</section>
<?php include('include/footer.php');?>