<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumor-board">
	<div class="container">
		<div class="col-sm-12 col-md-12 tumor-head">
			<img src="<?= assets('images/tumor-head2.jpg');?>" alt="tumor board" class="img-responsive">
		</div>
	</div>
	<div class=" col-sm-9 col-md-9 pink-strip">
			<div class="container">
				<div class="col-sm-12 col-md-12">
					<div class="col-sm-6 col-md-6">
						<p>SCIENTIFIC PROGRAM </p>
					</div>
					<div class="col-sm-6 col-md-6">
						<p>14th February 2015</p>
					</div>
				</div>
			</div>
	</div>


		<!-- Start-->
		<div class="container">
	<!-- case-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Case 1: Locally advanced breast cancer-Optimal imaging for work up and management</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-5 textspan">
							<span style="color:#cc0066">Team Members :</span> 
						</div>
						<div class="col-sm-7 textteam">
							<h5>P.P. Bapsy, Vaishnavi Joshi , Somashekhar S.P, Suniti Mishra</h5>
						</div>
					
						<div class="col-sm-5 col-md-5 textspan">
							<span style="color:#0066cc">Team Lead : </span> 
						</div>
						<div class="col-sm-7 col-md-7 textteam">
							<h5>Uday Maiya</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
<!-- session2-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Session 2 : Loco-Regional Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Chairpersons :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Subhasish Sarkar, Dr. Prabir Bijoy Kar</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case2-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Case 2:  Neoadjuvant Therapy</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Madhuchanda Kar</h5>
						</div>
					
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. Amit Dutt Dwary, Dr. Saikat Gupta, Dr. Suprana Ghosh Roy,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Shekhar Keshri, Dr. Chandragouda Dodagoudar, Dr. Sudipto Roy</p></h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate1-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Debate 2: That regional nodal radiotherapy is required in all early stage node + ve Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Prasenjit Chatterjee</h5>
						</div>
					
						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">For :</span> 
						</div>
						<div class="col-sm-6 col-md-63 texth51">
							<h5>Dr. Suman Mullick</h5>
						</div>

						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">Against :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth51">
							<h5>Dr. Tanmoy Mukhopadhyay</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

		<!-- session3-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Session 3: Her2+ve Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Chairpersons :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Srikrishna Mondal, Dr. Arnab Gupta</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case3-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Case 3: Targeted Therapy Sequencing</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. B.K.Smruti</h5>
						</div>
					
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. Sharadwat Mukhopadhyay, Dr. Amit Kumar, Dr. Abhishek Basu,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. P N Mohapatra, Dr. Santa Ayyagri,Dr. Naresh Somani, Dr. Subroto Saha</p></h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate3-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Debate 3: Should surgery be offered for loco regional control in Metastatic breast cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Arunabha Sengupta</h5>
						</div>
					
						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">For :</span> 
						</div>
						<div class="col-sm-6 col-md-63 texth51">
							<h5>Dr. Diptendra Sarkar</h5>
						</div>

						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">Against :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth51">
							<h5>Dr. Vinay Deshmane</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->


		<!-- session4-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Session 4 : Her2 -ve/ Er+ve Metastatic Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Chairpersons :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Shyamal Sarkar, Dr. Litan Naha Biswas</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case4-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Case 4: Endocrine Resistance</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Dinesh Pendharkar</h5>
						</div>
					
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. Sibashish Bhattacharya, Dr. Rakesh Roy, Dr. Sanjoy Chatterjee,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Manisha Singh, Dr. Ravi Mohan, Dr. Amitabh Ray,Dr. Linu Jacob,</p></h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate4-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip" style="height: 55px;">
						<p>Debate 4: That targeted therapy and endocrine treatment should be combined upfront before endocrine resistance develops</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Sharadwat Mukhopadhyay</h5>
						</div>
					
						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">For :</span> 
						</div>
						<div class="col-sm-6 col-md-63 texth51">
							<h5>Dr. Naresh Somani</h5>
						</div>

						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">Against :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth51">
							<h5>Dr. Anita Ramesh</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

		<!-- session5-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Session 5: Triple Negative Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Chairpersons :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Anil Poddar, Dr. Santanu Pal</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case5-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Case 5: TNBC</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Ullas Batra</h5>
						</div>
					
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. Anita Ramesh, Dr. B.K.Smruti, Dr. T Raja, Dr. Vinay Deshmane,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Sudipto Roy, Dr. Pritanjali Singh, Dr. Sumant Gupta</p></h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate5-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Debate 5: That all TNBC should receive a platinum agent in adjuvant settings</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Rakesh Roy</h5>
						</div>
					
						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">For :</span> 
						</div>
						<div class="col-sm-6 col-md-63 texth51">
							<h5>Dr. Sushant Mittal</h5>
						</div>

						<div class="col-sm-3 col-md-3 textspan1">
							<span style="color:#330033">Against :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth51">
							<h5>Dr. Kumardeep Dutta</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

		<!-- session6-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Satellite Symposium - Emcure</p>
					</div>
					<div class="cancertext">
						
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case6-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Panel Discussion: Paradigm Shift In Management of Her 2 Positive Early Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Chairpersons :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. A K Malhotra, Dr. J K Singh</h5>
						</div>
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Sharadwat Mukhopadhyay</h5>
						</div>
					
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. Ullas Batra, Dr. Anita Ramesh, Dr. Joydeep Purkayastha, Dr. PN Mohapatra,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Col. P Suresh, Dr. Sanjoy Chatterjee, Dr. Dinesh Pendharkar</p></h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

	<!-- session7-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Satellite Symposium - Pfizer</p>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case7-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Panel Discussion: Paradigm Shift In Management of Her 2 Positive Early Breast Cancer</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Speaker :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Chanchal Goswami</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate7-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Panel Discussion: Understanding place of CDK 4/6 inhibitors in Indian treatment landscape</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Chanchal Goswami</h5>
						</div>
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. A K Malhotra, Dr. Madhuchanda Kar,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Ullas Batra, Dr. Vineet Talwar</p></h5>
						</div>
					
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

		<!-- session8-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="grey-strip">
						<p>Satellite Symposium - Zydus</p>
					</div>
				</div>
			</div>
			</div>
		</div>
	<!-- case8-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Development of Vivitra</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span>Speaker :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Sanjeev Kumar</h5>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->
		<!-- dabate8-->
		<div class="col-sm-12">
			<div class="col-sm-10">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<div class="light-strip">
						<p>Panel Discussion: How Comfortable Are Doctors Using Biosimilar Trastuzumab?</p>
					</div>
					<div class="cancertext">
						<div class="col-sm-6 textspan">
							<span style="color:#cc0066">Moderator :</span> 
						</div>
						<div class="col-sm-6 texth5">
							<h5>Dr. Amit Dutt Dwary</h5>
						</div>
						<div class="col-sm-6 col-md-6 textspan1">
							<span style="color:#330033">Panelists :</span> 
						</div>
						<div class="col-sm-8 col-md-8 texth5">
							<h5>Dr. P N Mohapatra, Dr. Sandip Ganguly, Dr. Saurabh Kumar,Dr. Amitabh Ray,</br><p style="font-family:Myriad Pro;font-size: 19px;color: #000;">Dr. Anita Ramesh, Mr. Samir Desai, Dr. Sanjeev Kumar</p></h5>
						</div>
					
					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2">
				<div class="col-sm-12">
				<div class="col-sm-12">
				<div class="margin-top-16">
				<a href='<?=url('');?>' class="pull-right"><img src="<?= assets('images/view-btn.jpg');?>"></a>
				</div>
				</div>
				</div>
			</div>
		</div><!-- col-sm-12-->

		</div><!-- container-->
		<!--//new end-->
	
</section>


<?php include('include/footer.php');?>