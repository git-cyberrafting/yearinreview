<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead4.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>	
			<span class="no_btn">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>phocon2016" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>		
			<span class="insidespan">SUNDAY,6<sup>th</sup> NOVEMBER 2016,MUMBAI</span>
			</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>ONCOLOGY SESSION 5:LATE EFFECTS IN CHILDHOOD CANCER SURVIVORS</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Long-term Follow-up Guidelines for care of Childhood Cancer Survivors: Adaptation and Application in India</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Smita Bhatia</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>PEG L Asparaginase role in ALL</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Scott Howard</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>ONCOLOGY SESSION 6: NEURO-ONCOLOGY SYMPOSIUM</span>
						</div></br>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Classification and Pathology of Pediatric Brain Tumors-Recent Update</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Kirti Gupta</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
				<!--scetion2 end-->
				<!--section 2 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Enigma and challenges in management of Pediatric Low Grade</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Gliomas</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Rahul Krishnatry</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Pediatric Ependymoma: Insights into Biology and Management</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Ammar Gajjar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Neurosurgery in Pediatrics</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr.C.E Deopujari</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Pediatric Neuro- Oncology in India Challenges and the future</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr.Rakesh Jalali</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Panel Discussion  on Brain Tumors</span>
						</div>
						<div class="col-sm-12"></br>
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Tejpal Gupta</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Judges :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Amar Gajjar. Dr.Rakesh Jalali, Dr.Rahul Krishnatry, Dr. Shridhar Epari, Dr. Vikas Dua, Dr. Vikramjit Kanwar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
				<!-- section 2 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>