<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/gynhead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>gynecology" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)' ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 2, SATURDAY, 8<sup>th</sup> JANUARY 2017</span>
		</div>
	</div>
	<div class="container">
		<div class="back-grey">
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 1</span>
						</div>
						<div>
							<p class="sentence">Sentinal Node Biopsy in Gynecological Cancer</p>
						</div>	
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Krishnansu Tewari</span>
					</div>
					<div>
					<p class="sentence">Endometrial Carcinoma: Conventional & Molecular Pathology Discussion</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Jamie Prat</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
		<!--section 2-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 2 : CERVIX</span>
						</div>
						<div>
							<p class="pink mlr1">Chairpersons : Dr. Reen Engineer, Shylasree Surappa</p>
						</div>
						<div>
							<p class="sentence">Management and Care of Women With Invasive Cervical Cancer: ASCO Resource-Stratified Clinical Practice Guideline</p>
						</div>
						<div >
							<p class="author">Author: Chuang LT</p>
						</div>
						<div>
							<p class="sentence">Preoperative nomogram for individualized prediction of parametrial invasion in patients with FIGO stage IB cervical cancer treated with radical hysterectomy</p>
						</div>
						<div >
							<p class="author">Author: T.W. Kong</p>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Neha Kumar</span>
					</div>
					<div>
					<p class="sentence">Preliminary Results Of Eortc 55994 Comparing Neoadjuvant Chemotherapy Followed By Surgery With Chemoradiation For Stage Ib2-iib Cervical Cancer</p>
					</div>
					<div>
							<p class="author">Author: G. Kenter (EORTC)</p>
					</div>	
					<div>
					<p class="sentence">"Does paraaortic irradiation reduce the risk of distant metastasis in cervical cancer? A systematic review and metaanalysis of randomized clinical trials"</p>
					</div>
					<div>
							<p class="author">Author: Lukas Sapienza</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Rohit Ranade</span>
					</div>
					<div>
					<p class="sentence">Is IMRT for Gynecologic Cancers Less Toxic? Klopp AH, et al. Abstract 5. Presented at: ASTRO Annual Meeting</p>
					</div>
					<div>
							<p class="author">Author: Ann Klopp</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Supriya Chopra</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
		<!-- section2 end-->

		<!-- section3-->
		<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator: Kaustav Talapatra</p>
						<p class="mlr2" style="color:#330033">Panelists :Hemant Tongaonkar, Sudeep Gupta, Reena Engineer, C Sairam, Govind Babu</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		
		<!-- section3 end-->

		<!-- section4-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 3: UTERUS-I </span>
						</div></br>	
						<div>
					<p class="sentence">Impact of body mass index on surgical costs and morbidity for women with endometrial carcinoma / hyperplasia</p>
					</div>
					<div>
							<p class="author">Author: Rudy Sam Suidan</p>
					</div>
					<div>
					<p class="sentence">Manipulators Influence the Disease / Free Survival and Recurrence Rates in Early-Stage Endometrial Cancer?</p>
					</div>
					<div>
					<p class="sentence">Disease-free And Overall-survival After Total Laparoscopy Versus Open Abdominal Hysterectomy For Early Stage Endometrial Cancer: Results From The Lace Trial</p>
					</div>
					<div>
							<p class="author">Author: A. Obermair</p>
					</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Sampada Desai</span>
					</div>
					<div>
					<p class="sentence">Clinicopathologic Significance of Mismatch Repair Defects in Endometrial Cancer: An NRG Oncology / Gynecologic Oncology Group Study</p>
					</div>
					<div>
							<p class="author">Author: McMeekin DS</p>
					</div>
					<div>
					<p class="sentence">"Screening for Lynch syndrome among endometrial cancer patients less than 60 years"</p>
					</div>
					<div>
							<p class="author">Author: E. Aguirre</p>
					</div>
					<div>
					<p class="sentence">Vaginal brachytherapy versus external beam pelvic radiotherapy for high-intermediate risk endometrial cancer: long term results of the randomized</p>
					</div>
					<div>
							<p class="author">Author: R. Nout</p>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Monisha Gupta</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion </span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Bhupesh K Goyal</p>
						<p class="mlr2" style="color:#330033">Panelists :Umang Mittal, Santosh Menon, Prashant Nyati ,Swapnil Kapote,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dilip Nikam,Uma Singh, Amita maheshwari, Shalini Rajaram, Jamie Prat</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<!-- section4 end-->

		<!-- section5-->
		<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="grey-stip">
							<span>Session 4: UTERUS-II</span>
						</div></br>
						<div>
							<p class="sentence1">The role of platinum rechallenge as second line chemotherapy for metastatic endometrial carcinoma</p>
						</div>	
						<div>
							<p class="author">Author: R P Souza</p>
					</div>
					<div>
						<p class="sentence">Phase II study of everolimus, letrozole, and metformin in women with advanced / recurrent endometrial cancer</p>
					</div>	
					<div>
						<p class="author">Author: P. T. Soliman</p>
					</div>
					<div>
						<p class="sentence">"A DGOG open-label multicenter phase II study of pazopanib in metastatic and locally advanced hormone-resistant endometrial cancer"</p>
					</div>	
					<div>
						<p class="author">Author: L.E. Boom</p>
					</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Padmaj Kulkarni</span>
					</div>
					<div>
					<p class="sentence">A Combined Molecular And Immunological Stratification Strategy For Immunotherapeutic Treatment Of High Risk Endometrial Cancer</p>
					</div>
					<div>
							<p class="author">Author: F. Eggink</p>
					</div>	
					<div>
					<p class="sentence">"Pembrolizumab in advanced endometrial cancer: Preliminary results from the phase Ib KEYNOTE028 study"</p>
					</div>
					<div>
							<p class="author">Author: Patrick Aexander</p>
					</div>
					<div>
					<p class="sentence">Pembrolizumab in patients with advanced cervical squamous cell cancer: Preliminary results from the phase Ib KEYNOTE-028 study</p>
					</div>
					<div>
							<p class="author">Author: Jean-Sebastien F</p>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightgrey-stip">
						<span>Reviewer: Gaurav Gupta</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
			<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:B K SMRUTI</p>
						<p class="mlr2" style="color:#330033">Panelists :Boman Dhabhar, Govind Babu,Ranga Rao, Rakesh Taran, Devendra Pal,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G S Chowdhary, Seema Gulia</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
		<!-- section5 end-->
		</div>
	</div>
</section>
<?php include('include/footer.php');?>