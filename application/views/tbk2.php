<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/tumor-head2.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan"> 13-15 FEBRUARY 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
	
				<!-- section 1-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 1: Locally advanced breast cancer-Optimal imaging for work up and management</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Team Members :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. P.P. Bapsy, Dr. Vaishnavi Joshi , Dr. Somashekhar S.P, Dr. Suniti Mishra</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Team Lead  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Uday Maiya</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 1 end-->
				<!-- section 1-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 2: Management of DCIS</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Team Members :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Soumendranath Ray, Dr. Rosina Ahmed, Dr. Reena Nair,
Dr. Geetashree Mukherjee, Dr. Sanjit Agarwal, 
Dr. Soumitra Sankar Datta</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Team Lead  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sanjoy Chatterjee</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 1 end-->
				<!-- section 1-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 3: Oligo metastatic disease management</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Team Members :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Vani Parmar, Dr. M.H.Thakur, Dr. Seema Gulia, 
Dr. Ashwini Budrukkar, Dr. Tanuja Shet</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Team Lead  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Nita Nair</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 1 end-->
				<!-- section 1-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 4: Genetic testing/screening Prophylatic measures</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Team Members :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Neerja Gupta, Dr. Bikram Deka, Dr. Amit Verma, Dr. Richa Bansal,Dr. Charu Garg</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Team Lead  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. K. Geeta</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 1 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>
