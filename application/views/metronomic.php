<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/metronomic_head.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn3"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>metronomic2" ><img src="<?= assets('images/2.png');?>"></a><a class="btn-nonactive" href="<?=url('');?>metronomic3" ><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan-metronomic">DAY 1, FRIDAY, 6<sup>th</sup> MAY 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-grey">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip">
								<span>Session 1: Why Metronomic?</span>
							</div>	
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightgrey-stip">
							<span>Pharmacokinetics of low doses</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Vikram Gota, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Repositioning of drugs in oncology : The ReDo project</span>
						</div>

						<div>
							<p class="sentence">Speaker: Dr. Pan Pantziarka, Belgium</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Key note lecture : Next - generation drug repositioning : using high-through put screening technology to improve cancer treatment</span>
						</div>

						<div>
							<p class="sentence">  Speaker: Dr. Eddy Pasquier, France</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Session 2: Design of Metronomic Studies</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Single arm studies in metronomic therapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Aparna Parikh, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Randomized phase 2 studies in metronomic therapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Sudeep Gupta, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Randomized phase 3 studies in metronomic therapy</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Kumar Prabhash, Mumbai</p>
						</div>		
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Response assement in metronomic therapy</span>
						</div>
						<div>
							<p class="sentence">Radiologist View: Dr. Abhishek Mahajan, Mumbai</p>
						</div>	
						<div>
							<p class="sentence">Nuclear Medicine view : Dr. Nilendu Purandare, Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section2 end-->
				<!-- section3-->
						<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Session 3: Endpoint Selection in Metronomic Trials</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Appropriate clinical end point selection for metronomic studies</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Atanu Bhattacharjee, Thalassary</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Efficacy V/s. Toxicity endpoint selection for metronomic studies</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Sadhana K, Navi Mumbai</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section4-->
						<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="grey-stip bottom-18">
								<span>Session 4: Mathematics & Biology in Metrononic Therapies</span>
							</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Mathematical modelling of chemotherapy scheduling</span>
						</div>
						<div>
							<p class="sentence">Speaker: Dr. Sebastien Benzekry, France</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Debate : Tumor shrinkage (objective response) or clinical benefit rate : Which is the appropriate efficacy endpoint for Metronomic therapy?</span>
						</div>
						<div>
							<p class="sentence">For Tumor shrinkage : Dr. Amish Vora, Delhi</p>
						</div>
						<div>
							<p class="sentence">For Clinical benefit rate : Dr. Randeep Singh, Delhi</p>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightgrey-stip">
						<span>Panel Discussion : Survey of practice, perception and promise in Metronomic therapy and generation of a consensus statement</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Dr. Purvish Parikh, Mumbai & Dr. Vanita Noronha, Mumbai</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Dr. GS Bhattacharyya, Kolkatta; Dr. Sudeep Gupta, Mumbai;Dr. Kumar Prabhash, Mumbai;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dr. Satheesan B, Thallasery;Dr. Raman Govindarajan, Mumbai; Dr. Partha Roy, Gauhati;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr. Nishita Shetty, Mangalore; Dr. Gautam Goyal, Mumbai ;Dr. Hari Menon, Mumbai;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dr. Avinash Pandey, Hyderabad;Dr. Padmaj Kulkarni, Pune; Dr. Bharat Bhosale, Mumbai;</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dr. Amit Dutt, Mumba</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
				<!-- section4 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>