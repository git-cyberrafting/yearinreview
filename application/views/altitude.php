<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/altitudehead.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SUNDAY, 18<sup>th</sup> OCTOBER 2015</span>

			</div>
		</div>
		<div class="container">
			<div class="back-blue">
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Clinically relevant molecular biology of myeloma</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Chandrakala S, Ganesh Jaishetwar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Vikram Mathews</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Smoldering myeloma - Present approach</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Shriram Kane, T.P. Sahoo</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Sameer Melinkeri</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Assessment of minimal residual disease in myeloma</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Assessment of minimal residual disease in myeloma</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>M.B. Agarwal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Treatment of a newly diagnosed case of myeloma</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Treatment of a newly diagnosed case of myeloma</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Manju Sengar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Management of R/R myeloma after failure of VRD and autologous transplant</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>R. Manchanda, Vijay Hirani</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Nikhil Munshi</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>