<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="healthcare">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/healthcare.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>tmhealthcare" ><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>tmhealthcare2" ><img src="<?= assets('images/2.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/3.png');?>"></a></span>
			<span class="insidespan">DAY 3, SUNDAY, 29<sup>th</sup> JANUARY 2017</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 10: ROLE OF NGOS AND CSR - BRIDGING GAPS AND REDUCING INEQUALITIES</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Rumana Hamied, Sanjay Pai</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>CSR - Beyond the Mandate</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Rumana Hamied</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Delivering access to home care towards end of life</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Harmala Gupta</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Role of Philanthropy</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>R Venkataramanan</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion - CSR and NGOs - Determinants and Goals</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Nihal Kaviratne</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Anupama Mane, Shalaka Joshi, Archana Shetty, Anil Heroor, Namita Pandey, Tabassum W., V Kannan, Vedant Kabra, Sanjay Sharma, Nita Nair, Vikram Maiya, Manish Chandra</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section 2 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>SESSION 11: HEALTH COMMUNICATIONS
            (MEDIA ROLE AND RESPONSIBILITY & ROLE OF PROMINENT PERSONALITIES)</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p style="color:#ff0000">Ramesh Bharmal, Anil Srivastava</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Responsible Reporting</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Marita Hefler</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion - Spreading the right word</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Pankaj Chaturvedi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Nandini Sardesai (social activist), Marita Hefler, Nivedita Sinha (Cancer Activist), Sanjay Pai, Vish Vishwanath, Nishu Singh Goel (Consultant, TMC), Rakesh Jalali (Radiation Oncologist, TMC), Sanjay Nagral (Consultant GI Surgeon), Farukh Basrai (CENDIT), Anurag Basu (Film Director)</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--section 2 end-->
				<!-- section 3 start-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Access To Affectionate Care</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>S. S. Naganand</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>MUMBAI DECLARATION</span>
						</div>	
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>R. A. Badwe</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>