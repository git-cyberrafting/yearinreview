<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="healthcare">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/healthcare.png');?>">
		</div>
	</div>
	<div class="container">
		<div class="deepblue-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span style="float:right;margin-right:3em;">Day <a href="<?=url('');?>healthcare" ><img src="<?= assets('images/1.png');?>" style="width:20px"></a> <a href="<?=url('');?>healthcareday2" ><img src="<?= assets('images/2.png');?>" style="width:20px"></a><a href='javascript:void(0)'><img src="<?= assets('images/3.png');?>" style="width:20px"></a></span>
			<span style="float:right;margin-right:1em;">DAY 3, FRIDAY, 29th JANUARY 2017</span>
		
		</div>
	</div>
	<!-- section1-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 10: ROLE OF NGOS AND CSR - BRIDGING GAPS AND REDUCING INEQUALITIES</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Rumana Hamied, Sanjay Pai</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>CSR - Beyond the Mandate</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Rumana Hamied</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Delivering access to home care towards end of life</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Harmala Gupta</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Role of Philanthropy</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: R Venkataramanan</p>
			</div>
				
		</div>
	</div>
	
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - CSR and NGOs - Determinants and Goals</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Nihal Kaviratne</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists : Anupama Mane, Shalaka Joshi, Archana Shetty, Anil Heroor,Namita Pandey, </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tabassum W., V Kannan, Vedant Kabra,
				Sanjay Sharma, Nita Nair, Vikram Maiya, Manish Chandra</p>
			</div>
				
		</div>
	</div>
	<!-- section 1 end-->

	<!--section2-->
	<div class="container">
		<div class="lightback-blue">
			<div class="grey-stip">
				<span>SESSION 11: HEALTH COMMUNICATIONS
            (MEDIA ROLE AND RESPONSIBILITY & ROLE OF PROMINENT PERSONALITIES)</span>
			</div>
			<div>
				<p class="pink2 mlr1">Chairpersons: Ramesh Bharmal, Anil Srivastava</p>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Responsible Reporting</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: Marita Hefler</p>
			</div>
				
		</div>
	</div>
	
	
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Panel Discussion - Spreading the right word</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Moderator: Pankaj Chaturvedi</p>
			</div>
			<div>
				<p class="sentence2" style="color:#330033">Panelists :Nandini Sardesai (social activist), Marita Hefler,Nivedita Sinha (Cancer Activist), </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Sanjay Pai, Vish Vishwanath,Nishu Singh Goel (Consultant, TMC),</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rakesh Jalali (Radiation Oncologist, TMC),Sanjay Nagral (Consultant GI Surgeon),</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Farukh Basrai (CENDIT),
				Anurag Basu (Film Director)</p>
			</div>
				
		</div>
	</div>
	<!-- section 2 end-->
	<!--section3-->
	

	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>Access To Affectionate Care</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: S. S. Naganand</p>
			</div>
				
		</div>
	</div>
	<div class="container">
		<div class="lightback-blue">
			<div class="l-grey-stip">
				<span>MUMBAI DECLARATION</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence2">Speaker: R. A. Badwe</p>
			</div>
				
		</div>
	</div>
	
	<!-- section 4 end-->

</section>
<?php include('include/footer.php');?>