<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/naghead.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn" style="color: #fff !important"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>nag2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 1, 19<sup>th</sup> OCTOBER 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
			
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Inaugural Addresses by Chief Guest, Ms. Lisa Ray, & Guest of Honour, Dr. Anil D'Cruz, Director, TMH</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view"">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Lecture 1 - Rehabilitation and Physical Recovery after Breast Cancer with some Surgery information on Delayed Breast Reconstruction</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Nita Nair</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Survivor Experience 1 - Ms. Heena Khan</span>
						</div>				
							</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Lecture 2 - After cancer treatment – Dealing with one day at a time and the “New Normal”</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Purna Kurkure</p>
						</div>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Survivor Experience 2 - 6 things that cancer taught me Ms. Devieka Bhojwani & Dr. Shona Nag</span>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Panel: “What are my rights and duties as a survivor?” Ask the Expert</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Sudeep Gupta and Dr. Raj Nagarkar</p>
						</div>
						</div>	
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Medico-legal Expert  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Leena Ganguly</p>
						</div>
						</div>	
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Indian Cancer Society  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mr. Pradeep Gogte</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Social worker :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ms. Neelima Dalvi</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Novartis India Ltd :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Manish Mistry</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>NGO :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ms. Devieka Bhojwani & Dr. Shona Nag</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Lecture 3 - Updates on the treatment of Metastatic Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Bhawna Sirohi</p>
						</div>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Panel: “Addressing the fear of relapse”. Ask the Expert</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Astrid Lobo Gajiwala & Dr. Anupama Mane</p>
						</div>
						</div>	
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Palliative Medicine :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Maryanne Muckadam</p>
						</div>
						</div>	
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Oncologists  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Sudeep Gupta & Dr. Vani Parmar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Psychologist  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ms. Rebecca DeSouza</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Counsellor :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ms. Usha Banerji</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Survivor experience 3 - On her book, "To Cancer, With Love-My Journey of Joy". Ms. Neelam Kumar</span>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>