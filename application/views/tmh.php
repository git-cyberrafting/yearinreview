<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/tmhhead.png');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">FRIDAY, 9<sup>th</sup> OCTOBER 2015</span>

			</div>
		</div>
		<div class="container">
			<div class="back-purple">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightpurple-stip">
							<span>Improving Cancer Management Through Strengthening The Computed Tomography Cancer Staging Process (RCA)</span>
						</div>
						<div>
							<p class="sentence">Speaker : Kie Hwan Kim</p>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Medical Ethics</span>
						</div>

						<div>
							<p class="sentence">Speaker : Vedang Murthy</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Cross-sectional Anatomy of Female Pelvis</span>
						</div>

						<div>
							<p class="sentence">Speaker : Nilesh Sable</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Cross-sectional imaging and Staging in Endometrial Malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Kie Hwan Kim</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Cross-sectional imaging and Staging in Ovarian Malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Karthik Ganesan</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Cross-sectional imaging and Staging in Cervical Malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Priya Bhosale</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Multiparametric imaging in gynaecologic malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Abhishek Mahajan</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Role of Nuclear Medicine and molecular imaging in gynaecologic malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Archi Agrawal</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Post therapy changes in cervical malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Priya Bhosale</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Prediction of optimal cytoreductive surgery in ovarian cancer</span>
						</div>

						<div>
							<p class="sentence">Speakers : Kie Hwan Kim</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Role of Intervention in Gynaecologic malignancies</span>
						</div>

						<div>
							<p class="sentence">Speakers : Suyash Kulkarni</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				
				<!-- section2 end-->
		<div class="row">
			<div class="col-sm-10 verticalline">
				<div class="content">
					<div class="lightpurple-stip">
						<span>Panel Discussion: Role of Imaging in Staging of Gynaecologic Malignancies</span>
					</div>
					<div>
						<p class="pink mlr2">Moderator:Sudeep Gupta</p>
						<p class="mlr2" style="color:#330033">Panelists :
						Nandini Bahri, Kie Hwan Kim, Priya Bhosale,Amita Maheshwari, </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hemant Tongaonkar, Christine Meder,Umesh Mahantshetty, Sandip Basu, Archi Agrawal</p>
					</div>	
				</div>
			</div><!-- col-sm-10-->
			<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
		</div><!-- row-->
				<!-- section4 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>