<?php 
	include('include/header.php');
	include('include/navigation.php');
?>

<!--Slider-->
 <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000" >

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
                 <li data-target="#bootstrap-touch-slider" data-slide-to="3"></li>
                  <li data-target="#bootstrap-touch-slider" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">

                <!-- Third Slide -->
                <div class="item active">
                    <img src="<?= assets('images/slider1.jpg');?>" alt="yir"  class="slide-image"/>
                 </div>
                <!-- End of Slide -->

                <!-- Second Slide -->
                <div class="item">
                    <img src="<?= assets('images/slider2.jpg');?>" alt="yir"  class="slide-image"/>
                 </div>
                <!-- End of Slide -->

                <!-- Third Slide -->
                <div class="item">
                    <img src="<?= assets('images/slider3.jpg');?>" alt="yir"  class="slide-image"/>
                </div>
                <!-- End of Slide -->
                <!-- fourth Slide -->
                <div class="item">
                    <img src="<?= assets('images/slider4.jpg');?>" alt="yir"  class="slide-image"/>
                </div>
                <!-- End of Slide -->
                <!-- fifth Slide -->
                <div class="item">
                    <img src="<?= assets('images/slider5.jpg');?>" alt="yir"  class="slide-image"/>
                </div>
                <!-- End of Slide -->


            </div><!-- End of Wrapper For Slides -->

            <!-- Left Control -->
            <a class="left carousel-control" href="<?=url('');?>#bootstrap-touch-slider" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <!-- Right Control -->
            <a class="right carousel-control" href="<?=url('');?>#bootstrap-touch-slider" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

 </div> <!-- End  bootstrap-touch-slider Slider -->
        


<?php include('include/footer.php');?>