<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead5.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>	
			<span class="no_btn" style="color: #fff !important">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>phocon2016-hematology-d2" ><img src="<?= assets('images/2.png');?>"></a></span>	<span class="insidespan">SATURDAY,5<sup>th</sup> NOVEMBER 2016,MUMBAI</span>
			</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>HEMATOLOGY SESSION 1: ADVANCED HEMATOLOGY</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Advanced Clinical Parameters on Hematology Analysers</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Shanaz Khodaiji</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Applied Molecular Diagnostics in Hematology</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Eunice Sindhuvi Edison</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Molecular basis of Fanconi Anemia</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Rama S. Verma</h5>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 2: PLENARY (R D CHOKSI AUDITORIUM)</span>
						</div></br>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>The Enviroment and Child Health- In Search of Answers</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. ATK Rau</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Brain Tumor Genomics-Translating Discovery From Bench to Bedside</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Amar Gajjar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--scetion2 end-->
				<!--section 2 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>HEMATOLOGY SESSION 3:HAEMOPHILIA AND THROMBOSIS</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Prophylaxis and Home Theraphy in Haemophilia- Making it happen with Turoctocog alfa</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Satyaranjan Das</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Thrombosis- Clinical presentations</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Rashmi Dalvi</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Thrombosis- Challenges in management</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Tulika Seth</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>HEMATOLOGY SESSION 4:HSCT IN THALASSEMIA</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Revathi Raj</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>HEMATOLOGY SESSION 5:PANEL DISCUSSION ON PRACTICAL ISSUES IN THALASSEMIA HSCT</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Shweta Bansal</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Satyendra Katewa , Dr. Satya Yadav, Dr. Rajat Bhattacharyya , Dr. Sunil Bhat ,Dr. Lawrence Faulkner</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>HEMATOLOGY FREE PAPERS</span>
						</div>
						<div class="col-sm-12"></br>
						<div class="col-sm-6">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Satya Yadav, Dr. Santanu Sen</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Judges :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Tulika Seth, Dr. Pankaj Abrol, Dr.Shaliesh Kanvinde</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Discordant BeatsA prospective study of cardiac MRI to assess iron in patients with thalassemia major</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Divya S</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Acquired a plastic anaemia in children- Optimal outcomes depend on optimal delivery of Immunosuppressive therapy</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Dharrani Jayaraman</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>IS Serum Ferritin a Reliable Marker for Evaluation of Cardiac and Hepatic Iron Overload in Pateints of B-Thalassemia Major ?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Ankita Pandey</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Hematopoietic Stem Cell Transplantation In Thalassemia</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Wassem Iqbal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Chronic Granulomatous Disease: Case Series</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Purva Kanvinde</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>RIC HSCT in Primary Immunodeficiency children in developing world- A ray pf hope</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Neha Rastogi</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 2 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>