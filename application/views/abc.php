<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/abchead.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SUNDAY, 5<sup>th</sup> APRIL 2015</span>

			</div>
		</div>
		<div class="container">
			<div class="back-grey">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Session 1 : GENERAL GUIDELINES</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Asha Kapadia, R.A.Badwe, Raja Nagarkar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Bhawna Sirohi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Anil Heroor, Amish Dalal, Bhavesh Parikh, Chetan Deshmukh,Lokanatha Dasappa,Nilesh Lokeshwar, Peush Bajpai,Seema Khemnhavi, Vivek Garg </p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Session 2 : ER+ve / HER2-ve MBC</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>D C Doval, S H Advani</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>B K Smruti</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Beela Sarah Mathew, Jyoti Bajpal, Kannan Kalaichelvi,P N Mohapatra, Satya Dattatreya, TP Sahoo, Tejinder Singh</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--scetion2 end-->
				<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Session 3 : HER2+ve MBC</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Sudeep Gupta</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Senthil Rajappa</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Jaya Ghosh, Jatin Sarin, Ullas Batra, Niti Raizada, Ravi Mohan,
							Sandeep Btra, Sadashivadu Gundeti, Sushil P Mandhaniya</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				<!-- section 4-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgrey-stip">
							<span>Nanomedicine :- prescription of cancer, using lipid based delivert system for taxanes</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Sheikh Ajez</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Speakers  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Satya Dattatreya</h5>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 4 end-->
				<!-- section 5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Session 4 : TNBC</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Ramesh Nimaggada, R Gopal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Anupama Hooda</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Ashish Bakshi, Anjana Sainani, Chirag Desai,Gaurav Gupta, Kishore Kumar, Manish Singhal,Madhuchanda Kar, Poonam Patil, Ranjan Mohapatra</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 5 end-->
				<!-- section 6-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Session 5 : SPECIFIC METASTATIC SITES</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>K Pavitran</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>T Raja</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Anurag Mehta, Advait Gore, A K Dhar, Boman Dhabhar,Kaustav Talpatra, Randeep Singh, Suchanda Goswami,Urvashi Bhadur, V Kannan, Ashwini Budrukkar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 6 end-->
				<!-- section7-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="grey-stip">
							<span>Take Home Message</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Speakers  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Amit Agarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Speakers :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Shona Nag</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Speakers :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Seema Gulia</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2" style="visibility: hidden;">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 7 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>