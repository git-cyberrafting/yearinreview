<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/lunghead3.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SATURDAY, 9<sup>th</sup> APRIL 2016</span>

			</div>
		</div>
		<div class="container">
			<div class="back-gold">
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightgold-stip">
							<span>Afatinib: Moving towards complete, potent and irreversible blockade in EGFR mutation +ve NSCLC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>P. K. Julka</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Shyam Aggarwal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgold-stip">
							<span>Panel Discussion</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Chairpersons : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Purvish Parikh</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Manish Singhal, Dr. AK Malhotra, Dr. A K Vaid, Dr. Col P Suresh Nair, Dr. Sachin Gupta, Dr. Madhuchanda Kar, Dr. Ullas Batra, Dr. Naresh Somani, Dr. K Pavithran</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>