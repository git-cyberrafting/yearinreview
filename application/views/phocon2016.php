<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead4.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>	
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>phocon22016" ><img src="<?= assets('images/2.png');?>"></a></span>		
			<span class="insidespan">SATURDAY,5<sup>th</sup> NOVEMBER 2016,MUMBAI</span>
			</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip bottom-18">
							<span>ONCOLOGY SESSION 1:WILMS TUMOR</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Changing risk stratification and impact of molecular biology</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Jeffrey Dome</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Surgical aspects in Indian Context</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sandeep Agarwala</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Panel Discussion on Challenging scenarios</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Sajid Qureshi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Seema Kembhavi, Dr. Sandeep Agarwala, Dr. Aruna Rajendran, Dr. Jeffrey Dome, Dr. Nehal Khanna, Dr. Sammer Bakhshi</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 2: PLENARY</span>
						</div></br>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>The Enviroment and Child Health- In Search of Answers</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. ATK Rau</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Brain Tumor Genomics-Translating Discovery From Bench to Bedside</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Amar Gajjar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--scetion2 end-->
				<!--section 2 start-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>ONCOLOGY SESSION 3:HODGKIN LYMPHON</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Hodgkin Lymphoma:Current State of Art</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Scott Howard</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Hodgkin Lymphoma: Challenges</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Amita Mahajan</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Panel Discussion on Hodgkins-Optimising Therapy</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Gauri Kapoor</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Scott Howard, Dr. Richa Jain, Dr. Narendra Chaudhary , Dr. Arathi Srinivasan ,Dr. Ajith Kumar, Dr. Kaustav Talapatra</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>ONCOLOGY SESSION 4:IMMUNOTHERAPY IN PEDIATRIC ONCOLOGY</span>
						</div></br>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Role of Immunotherapy in Pediatric Hematological Malignancy</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr.Terry Fry</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Emerging Role of Immunotherapy in Pediatric Solid Tumors</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Shakeel Modak</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span> Immunotherapy in India-The way ahead</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Rahul Purwar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Dr. R.K Marwaha-PHO Chapter Paper Of The Year</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Raja Pramanik</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>ONCOLOGY FREE PAPERS</span>
						</div>
						<div class="col-sm-12"></br>
						<div class="col-sm-6">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Scott Howard, Dr. Sameer Bakhshi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Judges :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Deepa Trivedi, Dr. Gaurav Kharya, Dr.Roshni Pande</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>1. Multidisciplinary Management of Hepatoblastoma with Incorporation of Liver Transplantation in Children</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Vimal Kumar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>2. Voriconazole Plasma Levels, its Determinants and Impacts on Outcome of Invasive Fungal Infections in Children with Cancer: A Prospective study</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Vasudev Bhat</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>3. Dealays in Diagnosis and Treatments and their causes among children with cancer in India</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Nishant Verma</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>4. A High Sensitivity, Cost-effective, 10-Color Flow Cytometry based BCPALL-MRD Assay</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Dilshad Dhaliwad</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>5. Combating blood stream infections during induction chemotherapy in children with acute myeloid leukemia-smart bugs need smarter solutions</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Divya S</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>6. Diagnostic cerebrospinal fluid in intraocular group E retnoblastoma:Justified?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Richa Jain</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 2 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>