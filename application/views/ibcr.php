<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/ibcr-head.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">MUMBAI 2013</span>
	</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip bottom10">
							<span>1. Breast MRI</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sona Pungavkar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>2. Breast Cancer Screening-Indian and Global Perspective</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Nita Nair</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>3. Adjuvant Endocrine Therapy in Premenopausal Patients</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Seema Gulia</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>4. Standard Radiation Therapy in Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Anusheel Munshi</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>5. Assessment of Co-morbidity in Breast Cancer for Planning Chemotherapy</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Manish Singhal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>6. Management of HER2 Positive Metastatic Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. Jyoti Bajpai</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>7. Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sudeep Gupta</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>8. Advanced Breast Cancer ESMO Guidelines</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Hemant Malhotra</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>9. Surgical Management of the Axilla</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. Selvi Radhakrishna</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>10. Diagnostic Mammography</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. M H Thakur</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>11. Adjuvant Chemotherapy in Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Senthil Rajappa</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>12. Management of Axilla in Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Vani Parmar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>13. Managemnet of HER2 Positive and Metastatic Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Ashish Singh</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>14. Targeted Therapy for HER2 Negative Recurrent/Metastatic Disease</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Randeep Singh</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>15. Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Speaker: Dr. Radheyshyam Naik</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>16. Palliative Care in Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sushma Bhatnagar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>17. Neo adjuvant Strategies in HER2- Positive Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Shyam Aggarwal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>18. Overview of Breast Cancer Management</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. SVS Deo</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>19. Novel Therapies in Breast Cancer Part 1</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. G. S. Bhattacharyya</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>20. Novels systematic Therapeutic options for Hormone Positive Breast Cancer Part 2</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. G. S. Bhattacharyya</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>21. Novel Treatment for Triple- Negative Breast Cancer Part 3</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. G. S. Bhattacharyya</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>22. Inherited Predisposition for Cancer in Diverse Hereditary Cancer Syndromes</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Rajiv Sarin</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip bottom10">
							<span>23. Palliative Care in Breast Cancer </span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Ajay Bapna</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>