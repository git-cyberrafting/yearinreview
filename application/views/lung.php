<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/lunghead2.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">13<sup>th</sup>-15<sup>th</sup> MAY 2016</span>

			</div>
		</div>
		<div class="container">
			<div class="back-green">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightgreen-stip">
							<span>Afatinib: The complete story in the management of EGFR mutation positive NSCLC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Jason Lester</p>
						</div>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgreen-stip">
							<span>Implementing trial data into clinical practice: An Indian experience with PAN ErbB inhibition</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. S. Nithya</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgreen-stip">
							<span>Panel Discussion : Chennai</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Raju Titus Chacko</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Venkatraman, Dr. Ayan Bhattacharya, Dr. Anita Ramesh, Dr. Nithya VS Hospitals</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgreen-stip">
							<span>Panel Discussion : Delhi</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Shyam Aggarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ullas Batra, Dr. Amit Aggarwal, Dr. Nitesh Rohtagi,Dr. Navnit Singh, Dr. Amanjit Bal, Dr. Manish Singhal, Dr. Hemant Malhotra</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightgreen-stip">
							<span>Panel Discussion : Mumbai</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. B.K. Smruti</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Gaurav Gupta, Dr. Shibashish Bhattacharya, Dr. Tejinder Singh,
								Dr. Shailesh Bondarde, Dr. Anuradha Chougule</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>