<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/wcihead.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>wci2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 1, SATURDAY, 10<sup>th</sup> OCTOBER 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-purple">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightpurple-stip">
							<span>Clinical Audits and Quality Assurance in Management of Gynecological Cancers</span>
						</div>
						<div>
							<p class="sentence">Speaker : Rajendra Kerkar</p>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="purple-stip">
							<span>Session I: Cervical Cancer</span>
						</div>
						<div>
							<p class="pink mlr1">Chairpersons : Surendra Shastri, Nigma Mishra, Kie Hwan Kim</p>
						</div>	
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Quality Assurance in Gynecological Cytology Reporting in Pre-invasive cancers</span>
						</div>

						<div>
							<p class="sentence">Speaker : Radhika Srinivasan</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Essential requirements for reducing cervical cancer mortality 
							through screening programme in India</span>
						</div>

						<div>
							<p class="sentence">Speakers : Sharmila Patil</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Does imaging contribute to staging or management of cervical cancer</span>
						</div>

						<div>
							<p class="sentence">Speakers : Manash Biswas</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Radical Hysterectomy: Essential Quality Standards</span>
						</div>

						<div>
							<p class="sentence">Speakers : Bhupesh Goyal</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Session II: Dr KA Dinshaw Oration</span>
							</div>
							<div>
							<p class="pink mlr1">Speakers : Rajendra Badwe, Shyam Shrivastava, Devieka Bhojwani</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Cervical Cancer: The journey of four decades</span>
						</div>

						<div>
							<p class="sentence">Speakers : Firuza Patel</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Session III: Cervical Cancer</span>
							</div>
							<div>
							<p class="pink mlr1">Chairpersons : Shyam Shrivastava, Santosh Menon, Manash Biswas</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Synoptic pathology reporting for cervical cancers: Audit of practice in tertiary cancer center</span>
						</div>

						<div>
							<p class="sentence">Speakers : Kedar Deodhar</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Optimal standards for minimal access surgery in gynecological cancer</span>
						</div>

						<div>
							<p class="sentence">Speakers : Yogesh Kulkarni</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Does IMRT lead to improved compliance to chemo-radiation?</span>
						</div>

						<div>
							<p class="sentence">Speakers : Christine Meder</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Evolving standards in systemic chemotheraphy</span>
						</div>

						<div>
							<p class="sentence">Speakers : Tarini Sahoo</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Image guided brachytherapy: minimal standards for a quality assured programme in low resource settings</span>
						</div>

						<div>
							<p class="sentence">Speakers : Christine Meder</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				
				<!-- section2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="content">
							<div class="purple-stip">
								<span>Session IV: Indian Data Presentation: Gynecological Cancers</span>
							</div>
							<div>
							<p class="pink mlr1">Chairpersons : Jitendra Singh, Sudeep Gupta, Amita Maheshwari</p>
						</div>
						</div>
					</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Open and Robotic Hysterectomy: Morbidity and Outcomes</span>
						</div>

						<div>
							<p class="sentence">Speakers : Rupinder Sekhon</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Phase III RCT of RT vs. CTRT in Ca Cervix IIIB</span>
						</div>

						<div>
							<p class="sentence">Speakers : Shyam Shrivastava</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Compliance to Chemoradiation: Audit from Tertiary Cancer Centre.</span>
						</div>

						<div>
							<p class="sentence">Speakers : Siddhanna Palled</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Compliance to Concurrent chemoradiation: Institutional Audit</span>
						</div>

						<div>
							<p class="sentence">Speakers : Vinita Trivedi</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Patterns of nodal relapse after chemoradiation for cervical cancer</span>
						</div>

						<div>
							<p class="sentence">Speakers : Bhavana Rai</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Compliance to chemoradiation in Elderly</span>
						</div>

						<div>
							<p class="sentence">Speakers : Geeta Muttath</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Audit of compliance and toxicity with IMRT</span>
						</div>

						<div>
							<p class="sentence">Speakers : Srinivas Chilukuri</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightpurple-stip">
							<span>Update on Phase III RCT of IMRT vs 3DCRT in Ca Cervix</span>
						</div>

						<div>
							<p class="sentence">Speakers : Dayanand Sharma</p>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section4 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>