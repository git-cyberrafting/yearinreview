<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead1.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SUNDAY, 31<sup>st</sup> AUGUST 2014</span>

			</div>
		</div>
		<div class="container">
			<div class="back-red1">
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightred1-stip">
							<span>Welcome</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Dr. Shripad Banavali</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>The art of ALL induction</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Shripad Banavali</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Intrathecial therapy in ALL:Do's & Don'ts</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Girish Chinnaswamy</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Monitoring MRD in ALL:Case based insight for clinicians</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. Mani Subramanian</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelist :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Shripad Banavali, Dr. Sheila Weitzman, Dr. Deepak Bansal, Dr. Manju Sengar, Dr. Prashant Tembhare, Dr. Nikhil Patkar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Clinical problem in CNS- Directed therapy</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sheila Weitzman</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Fine tuning maintenance treatment of ALL</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Bharat Agarwal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Managing complication: Case based discussion</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>  Dr. M B Agarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelist :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sheila Weitzman, Dr. Mukesh Desai, Dr. Tushar Vora, Dr. S Chandrakala, Dr. Maya Prasad, Dr. Shweta Bansal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Bedside challenges in risk stratifications of pediatric NHL</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>  Dr. Shailesh Kanvinde</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Current therapy and issues in B-NHL including role of Rituximab</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sheila Weitzman</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>T-NHL: Addressing the contoversies in management - A case based discussion</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Brijesh Arora</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelist :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Dr. Sheila Weitzman, Dr. Santanu Sen, Dr. Anupama Borker, Dr. Reetu Jain, Dr. Purvi Kadakia, Dr. Sangeeta Mudliar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>LCH: Defining organ involvement and appropriate risk refinement in 2014</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Rashmi Dalvi</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Practical lessons in high risk LCH</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Dr. Sheila Weitzman</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightred1-stip">
							<span>Therapeutic dilemmas in LCH: Pearls from clinic</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color: #b45d8c">Moderator : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Gaurav Narula</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelist :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sheila Weitzman, Dr. Nitin Shah, Dr. Mamata Manglani,Dr. Swati Kanakia, Dr. Ratna Sharma, Dr. Vibha Bafna</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>