<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/converge2014-head.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>converge2014" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 2, SUNDAY, 21<sup>st</sup> SEPTEMPER 2014</span>
		</div>
		</div>
		<div class="container">
			<div class="back-violet">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightviolet-stip bottom10">
							<span>Recent Advances Session 2: Role of Circulating Tumor Cells in Breast Cancer </span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Dr. Purvish Parikh</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>International Speaker 2 (Biological Mechanism and Clinical Implications of ER in BC)</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Christian Jackish</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion 4 Management of Breast Cancer in young (<30 yrs) patients</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. K Govind Babu</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ajay Gogia, Dr. Shailesh Bondarde, Dr. KM Parthasarathy,Dr. Sandeep Jasuja, Dr. Nagaraj Huilgol</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				</div>
		</div>
	</section>
	<?php include('include/footer.php');?>