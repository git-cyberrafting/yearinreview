<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/convergehead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>converge2015" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 2, SATURDAY, 23<sup>rd</sup> AUGUST 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-violet">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightviolet-stip bottom10">
							<span>Role of Everolimus in the Management of Hormone Receptor Positive Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Hope Rugo</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Indian Data on Everolimus</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Jyoti Bajpai, Dr. Ankur Bahl, Dr. Shekar Patil</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion - Optimizing Adherence and Duration of Endocrine Therapy in Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ankur Bahl</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ghanshyam Biswas, Dr. C J Tamane, Dr. Prasanna, Dr. J B Sharma,
							Dr. Somashekar, Dr. A R Lone, Dr. Sajjan Rajpurohit</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Panel Discussion - Optimising QoL for Patients on 2nd Line Systemic Therapy for Hormone Receptor Positive Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Raju Titus Chacko</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Prasad Gunari, Dr. A V S Suresh, Dr. Hope Rugo, Dr. K M Parthasarathy, Dr. Manish Singhal, Dr. Bharath Vaswani</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Is There Any Role for Peripheral Blood Testing for Predicting Outcome - Crystal Ball Gazing for Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p> Dr. Amit Agarwal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightviolet-stip bottom10">
							<span>Debate- Is Tumor Biology Important in Young Women with Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color: #b45d8c">Moderator  :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Shyam Aggarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">For   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ramesh Sareen, Dr. Vashisht Maniar, Dr. Mukul Goyal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Against   :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Dipanjan Panda, Dr. Umang Mittal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view-ibcr">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>