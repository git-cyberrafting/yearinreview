<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/tumor-head1.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan">SATURDAY, 7<sup>th</sup> MAY 2016</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
	
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 1: Er+Ve Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Abhijit Basu, Dr. J K Singh</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section 3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 1: Hormonal Therapy</span>
						</div>
						<div class="col-sm-12 col-xs-6 videoPlayerDiv">
                                                  <iframe  src="<?=url('video?v1=Phocon2016/onco/Day2/1.%20Dr.Smita%20Bhatia.mp4&v2=Phocon2016/onco/Day2/1.%20Dr.Smita%20Bhatia.mp4');?>" frameborder="0" allowfullscreen></iframe>
						</div> 
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. T Raja</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Arunangshu Kar, Dr. Saurabh Kumar, Dr. Abhijit Sarkar,Dr. A K Malhotra, Dr. Linu Jacob, Dr. Koushik Chatterjee</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a  href="javascript:void(0);" class="videoPlayerLink"><img src="<?= assets('images/view-btn.jpg');?>"  style="width:72px;"></a>	
					</div>
				
				</div><!-- row-->
				<!-- section 2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 2 : Loco-Regional Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Subhasish Sarkar, Dr. Prabir Bijoy Kar</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section 3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 2: Neoadjuvant Therapy</span>
						</div>
                                             <div class="col-sm-12 col-xs-6 videoPlayerDiv">
                                                  <iframe  src="<?=url('video?v1=Phocon2016/onco/Day2/1.%20Dr.Smita%20Bhatia.mp4&v2=Phocon2016/onco/Day2/1.%20Dr.Smita%20Bhatia.mp4');?>" frameborder="0" allowfullscreen></iframe>
						</div>  
                                            
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Madhuchanda Kar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Amit Dutt Dwary, Dr. Saikat Gupta, Dr. Suprana Ghosh Roy,Dr. Shekhar Keshri, Dr. Chandragouda Dodagoudar, Dr. Sudipto Roy</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
					<a  href="javascript:void(0);" class="videoPlayerLink"><img src="<?= assets('images/view-btn.jpg');?>"  style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate 2: That regional nodal radiotherapy is required in all early stage node + ve Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Prasenjit Chatterjee</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">For :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Suman Mullick</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Against :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Tanmoy Mukhopadhyay</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				<!-- section 2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 3: Her2+ve Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Srikrishna Mondal, Dr. Arnab Gupta</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section 3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 3: Targeted Therapy Sequencing</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. B.K.Smruti</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sharadwat Mukhopadhyay, Dr. Amit Kumar, Dr. Abhishek Basu, Dr. P N Mohapatra, Dr. Santa Ayyagri, Dr. Naresh Somani, Dr. Subroto Saha</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate 3: Should surgery be offered for loco regional control in Metastatic breast cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Arunabha Sengupta</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">For :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Diptendra Sarkar</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Against :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Vinay Deshmane</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
					<!-- section 2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 4 : Her2 -ve/ Er+ve Metastatic Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Shyamal Sarkar, Dr. Litan Naha Biswas</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section 3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 4: Endocrine Resistance</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Dinesh Pendharkar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sibashish Bhattacharya, Dr. Rakesh Roy, Dr. Sanjoy Chatterjee,Dr. Linu Jacob, Dr. Manisha Singh, Dr. Ravi Mohan, Dr. Amitabh Ray</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate 4: That targeted therapy and endocrine treatment should be combined upfront before endocrine resistance develops</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sharadwat Mukhopadhyay</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">For :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Naresh Somani</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Against :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anita Ramesh</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
					<!-- section 2 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 5: Triple Negative Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Anil Poddar, Dr. Santanu Pal</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section 3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Case 5: TNBC</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Ullas Batra</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anita Ramesh, Dr. B.K.Smruti, Dr. T Raja, Dr. Vinay Deshmane,
								Dr. Sudipto Roy, Dr. Pritanjali Singh, Dr. Sumant Gupta</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate 5: That all TNBC should receive a platinum agent in adjuvant settings</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Rakesh Roy</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">For :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sushant Mittal</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Against :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Kumardeep Dutta</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				<!--end-->
				<!-- section 5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Satellite Symposium - Emcure</span>
						</div>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion: Paradigm Shift In Management of Her 2 Positive Early Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#0c7abf">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. A K Malhotra, Dr. J K Singh</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sharadwat Mukhopadhyay</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Ullas Batra, Dr. Anita Ramesh, Dr. Joydeep Purkayastha, Dr. PN Mohapatra, Dr. Col. P Suresh, Dr. Sanjoy Chatterjee, Dr. Dinesh Pendharkar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 5 end-->
				<!-- section 5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Satellite Symposium - Pfizer</span>
						</div>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Role of Cyclin-Dependent Kinase 4/6 inhibitors in Endocrine Therapy for Metastatic Breast Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Speaker  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Chanchal Goswami</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion: Understanding place of CDK 4/6 inhibitors in Indian treatment landscape</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Chanchal Goswami</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. A K Malhotra, Dr. Madhuchanda Kar, Dr. Ullas Batra, Dr. Vineet Talwar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 5 end-->
				<!-- section 5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Satellite Symposium - Zydus</span>
						</div>
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Development of Vivitra</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Speaker  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Sanjeev Kumar</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel Discussion: How Comfortable Are Doctors Using Biosimilar Trastuzumab?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Dr. Amit Dutt Dwary</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. P N Mohapatra, Dr. Sandip Ganguly, Dr. Saurabh Kumar,Dr. Amitabh Ray, Dr. Anita Ramesh, Mr. Samir Desai, Dr. Sanjeev Kumar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 5 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>