<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead3.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>	
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>4th-siop2" ><img src="<?= assets('images/2.png');?>"></a></span>		
			<span class="insidespan">SATURDAY, 6<sup>th</sup> FEBRUARY 2016</span>

			</div>
		</div>
		<div class="container">
			<div class="back-blue1">
					<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="lightblue1-stip">
							<span>Nutritional Implications Of Cancer and Cancer Treatment</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p> Ronald Barr, MD </p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Nutritional Status,Toxicity and Outcome </span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Elena J Ladas, PhD,RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>4th International SIOP-PODC Workshop On Nutrition in Children with cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Brijesh Arora, RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Nutrition and Childhood Malignancies: What have we been doing?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Amita Trehan, MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Realtionship of Baseline Nutritional Status and Socio-Demographic Factors in Childhood Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Sameer Bakhshi, MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Burden and Impact of malnutrition in India the TMH Perspective</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Tushar Vora, MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Low Vitamin D levels are Associated with an Adverse Clinical Outcome in Febrile Neutropenia</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Vineeta Gupta, MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Survey of Nutrition related myths and practise in  families with childhood cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Vekatraman Radhakrishnan, MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Nutritional Issues in Haematological Cancers</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p>Gaurav Narula, MD</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Nutritional Issues in Solid Tumors</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Girish Chinnaswamy ,MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Nutritional Risk Assesssment and Review of Algorithms</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Karina Viana, RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Dietary Counselling and Neutropenic Diet- Fact and Myths</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Madhu Sharma, PhD,MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Assessment of Nutritional Requirements in Children</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Maha Barber,MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Enter Nutrition: Where ,When,How and How long?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Karina Viana,RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Micronutrients Needs and Supplementation in Cancer</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Elena J Ladas PhD,MD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Parenteral Nutrition: Where ,When,How and How long?</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Tiasa Sinha,RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
					<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="lightblue1-stip">
							<span>Management of Nutrition in Stem Cell Transplantation</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Speaker :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Tiasa Sinha,RD</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>