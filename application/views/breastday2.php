<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="breast-cancer">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/breastcancer.png');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span style="float:right;margin-right:3em;">Day <a href="<?=url('');?>breast-cancer" ><img src="<?= assets('images/1.png');?>" style="width:20px"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>" style="width:20px"></a></span>
			<span style="float:right;margin-right:1em;">DAY 2, SATURDAY, 22nd JANUARY 2017</span>
		
		</div>
	</div>
	<!-- section 1-->
	<div class="container">
		<div class="back-blue">
			<div class="blue-stip">
				<span>Session 6 : Supportive Care</span>
			</div>
			<div>
				<p class="pink mlr1">Chairpersons: Indumati Ambulkar, Ranga Rao</p>
			</div>
			<div>
				<p class="sentence">Clinical performance of the DigniCap system, a scalp hypothermia system, in preventing chemotherapy induced alopecia</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Hope Rugo</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2015 ASCO Annual Meeting, Abstract 9518</p>
				</div>
			</div>
			<div>
				<p class="sentence">Scalp-Cooling Device Safe and Effective in Reducing Chemotherapy-Induced Alopecia in Women With Breast Cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Julie Nangia</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S5-02</p>
				</div>
			</div>
			<div>
				<p class="sentence">Acupuncture for chemotherapy-induced peripheral neuropathy in breast cancer, preliminary results of a pilot randomized controlled trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: W Lu</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS PD4-01</p>
				</div>
			</div>		
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Jyoti Bajpai</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">Randomized, placebo-controlled trial of duloxetine for aromatase inhibitor (AI) associated musculoskeletal symptoms (AIMSS) in early stage breast cancer (SWOG 1202)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Henry NL</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S5-06</p>
				</div>
			</div>
			<!--new-->
			<div>
				<p class="sentence">Higher Rate of Severe Toxicities in Obese Patients Receiving dose-dense (dd) Chemotherapy according to Unadjusted Body Surface Area- Results of the Prospectively Randomized GAIN study</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: J. Furlanetto</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Ann Oncol. 2016 Nov;27(11):2053-2059</p>
				</div>
			</div>
			<!-- new end-->	
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Krishna Prasad</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">Olanzapine for the Prevention of Chemotherapy-Induced Nausea and Vomiting</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Rudolph M. Navari</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: NEJM N Engl J Med2016;375:134 142</p>
				</div>
			</div>
			<div>
				<p class="sentence">Denosumab for the prevention of symptomatic skeletal events (SSEs) in patients with bone-metastatic breast cancer: A comparison with skeletal-related events (SREs)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: J-J. Body</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ESMO 1463 P</p>
				</div>
			</div>
			<div>
				<p class="sentence">Denosumab versus zoledronic acid to prevent aromatase inhibitors-associated fractures in postmenopausal early breast cancer; a mixed treatment meta-analysis</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Omar Abdel-Rahman</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Expert Rev Anticancer Ther. 2016 Aug; 16(8): 885-91</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Vijay Agarwal</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">Aromatase Inhibitors: Is Cancer Benefit Worth Cardiac Risk?</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Anne H. Blaes</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S5-07</p>
				</div>
			</div>
			<div>
				<p class="sentence">Are aromatase inhibitors associated with higher myocardial infarction risk in breast cancer patients? A Medicare population study</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: S Kamaraju</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS PD4-07</p>
				</div>
			</div>
			<div>
				<p class="sentence">The risk of myocardial infarction with aromatase inhibitors relative to tamoxifen in post menopausal women with early stage breast cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Husam Abdel-Qadir</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: EJC,November 2016 Volume 68, Pages 11 â€“21</p>
				</div>
			</div>
		</div>
	</div>
	

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Hemant Malhotra</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
		</div>
	</div>
	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip" style="margin-top: -24px;">
				<span>Chairpersons Remarks</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip" style="margin-top: -24px;">
				<span>Panel Discussion on Supportive Care</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="pink mlr2">Chairpersons: Jeremy Pautu, Shekhar Salkar</p>
				<p class="pink mlr2">Moderator: Bhawna Sirohi</p>
				<p class="mlr2" style="color:#330033">Panelists :Dinesh Pendharkar, Shekhar Patil, Nirmal Raut, Ashish Kaushal,Gaurav Gupta,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Anubha Bharthuar, Manish Kumar, Amol Dongre,Nikhil Ghadyalpatil, Arun Warrior, Kishore Kumar</p>
			</div>
		</div>
	</div>
	<!-- section1 end-->

	<!-- section2-->
	<div class="container">
		<div class="back-blue">
			<div class="blue-stip">
				<span>Session 7 : ER+Ve Breast Cancer</span>
			</div>
			<div>
				<p class="pink mlr1">Chairpersons : Hemant Malhotra, Gurpreet Singh</p>
			</div>
			<div>
				<p class="sentence">Addition of ovarian function suppression to endocrine therapy in premenopausal women with early breast cancer: A meta-analysis</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Chlebowski R</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS P2-09-06</p>
				</div>
			</div>
			<div>
				<p class="sentence">Twelve-Month Estrogen Levels in Premenopausal Women With Hormone Receptor-Positive Breast Cancer Receiving Adjuvant Triptorelin Plus Exemestane or Tamoxifen in the Suppression of Ovarian Function Trial (SOFT): The SOFT-EST Substudy</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Bellet M</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: J Clin Oncol. 2016 May 10;34(14):1584-93</p>
				</div>
			</div>
			<div>
				<p class="sentence">Absolute benefit of adjuvant endocrine therapies for premenopausal women with hormone receptor-positive, human epidermal growth factor receptor 2-negative early breast cancer: TEXT and SOFT trials</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Regan MM</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: J Clin Oncol. 2016 Jul 1;34(19):2221-31</p>
				</div>
			</div>	
			<div>
				<p class="sentence">Adjuvant Tamoxifen Plus Ovarian Function Suppression Versus Tamoxifen Alone in Premenopausal Women With Early Breast Cancer: Patient-Reported Outcomes in the Suppression of Ovarian Function Trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Karin Ribi</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: J Clin Oncol. 2016 May 34,(14) 1601-1610</p>
				</div>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Nitesh Rohatgi</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">Can chemotherapy and endocrine therapy be given concurrently in ER positive MBC?</p>
			</div>
			<!-- new end-->	
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Amit Agarwal</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">PALOMA-2: Primary results from a phase III trial of palbociclib (P) with letrozole (L) compared with letrozole alone in postmenopausal women with ER+/HER2- advanced breast cancer (ABC)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Finn RS</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ASCO Annual Meeting. Abstract 507 & N Engl J Med 2016 Nov17 ; 375(20):1925 1936</p>
				</div>
			</div>
			<div>
				<p class="sentence">Impact of palbociclib plus letrozole on health related quality of life (HRQOL) compared with letrozole alone in treatment naive postmenopausal patients</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Hope rugo</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ESMO 225 PD</p>
				</div>
			</div>
			<div>
				<p class="sentence">Ribociclib as First-Line Therapy for HR-Positive, Advanced Breast Cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Gabriel Hortobagyi</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ESMO LBA1 & N Engl J Med. 2016 Nov3 ;375
                     (18):1738-1748</p>
				</div>
			</div>
			<div>
				<p class="sentence">Biological effects of abemaciclib in a phase2 neoadjuvant study for postmenopausal patients with hormone receptor positive, HER2 negative breast cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Hurvitz S</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S4-06</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Shyam Aggarwal</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">BELLE-3 Trial of Buparlisib Plus Endocrine Therapy Meets Primary Endpoint of Progression Free Survival in Breast Cancer Patients</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Di Leo A</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S4-07</p>
				</div>
			</div>
			<div>
				<p class="sentence">The PI3K inhibitor, taselisib, has enhanced potency in PIK3CA mutant models through a unique mechanism of action</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Friedman LS</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S6-04</p>
				</div>
			</div>
			<div>
				<p class="sentence">A randomized adaptive phase II/III study of buparlisib, a pan-Class I PI3K inhibitor, combined with paclitaxel for the treatment of HER2- advanced breast cancer (BELLE-4)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: M. Martin</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Ann Oncol. 2016 Nov 14</p>
				</div>
			</div>
			<div>
				<p class="sentence">Pictilisib PI3Kinase inhibitor (a phosphatidylinositol 3-kinase [PI3K] inhibitor) plus paclitaxel for the treatment of hormone receptor-positive, HER2-negative, locally recurrent, or metastatic breast cancer: interim analysis of the multicentre, placebo controlled, phase II randomised PEGGY study</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: P. Vuylsteke</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Ann Oncol. 2016 Nov 1. pii: mdw562</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Nita Nair</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">Fulvestrant 500 mg versus anastrozole 1 mg for hormone receptor-positive advanced breast cancer (FALCON): an international, randomised, double-blind, phase 3 trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: John F R Robertson</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Lancet. 2017 Dec 17;388(10063):29973005</p>
				</div>
			</div>
			<div>
				<p class="sentence">PrECOG 0102: A randomized, double-blind, phase II trial of fulvestrant plus everolimus or placebo in postmenopausal women with hormone receptor (HR)-positive, HER2 negative metastatic breast cancer (MBC) resistant to aromatase inhibitor (AI) therapy</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Kornblum N</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S1-02</p>
				</div>
			</div>
			<div>
				<p class="sentence">BOLERO-4: Phase 2 trial of first-line everolimus (EVE) plus letrozole (LET) in estrogen receptorâ€“positive (ER+), human epidermal growth factor receptor 2â€“negative (HER2) advanced breast cancer (BC)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: M Royce</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ESMO 2220</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: T Raja</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
		</div>
	</div>
	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip" style="margin-top: -24px;">
				<span>Chairpersons Remarks</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
		</div>
	</div>
	
	<!-- section 2 end-->

	<!-- section 3-->
	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip" style="margin-top: -24px;">
				<span>Key Note Address by Hope Rugo : Extended Adjuvant Endocrine Therapy in Breast Cancer: At Crossroads</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="pink mlr1">Chairpersons: S D Banavali, Vijaykumar DK</p>
			</div>
			<div>
				<p class="sentence">Optimal duration of extended letrozole treatment after 5 years of adjuvant endocrine therapy; results of the randomized phase III IDEAL trial (BOOG 2006-05)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Blok EJ</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S1-04</p>
				</div>
			</div>
			<div>
				<p class="sentence">A randomized, double-blinded, placebo-controlled clinical trial to evaluate extended adjuvant endocrine therapy (5 years of letrozole) in postmenopausal women with hormone-receptor positive breast cancer who have completed previous adjuvant endocrine therapy: Initial results of NRG oncology/NSABP B-42</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Mamounas EP</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S1-05</p>
				</div>
			</div>
			<div>
				<p class="sentence">First results from the multicenter phase III DATA study comparing 3 versus 6 years of anastrozole after 2-3 years of tamoxifen in postmenopausal women with hormone receptor-positive early breast cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Tjan-Heijnen VC</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S1-03</p>
				</div>
			</div>	
			<div>
				<p class="sentence">10-year follow-up and biomarker discovery for adjuvant endocrine therapy; results of the TEAM trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: EJ Blok</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS PD2-07</p>
				</div>
			</div>
			<div>
				<p class="sentence">Predictors of recurrence during years 5-14 in 46,138 women with ER+ breast cancer allocated 5 years only of endocrine therapy (ET)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Hongchao Pan</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ASCO Annual Meeting, Abstract 505</p>
				</div>
			</div>
			<div>
				<p class="sentence">Extending Aromatase-Inhibitor Adjuvant Therapy to 10 Years</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Hurvitz S</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: N Engl J Med. 2016 Jul 21;375(3):209-19</p>
				</div>
			</div>
			<div>
				<p class="sentence">Comparison of EndoPredict and EPclin With Oncotype DX Recurrence Score for Prediction of Risk of Distant Recurrence After Endocrine Therapy</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: J R Buus</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: J Natl Cancer Inst. 2016 Jul 10;108(11)</p>
				</div>
			</div>
			<div>
				<p class="sentence">Annual Hazard Rates of Recurrence for Breast Cancer During 24 Years of Follow-Up: Results From the International Breast Cancer Study Group Trials I to V</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Colleoni M</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Ann Oncol. 2016 May;27(5):806-12</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author" style="color:#330033">Debate:</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Fulvestrant is the appropriate 1st line option for low volume metatstatic Er+Ve disease -Chanchal Goswami</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">vs</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip" style="margin-top: -15px;">
				<span>Letrozole + Palbociclib is the appropriate 1st line option for low volume metatstatic Er+Ve disease - Chirag Desai</span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>

		</div>
	</div>
	<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Panel Discussion on HER2+Ve Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Tomcha Singh, C J Tamane</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Senthil Rajappa</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Chetan Deshmukh, Shirish Alurkar, Bharat Bhosale, Kannan Kalaichelvi, Kaustav Talapatra, Santam Chakraborty, Shailesh Bondarde, Jaya Ghosh, Amit Agarwal, Avinash Deo, Manisha Singh</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->

	<!-- section3 end-->

	<!-- section 4-->
	<div class="container">
		<div class="back-blue">
			<div class="blue-stip">
				<span>Session 8 : Rapid Review: Part 2</span>
			</div>
			<div>
				<p class="pink mlr1">Chairpersons : N Sudhakar, Kajal Shah</p>
			</div>		
		</div>
	</div>


	<div class="container">
		<div class="back-blue">
			<div class="skyblue-stip">
				<span>Reviewer: Shona Nag </span>
			</div><a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>"  style="float:right;width:80px;margin-right:5em;margin-top:-1.5em"></a>
			<div>
				<p class="sentence">1. Impact of radiotherapy on complications and patient-reported satisfaction with breast reconstruction: Findings from the prospective multicenter MROC study</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Jagsi R</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS S3-07</p>
				</div>
			</div>

			<div>
				<p class="sentence">2. US Surgeons Snipe at UK's DCIS Trial, Who, in Turn, Cry Foul.LORIS TRIAL CONTROVERSY</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Melissa Pilewskie</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Medscape, Oct 16 Daniel Rea</p>
				</div>
			</div>
			<div>
				<p class="sentence">3. Expert Critiquie : Multicentric Breast Cancer: OK for Breast Conservation? Data show the surgery is feasible, with good oncologic outcomes</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Laura M. Spring</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Medpagetoday review</p>
				</div>
			</div>
			<div>
				<p class="sentence">4. Noncoding somatic and inherited single-nucleotide variants converge to promote ESR1 expression in breast cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Swneke D Bailey</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Nature Genetics 48, 1260â€“1266(2016)</p>
				</div>
			</div>
			<div>
				<p class="sentence">5. Can surrogate pathological subtyping replace molecular subtyping? Outcome results from the MINDACT trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: F Cardoso</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 SABCS PD7-01</p>
				</div>
			</div>
			<div>
				<p class="sentence">6. Circulating Tumour Cells and pathological complete response: independent prognostic factors in inflammatory breast cancer in a pooled analysis of two multicentre phase II trials (BEVERLY-1 and -2)
	of neoadjuvant chemotherapy combined with bevacizumab</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: J-Y. Pierga</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Annals of oncology, October 18, 2016</p>
				</div>
			</div>
			<div>
				<p class="sentence">7. Overall survival of patients with HER2-negative metastatic breast cancer treated with a first-line paclitaxel with or without bevacizumab in real-life setting: Results of a multicenter national observational study</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: S Delaloge</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ASCO Annual Meeting, Abstract 1013</p>
				</div>
			</div>
			<div>
				<p class="sentence">8. Disease-free (DFS) and overall survival (OS) at 3.4 years (yrs) for neoadjuvant bevacizumab (Bev) added to docetaxel followed by fluorouracil, epirubicin and cyclophosphamide (D-FEC), for women with HER2 negative early breast cancer: The ARTemis trial</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: HM Earl</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ASCO Annual Meeting, Abstract 1014</p>
				</div>
			</div>
			<div>
				<p class="sentence">9. Phase III Trial Evaluating Letrozole As First-Line Endocrine Therapy With or Without Bevacizumab for the Treatment of Postmenopausal Women With Hormone Receptor-Positive Advanced-Stage Breast Cancer: CALGB 40503 (Alliance)</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: Dickler MN</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Reference: Lancet Oncol. 2016 Dec 6. pii: S14702045 (16) 30631-3</p>
				</div>
			</div>
			<div>
				<p class="sentence">10. Effect of progesterone receptor on outcome of women with breast cancer</p>
			</div>
			<div class="col-sm-12">
			<div class="col-sm-3">
				<p class="author">Author: SS Dawood</p>
				</div>
				<div class="col-sm-9">
				<p class="author">Conference: 2016 ASCO Annual Meeting,Abstract 573</p>
				</div>
			</div>
</div>
	</div>
<!-- section5 end-->

</section>
<?php include('include/footer.php');?>