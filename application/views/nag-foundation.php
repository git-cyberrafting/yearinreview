<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/naghead1.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>nag-foundation2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 1, SATURDAY, 16<sup>th</sup> JANUARY 2016</span>
		</div>
	</div>
	<div class="container">
		<div class="back-brown">
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 1 : Local Regional Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">P.K. Jhulka, P. Raghu Ram </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Higher 10-year overall survival after breast conserving therapy compared to mastectomy in early stage breast cancer: a population based study with 37,207 patients</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Van Maaren MC</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Primary results, NRG Oncology/NSABP B-35: A clinical trail of anastrozole (A) versus tamoxifen (tam) in postmenopausal patients with DCIS undergoing lumpectomy plus radiotherapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: R.G. Margolese</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Sidharth Sahni</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Omiting are excision for a focally positive surgical margin after primary breast conserving surgery is safe.</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: E. Vos</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Whole Breast radiotherapy does not affect growth of cancer foci in other quadrants: results from the TARGIT A trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: J. Vaidya</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Comaprison of Complication and Reconstrution Failure in Radiated Patients With Breast Autologous Tissue And Tissue Expaner/Implant Based Reconstruction</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Neil Woody</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: C.B. Koppiker</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Whole exome sequencing of circulating and disseminated tumor cells in patients with metastatic breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D. Peters</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Development and validation of multiplex digital PCR assays on Circulationg tumor DNA in advanced breast cancer: Implemenstation for clinical routine use</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: I. Garcia-Murillas</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Purvish Parikh</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Late side-effect of breast cancer radiotherapy: Second cancer incidence and non breast-cancer moratality among 40,000 women in75 trails</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Taylor C.</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Long term Toxicity and Outcomes Of Hypofractionated radiation Therapy with an Incorporated Boost for Early- Stage Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Lora Wang</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Long term Risk of Breast Cancer Mortality After Local Failure in Women Treated With Breast Conserving Therapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Colin Murphy </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: ASTRO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Accelerated Partial Breast irradiation using Sole Interstitial Multicatheter Brachytherapy vs Whole Breast Irradiation for Early breast CAncer:5-year Results of a Randomized Phase III Trail-Part I: Loacl Control and survical Results</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: V. Strnad</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Anusheel Munshi</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Relationship of omission of adjuvant radiotherapy to outcomes of locoregional control and disease-free survival in patients with or without PCR after neoadjuvant chemotherapy for breast cancer: A meta analysis on 3481 patients from  the Gepar trails</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D. Krug</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Regional nodal irradiation in early-stage brast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Whelan TJ</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: N Engl J Med 373:307-316,2015</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Internal mammary and medial supraclavicular irradiation in breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Poortmans PM</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: N Engl J Med 373:317-327,2015</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Dexamethasone vs Placebo in the Prophylaxis of Radiation induced Pain Flare Following Palliative Radiation Therapy for Bone Metastases: A Double-blind Randomized, Controlled, Superiority Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: E Chow</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Ashwini Budrukkar</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Panel Discussion on Locoregional Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Sanjay Chatterjee</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Pranjali Gadgil, Subhasini John, Ramesh Sarin, Sanjay Sharma, Tabassum W, Mandar Nadkarni, Anil Herror,  Somashekar SP, Kaustav Talpatra, Rucha Kaushik</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section2-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 2 : ER Positive Braest Cancer and others</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">Ramesh Nimmagadda, N K Warrier</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>ALGB 70604 (Alliance): A randomized phase III study of standard dosing vs. longer interval dosing of zeledronic acid in  metastatic cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Andrew Louis Himelstein</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>The Impact of Adjuvant denosumab on disease-free survival: Results from 3,425 postmenopausal patients of the ABCSG-18 trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Gnant M</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Low Hormone receptor (HR) status and the benefit of hormonal therapy (HT) in patients with early breast cancer(EBC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: C. Guarducci</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Rajiv Sarin</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>A Phase III trial of adjuvant capecitabine in breast cancer patients with HER2-negative pathologic residual invasive disease after neoadjuvant chemotherapy( CREATE-X, JBCRG 04)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Lee S-J</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Consensus on breast cancer cell lines classification for an effective and efficient clinical decision making</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: H. Milioli</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Randeep Singh</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>A phase II trail of neoadjuvant palbociclib , a cyclin -dependent kinase (CDK) 4/6 inhibitor, in combination with anastrozole for clinical stage 2 or 3 estrogen receptor positive HER2 negative(ER+HER2-)Breast Cancer(BC)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: MaCX</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference:SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Preliminary efficacy and safety of  pembrolizumab (MK-3475) in patients with PD L1-positive, estrogen receptor-positive(ER+)/HER2-negative advanced breast cancer enrolled in KEYNOTE-028</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Rugo HS</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>High prevalence and clonal hetrogeneity of ESR1 Mutations (MT) in circulating tumor DNA (ctDNA) from patients (pts) enrolled in FERGI, A randomized Phase II study testing Pictilisib (GDC-0941) in combination with fulvestrant(F) in pts that failed a prior aromatase in-hibitor(AI)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Gendreau S</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Govind Babu</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>EndoPredict(EPclin) score for estimating residual distant (DR) risk in ER+/HER2-breast-cancer(br ca) patients treated with 5 year adjuvant endocrine therapy alone: Validation and comparison with the oncotype DX recurrence score(RS)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: DowSett M</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>"BEST ABSTRACT: Prospective trail of endocrine therapy alone in patients with estrogen-receptor positive, HER2-negative, node negative breast cancer: Result of the TAILORc low risk registry"</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author:J Sparano</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Ramesh Nimmagadda</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>I will use oncotype DX in Er+ve Breast cancer patients</p>
						</div>	
					<!--sentence end-->	
				</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->

			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Nitesh Rohatagi</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>I will not use oncotype DX in Er+ve Breast cancer patients</p>
						</div>	
					<!--sentence end-->	
				</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>

			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer:Shahennah Dawood</span>
					</div>
					</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Panel Discussion on ER Positive Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>T Raja</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Govind Babu, Lalit Mohan Sharma, Jyoti Bajpai, Amol Dongre , Ranga Rao, Manisha Singh, Tejinder Singh, Bhavna Parikh, SVSS Prasad, Nikhil Ghadyalpatil</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				
				<!--section 2end-->
				<!--section3-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="brown-stip">
							<span>Session 3 : HER2 Positive Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">D C Doval, CJ Tamane</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Neratinib after adjuvant chemotherapy and trastuzumab in HER2 positive early breast cancer: Primary analysis at 2 years of a phase 3, randomized, placebo-controlled trial (ExteNET)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Chan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Neratinib after trastuzumab-based adjuvant therapy in early stage HER2+ breast cancer: 3 year analysis from a phase 3 randomized, placebo-controlled, double-blind trial (ExteNET)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Chan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>NSABF FB-7: A phase II randomized trial evaluating neoadjuvant therapy with weekly paclitaxel (P) plus neratinib (N) or trastuzumab (T) or neratinib and trastuzumab (N+T) followed by doxorubicin and cycliphosphamide (AC) with postoperative T in women with locally advanced HER2 positive breast cancer.</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Jacobs S </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>A phase III randomized study of paclitaxel and trastuzumab versus paclitaxel alone for early stage, ER and PR receptor negative and HER2- positive breast cancer as adjuvant treatment</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: M. Singhal	</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Manish Singhal</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Final analysis of WSG-ADAPT HER2+/H2+ phase II trial: Efficacy, safety, and predictive markers for 12-weeks of neoadjuvant TDM1 with or without endocrine therapy versus trastuzumab + endocrine therapy in HER2-positive hormone receptor-positive early breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Harbeck N</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Efficacy of 12-weeks of neoadjuvant TDM1 with or without endocrine therapy in HER2 positive hormone-receptor positive early breast cancer: WSGADAPTHER2/ HR phase II trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Harbeck 2</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Phase III, randomized study of trastuzumab emtasine (T Dm 1),Pertuzumab (P) vs trastuzumab taxane (HT) for first line treatmentOf HER2 positive MBC: primary results from MARIANNE study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: P A Ellis	</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: TP Sahoo</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>“ORAL Neoadjuvant chemotherapy with trastuzumab or lapatinib: Survival analysis of the HER2 positive cohort of the Gepar Quinto study (GBG 44)”</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: M. Untch</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Five-year analysis of the phase II NeoSphere trial evaluating four cycles of neoadjuvant docetaxel (D) and/ or trastuzumab (T) and/ or pertuzumab (P)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: L. Gianni</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Sachin Hingmire</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>The effect of trastuzumab-based therapy on overall survival in small, node-negative HER2-positive breast cancer: To treat or not to treat?</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Van Ramshorst MS</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Prophylactic beta blockage preserves left ventricular ejection fraction in HER2 overexpressing breast cancer patients receiving trastuzumab: Primary results of the MANTICORE randomized controlled trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Pituskin E</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Adwaita Gore</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Functional subtyping with Blue Print 80-gene profile to identify distinct triple-positive subtypes with and without trastuzumab/chemosensitivity. 2015 Breast Cancer Symposium</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Peter D. Beitsch </p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO breast Symposium</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Discordance between HER2-phenotype on circulating tumor cells and primary tumor in women with advanced breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Schramm	</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer:Nitesh Lokeshwar</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Panel Discussion on HER Positive Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5> B K Smruti, Hemant Malhotra</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Avinash Deo, Vinod Raina, Anita Ramesh, Shaheenah Dawood, Anand Pathak, Indumati Ambulkar, Arvind Kumar, Gautam Goyal, Anjana Sainani, Tanveer Maksud</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Rapid Reviews-I</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section 3end-->
				<!--section4-->
				
			<!-- start-->
			
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="lightbrown-stip">
						<span>Reviewer: Shona Nag </span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>1. cfDNA analysis from BOLERO-2 plasma samples identifies a high rate ESR1 mutations: Exploratory analysis for prognostic and predictive correlation of mutations reveals different efficacy outcomes of endocrine therapy based regimens</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Chandarlapatys	</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>2. PIK3CA status in circulating tumor DNA (ctDNA) predicts efficacy of buparlisib (BUP) plus fulvestrant (FULV) in pstmenopausal women with endocrine-resistant HR+/HER2- advanced breast cancer (BC): First results from the randomized, Phase III BELLE-2 trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: J. Baselga</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>3. Effects of perioperative lapatinib in early HER2+ breast cancer – The UK EPHOS-B trial (CRUK/08/002)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: N. Bundred</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>4. High prevalence and clonal heterogeneity of ESR1 mutations (mt) in circulating tumor DNA (ctDNA) from patients (pts) enrolled in FERGI, a randomized Phase II study testing pictilisib (GDC-0914) in combination with fulvestrant (F) in pts that failed a prior aromatase inhibitor(AI)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: S. Gendreau</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>5.	A surrogate gene expression signature of tumor infiltrating lymphocytes (TILs) predicts degree of benefit from trastuzumab added to standard adjuvant chemotherapy in NSABP (NRG) trial B31 for HER2+ breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Seong-Rim Kim	</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: AACR</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>6. P53 mutations in HER2 positive and triple negative breast cancer treated with neoadjuvant chemotherapy – A translational subproject of Gepar Sixto trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: S. Darb-Esfahani</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>7.	Somatic BRCA 1 mutations determine different responses to platinum-based chemotherapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: E. Ignatova</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>8.	Histology and molecular characteristics of pregnancy associated breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Z. Tomasevic</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>9.	Evaluation of PDL1 expression in breast cancer by immunohistochemistry</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe;">Author: C. Solinas</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>10.	Targeting unique metabolic vulnerabilities in dormant ER & plus; tumor cells</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: E. Knudsen</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>11. Smart delivery of lapatinib to reduce its cardiotoxicity: A 99mTc labeled biodistribution study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Khanna</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>12.	230 Clinical patterns of metastatic spread from formal in fixed, paraffin-embedded (FFPE) expression profiles: A case-control study of 1,357 breast cancer patients</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: K. Lawler</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>13.	Evolution from primary breast cancer to relapse and metastasis</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: L. Yates</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>14.	The driver landscape of breast cancer metastasis and relapse. 2015 European Cancer Congress</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: L. Yates</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: SABCS</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>15.	Final updated results of the NRG Oncology/ NSABP ProtocolP2: Study of Tamoxifen and Raloxifene (STAR) in preventing breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
							<div class="col-sm-3">
							<p style="color:#0082fe">Author:D.L. Wickerham </p>
							</div>
							<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASCO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>16.	Breast MRI screening of women at average risk of breast cancer: An observational cohort study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Christiane K. Kuhl</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">
				Conference: ASCO Breast Symposium</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>17.	Agreement in risk assessment among breast cancer specialists: A survey within the MINDACT cohort</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: C. Drukker</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>18.	Male breast cancer receptor sub-types: Demographics, tumor characteristics and short term survival outcomes</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: S. Yadav</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div><div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>19.	Examination of the sentinel lymph node identification in breast cancer combining contrast enhanced ultrasonography and the blue dye method without radioisotope</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: K. Futsuhara</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference:IMPAKT</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>20.	Association between HER2 phenotype on circulating tumor cells and primary tumor characteristics in women  with metastatic breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: W. Janni</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ESMO</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>21.	Targeting HER2 in patients with HER2-negative metastatic breast cancer with elevated serum levels of the HER2 extracellular domain and/or HER2-positive circulating tumor cells in the clinical routine</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: C. Kurbacher</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: IMPAKT</p>
						</div>
						</div>
						
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
			<!--end-->
			
		</div>
	</div>
</section>
<?php include('include/footer.php');?>