<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/niccihead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn" style="color: #fff !important"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>nicci" ><img src="<?= assets('images/1.png');?>"></a> <a  href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 2, SUNDAY, 15<sup>th</sup> NOV 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
			<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Lighting the lamp</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>The future of breast cancer research in India â€“ Improving outcomes and bettering lives?</span>
						</div>	
						<div class="col-sm-12">
						<div class="col-sm-5">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-7 col-sm-pull-1">
							<p>Ramesh Nimmagadda, Chennai; Sandeep Kumar, Bhopal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-5">
							<p>Speaker  : </p> 
						</div>
						<div class="col-sm-7 col-sm-pull-1 abc-top">
							<h5>S D Banavali, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
					<div class="col-sm-2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Integrative oncology</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<div class="row">
					<div class="col-sm-10 verticalline">
							<div class="col-sm-12">
						<div class="col-sm-5">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-7 col-sm-pull-1">
							<p>Nirupa Bareja, Bengaluru; Aditya Chaubey, MSMF, Bengaluru;
							Vijay Anand Reddy, Hyderabad</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-5">
							<p>Speaker  : </p> 
						</div>
						<div class="col-sm-7 col-sm-pull-1 abc-top">
							<h5>Paul Salins</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Patient advocacy led by cancer charities</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Susan G Komen for the Cure:>Eric P. Winer, USA, Co-chair of the scientific advisory board</p>
						</div>
						</div>

						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Womenâ€™s Cancer Initiative-Tata Memorial Hospital (WCI TMH): Devieka Bhojwani, Mumbai, Vice President</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Breast cancer Now: Trevor Powles, UK, Vice-Chairman</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Breast cancer Research foundation (BCRF): Paul Salins, Bangalore, Managing Director</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Mazumdar Shaw Medical Foundation (MSMF): Paul Salins, Bangalore, Managing Director</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Cansupport: Harmala Gupta, Delhi, Founding President</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Nag foundation: Shona Nag, Pune, Trustee View </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Max Foundation : Viji Venkatesh, India, Country Head Indian Cancer Society (ICS):Purna Kurkure, Mumbai, Member, Managing committee</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>UshaLakshmi breast cancer Foundation (UBCF): P. Raghuram, CEO and Director.</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Nargis Dutt Memorial Charitable Trust (NMDCT): Priya Dutt, MP, Mahrashtra, Trustee View </p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Pink Warriors: Mamta Mathur, Delhi, Founder Member and Partner</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>New India Cancer Charity Initiative (NICCI):Shilpi Sirohi Singh, Meerut, Founding President</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section2 end-->
					<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Panel on Patient Advocacy : Impacting breast cancer policies and patient empowerment in India</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Maya Sharma, Bengaluru; Shona Nag, Pune</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Kiran Mazumdar Shaw, Bengaluru; Devika Bhojwani, DCGI, mumbai;
								Ravi Mehrotra (ICMR ), Priya Dutt, Mumbai; RA Badwe, Mumbai;
								Eric P. Winer, USA; P. Raghuram, Hyderabad</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>My breast cancer journey - Rachna Singh</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				</div>
		</div>
	</section>
	<?php include('include/footer.php');?>