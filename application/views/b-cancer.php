<?php 
	include('include/header.php');
	include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/breastcancer.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn">&nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>b-cancer2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan">DAY 1, SATURDAY, 21<sup>st</sup> JANUARY 2017</span>
		</div>
	</div>
	<div class="container">
		<div class="back-blue">
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="blue-stip">
							<span>Session 1 : Locoregional Therapies in Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">C B Koppiker, Gaurav Agarwal</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Locoregional Recurrence After Sentinel Lymph Node Dissection With or Without Axillary Dissection in Patients With Sentinel Lymph Node Metastases: Long-term Follow-up From the American College of Surgeons Oncology Group (Alliance) ACOSOG Z0011 Randomized Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Giuliano AE</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Ann Surg. 2016 Sep;264(3):413-20</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Sentinel node detection after neoadjuvant chemotherapy in patient without previous axillary node involvement (GANEA 2 trial): follow-up of a prospective multi-institutional cohort</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Jean-Marc Classe</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S2-07</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Ramesh Sarin</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Society of Surgical Oncology - American Society for Radiation Oncology - American Society of Clinical Oncology Consensus Guideline on Margins for Breast-Conserving Surgery With Whole-Breast Irradiation in Ductal Carcinoma In Situ</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Monica Morrow</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol 34:4040-4046</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Neoadjuvant Endocrine Therapy for Estrogen Receptor - Positive Breast Cancer - A Systematic Review and Meta - analysis</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Laura M. Spring</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Nov 1;2(11):1477-1486</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Selvi Radhakrishna</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>After 10 Years, Postmastectomy Radiation Is Shown to Improve Local Control But Not Overall Survival in Women with Breast Cancer and One to Three Positive Nodes Practice Update Editorial Team</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Tam Moses</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO Abstract 326</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Postmastectomy Radiotherapy: An American Society of Clinical Oncology, American Society for Radiation Oncology, and Society of Surgical Oncology Focused Guideline Update</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Recht A</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Dec 20;34(36):44314442</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>NRG Oncology/RTOG 1014: 3 Year Efficacy Report From a Phase II Study of Repeat Breast Preserving Surgery and 3D Conformal Partial Breast Re-Irradiation (PBrI) for In Breast Recurrence</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D. W. Arthur</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO LBA-10</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Anusheel Munshi</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Nipple-Sparing Mastectomy Shown to Be Safe - and Increasingly Preferred</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: James Jakub,  Tina J. Hieken</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: American Society of Breast Surgeon Abstract 0173 and  ASCOPOST review</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>10 year survival after breast-conserving surgery plus radiotherapy compared with mastectomy in early breast cancer in the Netherlands: a population-based study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Marissa C van Maaren</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: The Lancet Oncology Volume 17, No. 8, p1158-1170,August 2016</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Anupama Mane</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Postoperative Stereotactic Radiosurgery Versus Observation for Completely Resected Brain Metastases: Results of a Prospective Randomized Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: A. Mahajan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Int J Radiat Oncol Biol Phys, October 1, 2016 Volume 96,Issue 2, Supplement, Page S2</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>N107C/CEC.3: A Phase III Trial of Post - Operative Stereotactic Radiosurgery (SRS) Compared with Whole Brain Radiotherapy (WBRT) for Resected Metastatic Brain Disease</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: P. D. Brown</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: ASTRO LBA 1</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Effect of Radiosurgery Alone vs Radiosurgery With Whole Brain Radiation Therapy on Cognitive Function in Patients With 1 to 3 Brain Metastases A Randomized Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Paul D. Brown</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA. 2016 Jul 26;316(4):401-9</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Ashwini Budrukkar</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Chairpersons Remarks</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Panel Discussion on Locoregional Therapies in Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Rajesh Vashistha, Vijay Haribhakti</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Sanjoy Chatterjee</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Anupama Mane, Shalaka Joshi, Archana Shetty, Anil Heroor,Namita Pandey, Tabassum W., V Kannan, Vedant Kabra,Sanjay Sharma, Nita Nair, Vikram Maiya, Manish Chandra</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section2-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="blue-stip">
							<span>Session 2 : Translational Science</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">Ramesh Nimmagadda, R K Deshpande</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Whole exome and transcriptome sequencing of resistant ER+ metastatic breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Cohen O</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S1-01</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>The genomic landscape of male breast cancers</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Salvatore Piscuoglio</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Aug 15;22(16):4045-56</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Genomic Characterization of Primary Invasive Lobular Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Christine D</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Jun 1;34(16):1872-81</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Amit Verma</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Prognostic and predictive value of circulating ESR1 mutations in metastatic breast cancer patients (mBC) progressing under aromatase inhibitor (AI) treatment</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: L Augusto</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 511</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Plasma ESR1 Mutations and the Treatment of Estrogen Receptor-Positive Advanced Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Charlotte Fribbens</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Sep 1;34(25):2961-8</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Rejiv Rajendranath</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>PAM50 intrinsic subtype as a predictor of pathological complete response following neoadjuvant dual HER2 blockade without chemotherapy in HER2-positive breast cancer: First results of the PAMELA clinical trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Prat Aparicio A</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S3-03</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>70-Gene Signature as an Aid to Treatment Decisions in Early-Stage Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Fatima Cardoso</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: AACR Abstract CT039 & N Engl J Med2016; 375:717729</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Comparison of EndoPredict and EPclin With Oncotype DX Recurrence Score for Prediction of Risk of Distant Recurrence After Endocrine Therapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Richard Buus</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JNCI J Natl Cancer Inst (2016) 108(11):djw149</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Manish Singhal</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Efficacy and tolerability of veliparib (V; ABT-888) in combination with carboplatin and paclitaxel (P) vs placebo (Plc)+C/P in patients (pts) with BRCA1 or BRCA2 mutations and metastatic breast cancer: A
randomized, phase 2 study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Han HS</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S2-05</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>DNA repair deficiency biomarkers and MammaPrint high1/(ultra) high 2 risk as predictors of veliparib / carboplatin response: Results from the neoadjuvant I-SPY 2 trial for high risk breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Wolf DM</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S2-06</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Adaptive Randomization of Veliparib-Carboplatin Treatment in Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: HS Rugo</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: N. Engl. J. Med; 2016 Jul 7; 375(1)23-34</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Adwaita Gore</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Molecular Alterations and Everolimus Efficacy in Human Epidermal Growth Factor Receptor 2-Overexpressing Metastatic Breast Cancers: Combined Exploratory Biomarker Analysis From BOLERO-1 and BOLERO-3</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Fabrice Andre</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Jun 20;34(18):2115-24</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Prevalence of ESR1 Mutations in Cell-Free DNA and Outcomes in Metastatic Breast Cancer A Secondary
Analysis of the BOLERO-2 Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Sarat Chandarlapaty</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Oct 1;2(10):1310-1315</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: T P Sahoo</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Chairpersons Remarks</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Panel Discussion on Translational Science</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Asha Kapadia, Madhuchanda Kar</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>B K Smruti</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Rajiv Sarin, Shaheenah Dawood, Amit Verma, Vijay Agarwal, Vinod Raina, Sudeep Gupta, AVS Suresh, Tanuja Shet, Seema Gulia, Amit Dutt, P P Bapsy</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Debate: AI (with ovarian suppression) is now the preferred adjuvant endocrine therapy in premenopausal patients with ER positive disease</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Chairpersons: Ramesh Bilimagga, K C Gopinath</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Yes :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Shaheenah Dawood </h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">No :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Senthil Rajappa</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section 2end-->
				<!--section3-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="blue-stip">
							<span>Session 3 : HER2+ve Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">A K Malhotra, S H Advani</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>A phase III trial evaluating pCR in patients with HR+, HER2-positive breast cancer treated with neoadjuvant docetaxel, carboplatin, trastuzumab, and pertuzumab (TCHP) +/- estrogen deprivation: NRG oncology/NSABP B-52</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Rimawi MF</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S3-06</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Pathologic complete response rates after neoadjuvant trastuzumab emtansine + pertuzumab vs docetaxel + carboplatin + trastuzumab + pertuzumab treatment in patients with HER2-positive early breast cancer (KRISTINE)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Hurvitz SA</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 500</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Association of Pathologic complete Response to Neoadjuvant therapy in HER+ve Breast Cancer with Long Term Outcomes : A Meta Analysis</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Kristine R. Broglio</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Jun 1;2(6):751-60</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Bhawna Sirohi</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Primary analysis of PERTAIN: A randomized, two-arm, open-label, multicenter phase II trial assessing the efficacy and safety of pertuzumab given in combination with trastuzumab plus an aromatase inhibitor in first-line patients with HER2-positive and hormone receptor-positive metastatic or locally advanced breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Grazia Arpino</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS S3-04</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>T-DM1 Activity in Metastatic Human Epidermal Growth Factor Receptor 2 Positive Breast Cancers That
Received Prior Therapy With Trastuzumab and Pertuzumab</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Hannah Dzimitrowicz</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Journal of Clinical Oncology 34, no. 29 (October 2016) 3511-3517</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>PHEREXA: A phase III study of trastuzumab + capecitabine +/- pertuzumab for patients who progressed during/after one line of trastuzumab-based therapy in the HER2 positive metastatic breast cancer setting</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Urruticoechea A</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, LBA 503</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Shaheenah Dawood</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Neratinib Plus Paclitaxel vs Trastuzumab Plus Paclitaxel in Previously Untreated Metastatic ERBB2-Positive Breast Cancer. The NEfERT-T Randomized Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Ahmad Awada</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Dec 1;2(12):1557-1564</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Neratinib after trastuzumab-based adjuvant therapy in patients with HER2-positive breast cancer (ExteNET): a multicentre, randomised, double-blind, placebo controlled, phase 3 trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Arlene Chan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Lancet Oncol. 2016 Mar;17(3):367-77</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
				<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Muzammil Shaikh</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Association of Polymorphisms in FCGR2A and FCGR3AWith Degree of Trastuzumab Benefit in the Adjuvant Treatment of ERBB2/HER2-Positive Breast Cancer Analysis of the NSABP B31 Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Patrick G. Gavin</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Nov 3</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Effects of Estrogen Receptor and Human Epidermal Growth Factor Receptor-2 Levels on the Efficacy of Trastuzumab A Secondary Analysis of the HER A Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Sherene Loi</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Ref erence: JAMA Oncol. 2016 Aug 1;2(8):1040-7</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Intrinsic Subtype and Therapeutic Response Among HER2-Positive Breast Tumors from the NCCTG (Alliance) N9831 Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Edith A. Perez</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Natl Cancer Inst. 2016 Oct 28;109(2)</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Jaya Ghosh</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Chairpersons Remarks</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Panel Discussion on HER2+Ve Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Tomcha Singh, C J Tamane</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Senthil Rajappa</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Chetan Deshmukh, Shirish Alurkar, Bharat Bhosale, Kannan Kalaichelvi, Kaustav Talapatra, Santam Chakraborty, Shailesh Bondarde, Jaya Ghosh, Amit Agarwal, Avinash Deo, Manisha Singh</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section 3end-->
				<!--section4-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="blue-stip">
							<span>Session 4 : Triple Negative Breast Cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000"> K. Pavithran, N. K. Warrier</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12">
						<p>MERIBEL, a phase 2, multicenter, single -arm trial to evaluate Eribulin therapy for aggressive taxane -resistant HER2-negative metastatic breast cancer(ESMO)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: V Ortega</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Annals of Oncology (2016) 27 (6): 6899. 10.1093 /annonc/mdw365 (ESMO 2016)</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Eribulin induces vascular remodelling and reoxygenation in advanced breast cancer patients: A comparative study with bevacizumab</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Ueda S</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS P4-02-01</p>
						</div>
						</div>
						
					</div>
				</div><!-- col-sm-10-->
			</div><!-- row-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Chetan Deshmukh</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>ABC Joint Analysis: Optimal Chemotherapy Backbone for HER2-Negative EBC: TC vs TaxAC in HER2-Negative EBC (ABC Joint Analysis): Timeline, Accrual, Treatment</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Joanne Lorraine Blum</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 1000</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Effect of Tailored Dose-Dense Chemotherapy vs Standard 3-Weekly Adjuvant Chemotherapy on Recurrence-Free Survival Among Women With High-Risk Early Breast Cancer, A Randomized Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Theodoros Foukakis</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA. 2016;316(18):1888-1896</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Comparison of four cycles epirubicin and cyclophosphamide (EC) followed by four cycles docetaxel (T) versus six cycles docetaxel and carboplatin (TP) as adjuvant chemotherapy for women with operable triple negative breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Peng Yuan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 1068</p>
						</div>
						</div>
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<!-- start-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: A K Vaid</span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>Capecitabine Monotherapy Extends Life and Benefits Patients Age 70 Years and Older with Metastatic Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Stephen Johnston</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ESMO 240 P</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>Association of Proton Pump Inhibitors and Capecitabine Efficacy in Advanced Gastroesophageal Cancer: Secondary Analysis of the TRIO-013/LOGiC Randomized Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Chu MP</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Oct 13</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<!--end-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: G S Bhattacharyyah</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Chairpersons Remarks</span>
					</div>	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Panel Discussion on Triple Negative Breast Cancer</span>
					</div>
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			<div class="row">
					<div class="col-sm-10 verticalline">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>J K Singh, K. C. Lakshmaiah</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Ashish Bakshi</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Anita Ramesh, J S Sekhon, Reetu Jain, T P Sahoo, Bhavna Parikh, Devavrat Arya, Mehboob Basade, Ranga Rao, SVSS Prasad</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!--//end//-->
				<!--section 4end-->
				<!--section5-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="content">
						<div class="blue-stip">
							<span>Session 5 : Rapid Review : Part 1</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#ff0000">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4">
							<p style="color:#ff0000">J K Singh, K. C. Lakshmaiah</p>
						</div>
						</div>
					</div>
				</div><!-- col-sm-10-->
				</div><!-- row-->
				<div class="row">
				<div class="col-sm-10 verticalline">
					<div class="skyblue-stip">
						<span>Reviewer: Shona Nag </span>
					</div>
					<!--sentence-->
					<div class="col-sm-12">
						<div class="col-sm-12">
						<p>1. Expert Critique : Node-Positive Breast Ca: Door Opens to SLN After Neoadjuvant Chemo - False-negative rates lower with advances in surgical techniques (ACOSOG, Alliance Z1071 trial, The SN FNAC Study, SENTINA trial )</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Mateusz Opyrchal</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Medpage Today ASCO Reading room</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>2. Axillary Lymph Node Dissection Can Largely Be Avoided in Breast Cancer (Z11 and Sound trial)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Melissa Louise Pilewskie</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 18th Annual Lynn Sage Breast Cancer Symposium Discussion</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>3. A model to predict axillary nodal pathologic complete response following neoadjuvant chemotherapy for breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: O Kantor</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract1047</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>4. Neratinib + fulvestrant in ERBB2-mutant, HER2-non-amplified, estrogen receptor (ER)-positive, metastatic breast cancer (MBC): Preliminary analysis from the phase II SUMMIT trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D Hyman</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS PD2-08</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>5. Monitoring of Serum DNA Methylation as an Early Independent Marker of Response and Survival in Metastatic Breast Cancer: TBCRC 005 Prospective Biomarker Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Kala Visvanathan</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Nov 21</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>6. Anti-tumor activity of PM01183 (lurbinectedin) in BRCA1/2-associated metastatic breast cancer patients: results of a single-agent phase II trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: J Balmana</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ESMO 223O</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>7. Dual Block with Lapatinib and Trastuzumab Versus Single-Agent Trastuzumab Combined with Chemotherapy as Neoadjuvant Treatment of HER2-Positive Breast Cancer: A Meta-analysis of Randomized Trials</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Clavarezza M</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Clin Cancer Res; 22(18); 4594-603</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>8. Effects of perioperative lapatinib and trastuzumab, alone and in combination, in early HER2+ breast cancer - the UK EPHOS-B trial (CRUK/08/002)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: N. Bundred</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 EBCC 6LBA</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>9. Tumour-infiltrating lymphocytes in advanced HER2-positive breast cancer treated with pertuzumab or placebo in addition to trastuzumab and docetaxel: a retrospective analysis of the CLEOPATRA study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe; visibility: hidden;">Author: Melissa Louise Pilewskie</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Lancet Oncol. 2016 Dec 6. pii: S14702045 (16) 30631-3</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>10. Original Investigation: RNA Sequencing to Predict Response to Neoadjuvant Anti HER2 Therapy.A Secondary Analysis of the NeoALTTO Randomized Clinical Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Debora Fumagalli</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Sep 29</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>11. Lapatinib-Related Rash and Breast Cancer Outcome in the ALTTO Phase III Randomized Trial</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Amir Sonnenblick/p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Natl Cancer Inst. 2016 Apr 20;108(8)</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>12. Intrinsic Subtype Switching and Acquired ERBB2/HER2 Amplifications and Mutations in Breast Cancer Brain Metastases</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Nolan Priedigkeit</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Dec 7</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>13. Prevalence and predictors of androgen receptor and programmed death-ligand 1 in BRCA1 associated and sporadic triple-negative breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Nadine M. Tung</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: npj Breast Cancer 2, Article number:16002(2016)</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>14. A novel biomarker to predict sensitivity to enzalutamide (ENZA) in TNBC</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Joel S. Parker</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol 33, 2015 (suppl; abstr 1083)</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>15. Lack of Androgen Receptor Protein May Contribute to Racial Disparities in Triple Negative Breast Cancer Outcomes</p>
						</div>	
						</div>
						<div class="col-sm-12">
							<div class="col-sm-3">
							<p style="color:#0082fe">Author: Ritu Aneja
							</div>
							<div class="col-sm-9">
							<p style="color:#0082fe">Conference: AACR Abstract B12</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>16. A phase II, open-label, multicentre, translational study for biomarkers of eribulin mesylate: evaluation of the utility of monitoring epithelial-to-mesenchymal transition (emt) markers on tumour cells in the malignant plural effusion of patients with metastatic breast cancer (expect-study)</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Watanabe</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">
				Conference: 2016 SABCS OT1-01-01</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>17. Prognosis of triple negative breast cancer patients who attain pathological complete response with neoadjuvant carboplatin/docetaxel and do not receive adjuvant anthracycline chemotherapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Priyanka Sharma</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 1015</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>18. BRCA1/2 mutation prevalence in triple-negative breast cancer patients without family history of breast and ovarian cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: K Rhiem</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 1090</p>
						</div>
						</div><div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>19. Randomized phase II/III trial of active immunotherapy with OPT-822/OPT-821 in patients with metastatic breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Chiun-Sheng Huang</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 1003</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>20. Randomized, double blind trial to evaluate the safety and efficacy of metformin vs placebo plus neoadjuvant chemotherapy in locally advanced breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: C Arce-Salinas</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 ASCO Annual Meeting, Abstract 579n</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>21. A phase II, multicentre, randomised trial of eribulin plus gemcitabine (EG) vs. paclitaxel plus gemcitabine (PG) in patients with HER2-negative metastatic breast cancer as first-line chemotherapy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Park et al</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">
			Conference: 2016 SABCS OT1-01-12</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>22. PAINTER: Evaluation of eribulin tolerability and correlation between a set of Polymorphisms and neuropathy in patients with metastatic breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: La Verde</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: 2016 SABCS OT1-01-05</p>
						</div>
						</div><div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>23. Survival After Early-Stage Breast Cancer of Women Previously Treated for Depression: A Nationwide Danish Cohort Study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Nis P. Suppli</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JCO,November 14, 2016</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>24. A Population-Based Study of Cardiovascular Mortality Following Early-Stage Breast Cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Husam Abdel-Qadir</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Conference: JAMA Cardiology,2016 Oct 12</p>
						</div>
						</div><div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>25. Obesity As a Risk Factor for Anthracyclines and Trastuzumab Cardiotoxicity in Breast Cancer: A Systematic Review and Meta-Analysis</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Charles Guenancia</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Clin Oncol. 2016 Sep 10;34(26):3157 65n</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>26. Role of Troponins I and T and N-Terminal Prohormone of Brain Natriuretic Peptide in Monitoring Cardiac Safety of Patients With Early-Stage Human Epidermal Growth Factor Receptor 2-Positive Breast Cancer Receiving Trastuzumab: A Herceptin Adjuvant Study Cardiac Marker Substudy</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: D Zardavas</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J. Clin. Oncol 2016 Oct 23</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>27. The temporal risk of heart failure associated with adjuvant trastuzumab in breast cancer patients: A population study</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Hart A.Goldhar</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JNCI J Natl cancer Inst (2016) 108(1)</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>28. Cardiac outcomes of patients receiving adjuvant weekly paclitaxel and trastuzumab for node negative ERBB2-positive breast cancer</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: C Dang</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Jan;2(1):29-36</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>29. Effect of Temporary Ovarian Suppression on Chemotherapy-Induced Amenorrhea, Pregnancy, and Outcome</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Pamela N. Munster</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: JAMA Oncol. 2016 Aug 1;2(8):1089-90</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>30. Endocrine therapy for hormone receptor-positive metastatic breast cancer: American Society of Clinical Oncology guideline</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Rugo HS</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: J Oncol Pract. 2016 Jun;12(6):583-7</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-12 content-bottom">
						<p>31. Do all patients with advance HER2 positive breast cancer need upfront - cemo when receiving trastuzumab ? Randamized phase III trial SAKK 22/29</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-3">
							<p style="color:#0082fe">Author: Pagani O</p>
						</div>
						<div class="col-sm-9">
							<p style="color:#0082fe">Reference: Ann Oncol. 2016 Dec 19. pii : mdw622.
                  doi:10.1093/annonc/mdw622</p>
						</div>
						</div>
						
					<!--sentence end-->	
				</div>
				<div class="col-sm-2 ">
					<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
				</div>
			</div><!-- row-->
			
			<!--end-->
			
		</div>
	</div>
</section>
<?php include('include/footer.php');?>