<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/phead5.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>	
			<span class="no_btn" style="color: #fff !important">&nbsp;DAY &nbsp; <a  class="btn-nonactive" href="<?=url('');?>phocon2016-hematology-d1" ><img src="<?= assets('images/1.png');?>"></a> <a href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>	<span class="insidespan">SUNDAY,6<sup>th</sup> NOVEMBER 2016,MUMBAI</span>
			</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>HEMATOLOGY SESSION 6: CASES BASED PANEL DISCUSSION</span>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Intriguing Anemias</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Sudhir Sane, Dr. Ratna Sharma </h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr.Madhusmita Sengupta, Dr. Nirav Buch, Dr. Shirali Agarwal, Dr. Neha Rastogi, Dr. Akanksha Chichra</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Pancytopenia Epiphenomenon And Infections</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Archana Swami, Dr. Sandeep Bartakke</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Archana Kumar , Dr. Rachna Seth, Dr. Rakesh Mittal , Dr. Sangeeta Mudaliar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>BLEEDING NEONATE: Prolonged Diagnosis Time !!</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Nita Radhakrishnan, Dr, Ruchi Nanavati</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Rhishikesh Thakre, Dr. Narendra Chaudhary, Dr. Priti Mehta, Dr. Sirisharani S., Dr. Sunil Udgire</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Immunisation in Pediatric Hematology/ Oncology</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Nitin Shah</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anupam Sachdev, Dr. Ramandeep Arora, Dr. Brijesh Arora, Dr. Tushar Vora</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Thalassemia Management Beyond Transfusion</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Deepak Bansal, Dr. Praveen Sobti</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Mamta Manglani, Dr. Sunil Bhat, Dr. Abhishek Kulkarni, Dr. Pinky Shah, Dr. Rajiv Bansal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>THROMBOCYTOPENIA: Does the count matter?</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Shaliesh Kanvinde, Dr. Gaurav Narula</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Sujata Sharma, Dr. Vibha Bafna, Dr. Vineet Gupta, Dr. Sandeep Jain, Dr. Anoop P.</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>BLOOD COMPONENTS: Does the product matter?</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Nitin Shah, Dr, Priti Desai</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anand Deshpande, Dr. Anupama Borker, Br. Minal Wade, Dr. Pankaj Abrol, Dr. Deenadayalan M.</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>ONCO-EMERGENCIES: Does the time matter?</span>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-4 abc-top">
							<h5>Dr. Santanu Sen, Dr. Ramandeep Arora</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-2">
							<p>Dr. Anirban Das, Dr. Deepa Trivedi, Dr. Purvi Kadakia, Dr. Manas Kalra, Dr. Anupa Mishra</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 1 end-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>