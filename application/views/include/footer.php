

<section id="footer">
  	<div class="container">
  		<div class="col-md-3 hidden-sm"></div>
  		<div class="col-md-9">
  			<ul class="nav-footer">
  				<li class=""><a href="<?=url('');?>index">HOME</a></li>
  				<li class=""><a href="<?=url('');?>yir2017">YIR 2017</a></li>
  				<li class=""><a href="<?=url('');?>tumor-board">TUMOR BOARD</a></li>
  				<li class=""><a href='<?=url('');?>'>CONFERENCE</a></li>
  				<li class=""><a href="<?=url('');?>virtual">VIRTUAL CLASSROOM</a></li>
  				<li class=""><a href='<?=url('');?>'>CONTACT US</a></li>
  			</ul>
  		</div><br/>
  		<div class="col-sm-12">
  			<p class="footer-copy">	All Copyrights reserved by WCI-TMH  <span style="color:#ad9259">|</span>  Website Designed and Developed by River Route Creative Group</p>
  		</div>
  	</div>
  </section>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<?=js('jquery-1.11.3.min');?>
	<!-- Include all compiled plugins (below), or include individual files as needed --> 
	<?=js('bootstrap');?>
         <script> var logo="<?php echo assets("logo/year-IN-Review.png");?>";
         var base_url="<?php echo url();?>";</script>
  
	 <?=js('https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js');?>
<?=js('https://vjs.zencdn.net/5.19.2/video.js');?>
<!--  <script>
    videojs.options.flash.swf = "../node_modules/video.js/dist/video-js.swf"
  </script>-->

 <?=js('https://cdn.sc.gl/videojs-hotkeys/latest/videojs.hotkeys.min.js');?>
 <?=js('videojs-brand.min');?>
    <?=js('videojs-resolution-switcher');?>
     <?=js('my-video-player');?>
<script>
$(document).ready(function(){
   var currentLocation=window.location.href;
     $(".navbar-nav li").removeClass('active');
    $("a").each(function(idx) {
        if ($(this).attr('href') == currentLocation) {
          $(this).parent().addClass('active');
            if($(this).parent().parent().hasClass('dropdown-menu')){
                $(this).parent().parent().parent().addClass('active');
            }
        }
    });
  
});

</script>

<script>
$(document).ready(function(){
  $('.videoPlayerLink').click(function(){
      $(".videoPlayerDiv").css('display','none');
      console.log($(this).parent().siblings().children('div.videoPlayerDiv').children());
      $(this).parent().siblings().children('div.videoPlayerDiv').show();
  });
  
});

</script>
  <script>
 $(function(){
    $(".navbar-nav li.dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
</script>
  
  </body>
</html>