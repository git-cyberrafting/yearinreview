<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Year In Review</title>
    <!-- Bootstrap -->
	<?=css('bootstrap');?>
	<!--Main CSS-->
	<?=css('style.css');?>
        <?=css('https://vjs.zencdn.net/5.19.2/video-js.css');?>
        <?=css('videojs-resolution-switcher');?>
        <?=css('my-video-player');?>
  <!-- If you'd like to support IE8 -->
 
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        <style>
   iframe video {
    width: 100% !important;
    height: auto;
}
.videoPlayerDiv iframe{
    width:430px; height:300px;
}
.videoPlayerDiv{
    display: none;
}
</style>
  </head>
  <body style="overflow-x: hidden;">