<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumor-board">
	<div class="container">
		<div class="col-sm-12 col-md-12 tumor-head">
			<img src="<?= assets('images/tumorboard-slider.png');?>"  alt="tumor board" class="img-responsive">
			<h3> CONFERENCE PROCEDINGS</h3>
			<div class="text-border"></div>
		</div>
	</div><hr>
	<div class="container">
		<div class="col-sm-12 col-md-12 abt-breadcrumb">
			<ol class="breadcrumb">
				<li><a href='javascript:void(0)'>HOME</a></li>
				<li><a href='javascript:void(0)'>CONFERENCE</a></li>
				<li class="active">SOLID TUMOR</li>        
			</ol>
		</div>
	</div><hr>

	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<!-- drop down-->
				<ul class="sidenav navbar-nav">
					<div class="dropborder"></div>
					<li class="dropdownside">
						<a href='javascript:void(0)' class="dropdown-toggle active" data-toggle="dropdown">Solid Tumor </a>
						<div class="dropborder"></div>
						<ul class="dropdown-menu drop">
							<li class="active"><a href='javascript:void(0)'>Colorectal</a></li>
							<li class="divider"></li>
							<li><a href='javascript:void(0)'>Breast</a></li>
							<li><a href='javascript:void(0)'>Gentiourinary</a></li>
							<li><a href='javascript:void(0)'>Gynecological</a></li>
							<li><a href='javascript:void(0)'>Gastric</a></li>
							<li><a href='javascript:void(0)'>Hepato-biliary</a></li>
							<li><a href='javascript:void(0)'>Pancreatic</a></li>
							<li><a href='javascript:void(0)'>Head & Neck</a></li>
							<li><a href='javascript:void(0)'>Lung</a></li>
							<li><a href='javascript:void(0)'>Soft Tissu Sarcoma</a></li>
						</ul>
					</li>
				</ul>
			<!--dropdown end--> 
			
			<div class="sidelist">
					<ul class="sidenav navbar-nav">

						<li class="dropdownside"><a href='javascript:void(0)' style="color:#000">Hematology</a></li>
						<div class="dropborder1"></div>
						<li class="dropdownside"><a href='javascript:void(0)' style="color:#000">Pediatric</a></li>
						<div class="dropborder1"></div>
						<li class="dropdownside"><a href='javascript:void(0)' style="color:#000">Hemato-Oncology</a></li>
						<div class="dropborder1"></div>
					</ul>
				</div>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class=" row confer-head">
					<h5>2017 CONFERENCE</h5>
					<div class="confer1-border"></div>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>gynecology" ><img src="<?= assets('images/gly.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>tmhealthcare" ><img src="<?= assets('images/confer1.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>b-cancer" ><img src="<?= assets('images/confer2.jpg');?>"></a>
				</div>
				

				<div class=" row confer-head">
					<h5>2016 CONFERENCE</h5>
					<div class="confer1-border"></div>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>metronomic" ><img src="<?= assets('images/metronomic.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>nag-foundation" ><img src="<?= assets('images/nag.jpg');?>"></a>
				</div>

				<div class=" row confer-head">
					<h5>2015 CONFERENCE</h5>
					<div class="confer1-border"></div>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>nicci" ><img src="<?= assets('images/nicci.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>nag" ><img src="<?= assets('images/nag_breast.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>satellite" ><img src="<?= assets('images/satellite.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>tmh" ><img src="<?= assets('images/tmh.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>wci" ><img src="<?= assets('images/wci.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>oncology" ><img src="<?= assets('images/oncology.jpg');?>"></a>
				</div>
				<div class=" col-sm-12 confer-head-img1">
					<a href="<?=url('');?>abc" ><img src="<?= assets('images/abc.jpg');?>"></a>
				</div>

			</div>
		</div>
	</div>

</section>

<?php include('include/footer.php');?>test