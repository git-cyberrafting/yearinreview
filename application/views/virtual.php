<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="virtual">
	<div class="container">
		<div class="col-sm-12 col-md-12 tumor-head">
			<img src="<?= assets('images/tumorboard-slider.png');?>"  alt="tumor board" class="img-responsive">
			<div class="back-gray1">
			<h3>VIRTUAL CLASSROOM</h3>
			<div class="virtual-border"></div>
			</div>
		</div>
	</div><hr>
	<div class="container">
		<div class="col-sm-12 col-md-12 abt-breadcrumb">
			<ol class="breadcrumb">
				<li><a href='javascript:void(0)'>HOME</a></li>
				<li class="active">VIRTUAL CLASSROOM</li>        
			</ol>
		</div>
	</div><hr>
	<div class="tumor-image">
	
		<div class="container">
		<div class="back-gray">
			<div class="col-md-2 hidden-sm"></div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
					<a href="<?=url('');?>videoplayer/index.html"><img src="<?= assets('images/virtual1.jpg');?>" alt="breast cancer" class="img-responsive"></a>
				</div>
			</div>
			
			<div class="col-md-2 hidden-sm"></div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
				<a href="<?=url('');?>ibcr" ><img src="<?= assets('images/virtual2.jpg');?>" alt="indo British" class="img-responsive"></a>
			</div>
			</div>
			
			<div class="col-md-2 col-sm-12 hidden-sm">	</div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
				<a href="<?=url('');?>converge2015" ><img src="<?= assets('images/virtual3.jpg');?>" alt="lung cancer" class="img-responsive"></a>
			</div>
			</div>
			<div class="col-md-2 col-sm-12 hidden-sm">	</div>
			<div class="col-md-10 col-sm-10">
				<div class="col-md-10 col-sm-10">
				<a href="<?=url('');?>converge2014" ><img src="<?= assets('images/virtual4.jpg');?>" alt="lung cancer" class="img-responsive"></a>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>


<?php include('include/footer.php');?>