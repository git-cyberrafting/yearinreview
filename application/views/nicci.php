<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/niccihead.png');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn" style="color: #fff !important"> &nbsp;DAY &nbsp; <a href='javascript:void(0)'><img src="<?= assets('images/1.png');?>"></a> <a class="btn-nonactive" href="<?=url('');?>nicci2" ><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 1, SATURDAY, 14<sup>th</sup> NOV 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
			<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Inaugration of the meeting Welcome by Bhawna Sirohi, Sharat Damodar,
								Devi Shetty</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 1: Breast Cancer - The future : Indian Cancer Congress Session</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>GK Rath, New Delhi ; KS Gopinath, Bengaluru ;Ramesh Billimagga, Bengaluru</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Ashish singh, CMC Vellore </h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Plenary : Landmark advances in the management of breast cancer over the last Decade: Where will we be in 2025?</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Eric Winer, Harvard University, Boston, USA </p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 2: Endocrine therapy in early breast cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Nalini Kilara, Bengaluru; Anita Ramesh, Chennai;Dinesh Pendharkar, Mumba</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Sushant Mittal, Artemis, Delhi</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Plenary : Adjuvant Endocrine Therapy: Replacing chemotherapy with endocrine therapy: is this an achievable goal?</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Speaker : </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Ian E. Smith, Royal Marsden Hospital, London, UK</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section2 end-->
					<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 3: Prevention of breast cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>V Vyas, Wardha; Kapil Kumar, Delhi</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Nishitha Shetty, Mangalore</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Risk reducing strategies among women at high risk of developing breast cancer: can we define risk accurately?</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Sudeep Gupta, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Trevor Powles, UK; Nivedita Sinha, Mumbai;Shubhada Mahesh, Bengaluru; Niby Jacob, Bengaluru;K Geeta, Delhi; Ian E. Krop, USA; Arvind Krishnamurthy, Chennai</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!-- section3 end-->
				<!-- section4-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 4: Advanced Breast cancer â€“the right way forward</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>PP Bapsy, Bengaluru; Govind Babu, Bengaluru;JK Singh, Patna</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Amol Dongre, Nagpur</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Plenary : Optimal sequencing of anti her2 therapy in the metastatic setting? - Practical recommendations in situations with limited access to all approved agents</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Clifford Hudis, Memorial Sloan Kettering Cancer Centre, New York, USA</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Controversy</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Gautam Goyal, Chandigarh Oligometastatsestreat as M1 or LABC ?</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Nita Nair, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section4 end-->
				<!-- section5-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 5: Biomarkers / Bone Modifying agents</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Vamshi Krishna, Hyderabad; Pritanjali Singh, Patna;
								SH Advani, Mumbai</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Neha Chaudhary, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Biomakers in breast cancer : Rational use and clinical implications</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>BK Smruti, Bombay Hospital, Mumbai</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Rekha V. Kumar, Bengaluru, Ian E. Smith, UK;Sudhir Borongha, Bengaluru; N Rohatgi, Delhi;Geetashree Mukherjee, Kolkata; Govind Babu, Bengaluru</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Plenary : Do bone modifying agents reducing the risk of breast cancer recurrence: putting EBCCTG data into perspective</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Trevor Powles, Parkside Cancer Centre,Wimbledon, UK</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section5 end-->
				<!-- section6-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 6: Research study presentation - Of all the proposals submitted from across the country, following five research proposals were shortlisted by peer review.</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>R Gopal, Mumbai; Asha Kapadia, Mumbai;Kiran Kothari, Ahmedabad</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Rucha Kaushik, Mumbai Peer Review/Judges:Eric P. Winer, USA; R A Badwe, Mumbai;Kiran Mazumdar Shaw, Bengaluru; Ian E. Smith, UK;R Digumarti,Hyderabad; Clifford Hudis, USA;SD Banavali, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Study 1 : Weight loss in breast cancer survivors can improve outcomes</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Presentor : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Rosina Ahmed, Tata Memorial Centre, Kolkata</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Study 2 : Development of a microfluidics-based platform for detecting and isolating CTCs</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Presentor : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Rahul Purwar, Indian Institute of Technology, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Study 3 : Randomised trial of adjuvant metronomic chemotherapy in patients with locally advanced Triple negative breast cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Presentor : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Shona Nag, Jehangir Hospital, Pune</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Study 4: Randomised Phase II trial to evaluate the benefit of predictive genetic markers in treatment of breast cancer in the neo adjuvant setting</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Presentor : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Sudeep Gupta, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Study 5 : Smart trigger responsive, targeted nanocapsules for oral delivery of paclitaxel, resveratrol and Vitamin D in triple negative breast cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Presentor : </p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Rinti Banerjee, Indian Institute of Technology, mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<!-- section6 end-->
				<!-- section7-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="blue-stip">
							<span>Session 7: Newer approaches to treating breast cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Gaurav Agarwal, Lucknow; Shekhar Patil, Bengaluru;Nagraj Huilgol, Mumbai</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>P. Santosh, MS Ramaiah Bengaluru</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate : Trials are now warranted of surgery vs. none as part of the curative treatment for early breast cancer</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#330033">For :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Ian E. Smith, Royal Marsden Hospital, London, UK</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#330033">Against :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>R A Badwe, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Session on Health Economics</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view2">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Shaheenah Dawood, Dubai Hospital, UAE;Establishing a framework for evidence based cost effectiveness in healthcare delivery</h5>
						</div>
						</div>
							<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>CS Pramesh, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Shankar Prinja, Chandigarh; Clifford Hudis, USA;Eric P. Winer, USA; P. Boregowda, Bengaluru;Devi Shetty, Bengaluru; Kamal Saini, Belgium</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->
				<!-- section7 end-->
				<!-- section8-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 8: Triple negative breast cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Poonam Patil, Bengaluru; Harit Chaturvedi, Delhi; Sanjeev Misra, Jodhpur</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Pragnya Coca, Bangalore</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Plenary : Novel therapeutic approaches to women with metastatic TNBC: 2015 has been a great year!</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Speaker :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Ian E. Krop, Dana Faber Cancer Institute, Harvard University, Boston, USA</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<!-- section8 end-->
					<!-- section9-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 9: Neoadjuvant / adjuvant startegy for locoregional control</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Anthony Pais, Bengaluru; DK Parida,Bhuvaneshwar; Sanjay Sharma, Mumbai</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 abc-top">
							<h5>Ritesh Pruthy, Chandigarh; Jeba Singh, Madurai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate : pCR - a surrogate for survival - should we aim for it ?</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Yes :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Shona Nag, Jehangir Hospital, Pune</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>No :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Sudeep Gupta, Tata Memorial Centre, Mumbai</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Debate : Sentinel node positive in Breast conserving surgery â€“ treating the axilla</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1 ">
							<p>Rahul Krishnatry, Bengaluru; Sandeep Jain, Bengaluru Axillary clearance is a must: Somashekhar SP, Manipal Hospital, Bengaluru
							Radiotherapy in enough: Sanjoy Chatterjee, Tata Memorial Centre, Kolkata</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				
				<!-- section9 end-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="blue-stip">
							<span>Session 10: Day to Day Practice in breast cancer</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Selvi R, Chennai; Siddharth Sahni, Delhi; A A Ranade, Pune</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Session Rapporteur: </p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>AK Malhotra, Kolkata</p>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip">
							<span>Challenging cases</span>
						</div>	
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Case 1 :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Neoadjuvant Her 2 positive case</h5>
						</div>
						</div>
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p>Case 2 :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Metastatic hormone receptor positive case</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 ">
							<p>Shaheenah Dawood, Dubai Hospital, UAE Vineet Gupta, Sakra World Hospital, Bengaluru</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Anupama Mane, Pune; Pranjali Gadgil, Pune;NK Warrier, Calicut; Kaustav Talpatra, Mumbai;Sujai Hegde, Pune; Archana Shetty, Mumbai;Ian Krop, USA; Ajay Gogia, Delhi</p>
						</div>	
						</div>
					</div>
				</div><!-- row-->

			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>