<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Year In Review</title>
    <!-- Bootstrap -->
	<?=css('bootstrap');?>
	<?=css('https://vjs.zencdn.net/5.19.2/video-js.css');?>
        <?=css('videojs-resolution-switcher');?>
        <?=css('my-video-player');?>
  <!-- If you'd like to support IE8 -->
 
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        <style>
 .video-js {
    margin: 0;
}

.video-js .vjs-control {
    font-size: 15px;
    margin-left: 5px;
}
.video-js .vjs-time-control.vjs-time-divider {
    display: block;
    font-size: 15px;
    color: #fff;
    padding-left: 2px;
    padding-right: 0;
    width: 0 !important;
    margin-right: -27px;
}
.vjs-brand-container {
    margin-top: 5px;
}
.vjs-menu li.vjs-menu-item {
    font-size: 13px;
    text-align: left;
}
.vjs-menu li {
    list-style: none;
    margin: 0;
    padding: 0.2em 0;
    line-height: 1em;
    font-size: 1.2em;
    text-align: center;
    text-transform: lowercase;
}
.video-js .vjs-control-bar {
    height: 4.5em;
    background: #222;
}
</style>
  </head>
  <body>
      <?php  
        $v1=$v12=$v2=$v22='';
        $url='http://yearinreview.in/';
        
        if(isset($_GET['v1'])){
            $v1=$url.$_GET['v1'].'?360';
            $v12=$url.$_GET['v1'].'?480';
        }
        else {
            $this->load->library('user_agent');
             if ($this->agent->is_referral()){
                 redirect($this->agent->referrer());
             }else{
                redirect(url()); 
             }
        }
        if(isset($_GET['v2'])){
           $v2=$url.$_GET['v2'].'?720';
           $v22=$url.$_GET['v2'].'?1080';
        }
        ?>
 <video id="video_1"     width='430'
    height='300'  class="vjs-matrix video-js vjs-big-play-centered" controls data-setup='{}' >
     <?php if($v2!=''):?>
                        <source src="<?=$v22;?>" type='video/mp4' label='1080 HD' res='1080' />
                        <source src="<?=$v2;?>" type='video/mp4' label='720 SD' res='720'/>
                      <?php endif;?>  <source src="<?=$v12;?>" type='video/mp4' label='480 ' res='480' />
                        <source src="<?=$v1;?>" type='video/mp4' label='Auto' res='360'/>
                    </video>
           

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<?=js('jquery-1.11.3.min');?>
	<!-- Include all compiled plugins (below), or include individual files as needed --> 
	<?=js('bootstrap');?>
         <script> var logo="<?php echo assets("logo/year-IN-Review.png");?>";
         var base_url="<?php echo url();?>";</script>
  
	 <?=js('https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js');?>
<?=js('https://vjs.zencdn.net/5.19.2/video.js');?>
<!--  <script>
    videojs.options.flash.swf = "../node_modules/video.js/dist/video-js.swf"
  </script>-->

 <?=js('https://cdn.sc.gl/videojs-hotkeys/latest/videojs.hotkeys.min.js');?>
 <?=js('videojs-brand.min');?>
    <?=js('videojs-resolution-switcher');?>
     <?=js('my-video-player');?></body>
</html>