<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="tumornew">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/naghead.jpg');?>">
		</div>
	</div>
	<div class="container">
	<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="no_btn" style="color: #fff !important"> &nbsp;DAY &nbsp; <a class="btn-nonactive" href="<?=url('');?>nag" ><img src="<?= assets('images/1.png');?>"></a> <a  href='javascript:void(0)'><img src="<?= assets('images/2.png');?>"></a></span>
			<span class="insidespan-nicci">DAY 2, 20<sup>th</sup> OCTOBER 2015</span>
		</div>
		</div>
		<div class="container">
			<div class="back-blue">
				<div class="row">
					<div class="col-sm-10 verticalline"></br>
						<div class="skyblue-stip ">
							<span>Lymphoedema workshop</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Physiotherapist  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Anuradha Daptardar</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Occupational therapist :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Dr. Manjusha Vagal</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Survivor Experience 1 - Ms. Heena Khan</span>
						</div>				
							</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->

				<!--section 2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Nutrition after cancer treatment, with special emphasis on weight loss</span>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p>Dietician  :</p> 
						</div>
						<div class="col-sm-8 col-sm-pull-1">
							<p>Mr. Y. T. Shivshankar</p>
						</div>
						</div>	
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="skyblue-stip">
							<span>Thanks and Concluding Remarks Ms. Devieka Bhojwani & Dr. Shona Nag</span>
						</div>
					</div>
					<div class="col-sm-2 ">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-btn.jpg');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>