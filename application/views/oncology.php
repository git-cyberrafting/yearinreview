<?php 
include('include/header.php');
include('include/navigation.php');
?>
<section id="gynecology">
	<div class="container">
		<div class="col-sm-12 top-image">
			<img src="<?= assets('images/oncologyhead.jpg');?>">
		</div>
	</div>
	<div class="container">
		<div class="black-stip">
			<span>SCIENTIFIC PROGRAM</span>			
			<span class="insidespan-satellite">SATURDAY, 16<sup>th</sup> MAY 2015</span>

			</div>
		</div>
		<div class="container">
			<div class="back-bluee">
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 1 : Onco-pathology Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. Anita Borges, Dr. Purvish Parikh</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. Vineet Gupta	</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Sangeeta Desai, Dr. Chandralekha Tampi, Dr. Anita Bhaduri,Dr. Sarabjeet Arneja, Dr. Tanuja Shet, Dr. Roshan Chinoy,Dr. Shaheenah Dawood, Dr. G.S. Bhattacharyya, Dr. Jyoti Bajpai,Dr. D.C. Doval, Dr. Satya Dattatreya</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section1 end-->
				<!-- section2-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Influence of germline genetics in management of breast cancer patients and their families</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p>Speakers  :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. Shaheenah Dawood</p>
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!--scetion2 end-->
				<!-- section3-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 2 : Surgical Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. Ian D`souza, Dr. Mehul Bhansali, Dr. Vijay Haribhakti</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. Sanjay Sharma</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Mandar Nadkarni, Dr. Gaurav Agarwal, Dr. Anil Heroor, Dr. Sandip Bipte,Dr. K. Geeta, Dr. Anthony Pais, Dr. C.B. Koppiker, Dr. R.K. Deshnpande,Dr. Vani Parmar, Dr. Amish Dalal, Dr. Dhairyasheel Savant</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Take Home Message :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Vani Parmar</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 3 end-->
				<!-- section 5-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 3 : Radiotherapy Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. B.C. Goswami, Dr. Nagraj Huilgol</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. Uday Maiya</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. V. Kannan, Dr. Kaustav Talapatra, Dr. Ashwini Buddrukar,Dr. Gautam Sharan, Dr. Sandeep De, Dr. A.K. Anand, Dr. Manish Chandra,Dr. Vijayanand Reddy, Dr. Dinesh Singh</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Take Home Message :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Vijayanand Reddy</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 5 end-->
				<!-- section 6-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 4 : Endocrine Therapy Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. Asha Kapadia, Dr. Ramesh Nimmagadda</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. B.K. Smruti</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Raju Chacko, Dr. Madhuchanda kar, Dr. Anita Ramesh,Dr. Mehboob Basade, Dr. Ajay Bapna, Dr. Adwaita Gore,Dr. Bhavna Parikh, Dr. Bharat Bhosale, Dr. Smita Gupte</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Take Home Message :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Raju Chacko</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 6 end-->
				<!-- section 7-->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 5 : Chemotherapy Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. R. Gopal, Dr. D.C.Doval</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. Sudeep Gupta</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Sunil Kumar Gupta, Dr. Indumati Ambulkar, Dr. Krishna prasad,Dr. Amit Agarwal, Dr. Satya Dattatreya, Dr. Boman Dhabhar,Dr. Ashish Joshi, Dr. Linu Jacob, Dr. P.G. Chitalkar</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Take Home Message :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Amit Agarwal</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 7 end-->
				<!-- section 8 -->
				<div class="row">
					<div class="col-sm-10 verticalline">
						<div class="bluee-stip">
							<span>Panel 6 : Targeted / Neoadjuvant / Miscellaneous Therapy Updates</span>
						</div></br>
						<div class="col-sm-12">
						<div class="col-sm-6 ">
							<p style="color:#cc0066">Chairpersons :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3">
							<p>Dr. S.H. Advani, Dr. Tapan Saikia</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-6">
							<p style="color:#cc0066">Moderator :</p> 
						</div>
						<div class="col-sm-6 col-sm-pull-3 abc-top">
							<h5>Dr. Senthil Rajappa</h5>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#330033">Panelists :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. Ramesh Nimmagadda, Dr. T. Raja, Dr. Bhavesh Parikh,Dr. Randeep Singh, Dr. Govind Babu, Dr. Rejiv Rajendranath,Dr. T.P. Sahoo, Dr. Ashish Bakshi, Dr. Sandeep Goyle,Dr. Muzammil Sheikh, Dr. Sanjoy Chatterjee</p>
						</div>	
						</div>
						<div class="col-sm-12">
						<div class="col-sm-4">
							<p style="color:#0c7abf">Take Home Message :</p> 
						</div>
						<div class="col-sm-8 col-md-8 col-sm-pull-1">
							<p>Dr. T. Raja</p>
						</div>	
						</div>
					</div>
					<div class="col-sm-2 col-md-2 btn-view">
						<a href='<?=url('');?>'><img src="<?= assets('images/view-black.png');?>" style="width:72px;"></a>	
					</div>
				</div><!-- row-->
				<!-- section 8 end-->
				
			</div>
		</div>
	</section>
	<?php include('include/footer.php');?>