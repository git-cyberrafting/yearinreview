<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class VideoModel extends MY_Model {

    protected $primaryTable = 'tbVideo';
    protected $primaryTablePK = 'id';
    
    protected $secondaryTable = '';
    protected $secondaryTablePK = '';

    function __construct() {
        parent::__construct($this->primaryTable,$this->primaryTablePK);
    }
    
}
